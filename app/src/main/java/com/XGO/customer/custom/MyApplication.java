package com.XGO.customer.custom;

import androidx.multidex.MultiDexApplication;
import com.crashlytics.android.Crashlytics;
//import com.mapbox.mapboxsdk.Mapbox;

import io.fabric.sdk.android.Fabric;

/**
 * Created by android on 15/3/17.
 */

public class MyApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        //Mapbox.getInstance(getApplicationContext(), getString(R.string.mapboxkey));


    }

    private String someVariable;
    private  String myrideid;
    private Double myfare;
    private Double Driverfare;

    public String getSomeVariable() {
        return someVariable;
    }

    public void setSomeVariable(String someVariable) {
        this.someVariable = someVariable;
    }

    public String myrideid() {
        return myrideid;
    }

    public void myrideid(String myrideid) {
        this.myrideid = myrideid;
    }

    public Double myfare() {
        return myfare;
    }

    public void myfare(Double myfare) {
        this.myfare = myfare;
    }

    public Double Driverfare() {
        return Driverfare;
    }

    public void Driverfare(Double Driverfare) {
        this.Driverfare = Driverfare;
    }
}
