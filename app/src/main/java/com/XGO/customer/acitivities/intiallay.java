package com.XGO.customer.acitivities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.XGO.customer.R;
import com.XGO.customer.session.SessionManager;

public class intiallay extends AppCompatActivity {
    SessionManager sessionManager;
    Button sign1,register1,otplogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.intiallay);
        bindView();

        sign1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(intiallay.this, LoginActivity.class));
                finish();

            }
        });
        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(intiallay.this, RegisterActivity.class));
                finish();

            }
        });
        otplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(intiallay.this, LoginActivity.class));
                finish();

            }
        });

    }

    public void bindView() {
        sessionManager = new SessionManager(getApplicationContext());
        sign1 = (Button) findViewById(R.id.sign1);
        register1 = (Button) findViewById(R.id.register1);
        otplogin = (Button) findViewById(R.id.otplogin);
    }
}