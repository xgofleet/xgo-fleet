package com.XGO.customer.acitivities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.XGO.customer.fragement.ScheduledRequestfragment;
import com.XGO.customer.fragement.refer;
import com.XGO.customer.pojo.PendingRequestPojo;
import com.bumptech.glide.Glide;
import com.XGO.customer.R;
import com.XGO.customer.Server.Server;
import com.XGO.customer.custom.CheckConnection;
import com.XGO.customer.fragement.AcceptedRequestFragment;
import com.XGO.customer.fragement.HomeFragment;
import com.XGO.customer.fragement.ProfileFragment;
import com.XGO.customer.session.SessionManager;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import com.razorpay.PaymentResultListener;
import com.thebrownarrow.permissionhelper.ActivityManagePermission;
import com.thebrownarrow.permissionhelper.PermissionUtils;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by android on 7/3/17.
 */

public class  HomeActivity extends ActivityManagePermission implements NavigationView.OnNavigationItemSelectedListener, ProfileFragment.ProfileUpdateListener, ProfileFragment.UpdateListener, PaymentResultListener {
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    public Toolbar toolbar;
    String permissionAsk[] = {PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE, PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_ACCESS_FINE_LOCATION,PermissionUtils.Manifest_READ_CONTACTS,PermissionUtils.Manifest_CALL_PHONE, PermissionUtils.Manifest_ACCESS_COARSE_LOCATION};
    TextView is_online, username;
    SwitchCompat switchCompat;
    LinearLayout linearLayout;
    NavigationView navigationView;
    SessionManager sessionManager;
    private ImageView avatar;
    ImageView imageView11;
    private final int requestCode = 20;
    String frag;
    String[] FILE;
    int count;
    private static int IMG_RESULT = 1;
    String ImageDecode;
    RelativeLayout footer, getdetails;
    PendingRequestPojo pojo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //  toolbar.setBackground(new ColorDrawable(Color.TRANSPARENT));
        BindView();
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("action")) {
            String action = intent.getStringExtra("action");
            AcceptedRequestFragment acceptedRequestFragment = new AcceptedRequestFragment();
            Bundle bundle = new Bundle();
            bundle.putString("status", action);
            acceptedRequestFragment.setArguments(bundle);
            changeFragment(acceptedRequestFragment, "Requests");
        }
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
        avatar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                changeFragment(new ProfileFragment(), getString(R.string.profile));

            }
        });

    }

  /* private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        })
        builder.show();
    }*/
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//        try {
//
//            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
//                    && null != data) {
//
//
//                Uri URI = data.getData();
//                String[] FILE = { MediaStore.Images.Media.DATA };
//
//
//                Cursor cursor = getContentResolver().query(URI,
//                        FILE, null, null, null);
//
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndex(FILE[0]);
//                ImageDecode = cursor.getString(columnIndex);
//                cursor.close();
//
//                avatar.setImageBitmap(BitmapFactory
//                        .decodeFile(ImageDecode));
//
//            }
//        } catch (Exception e) {
//            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
//                    .show();
//        }
//
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                onCaptureImageResult(data);
                Uri contactUri = data.getData();
                Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
                cursor.moveToFirst();
                int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                Log.d("phone number", cursor.getString(column));
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                //Log.w("path of image from gallery......******************", picturePath+"");
                avatar.setImageBitmap(thumbnail);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        avatar.setImageBitmap(thumbnail);
    }

    private void setupDrawer() {          //to open side bar
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setBackground(null);
        //  globatTitle = );
        getSupportActionBar().setTitle(getString(R.string.app_name));

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }


        };


        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        //drawer.shouldDelayChildPressedState();


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void drawer_close() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        AcceptedRequestFragment acceptedRequestFragment = new AcceptedRequestFragment();
        ScheduledRequestfragment scheduledRequestFragment = new ScheduledRequestfragment();
        Bundle bundle;

        switch (item.getItemId()) {
            case R.id.home:
                changeFragment(new HomeFragment(), getString(R.string.home));
                break;
            case R.id.pending_requests:
                bundle = new Bundle();
                bundle.putString("status", "PENDING");
                acceptedRequestFragment.setArguments(bundle);
                changeFragment(acceptedRequestFragment, "Requests");
                break;
            case R.id.schedule:
//                Intent i = new Intent(HomeActivity.this, schedule.class);
//                startActivity(i);

                bundle = new Bundle();
                bundle.putString("status", "PENDING");
                scheduledRequestFragment.setArguments(bundle);
                changeFragment(scheduledRequestFragment, "Requests");
                break;

            case R.id.completed_rides:
                bundle = new Bundle();
                bundle.putString("status", "COMPLETED");
                acceptedRequestFragment.setArguments(bundle);
                changeFragment(acceptedRequestFragment, "Requests");
                break;
            case R.id.cancelled:
                bundle = new Bundle();
                bundle.putString("status", "CANCELLED");
                acceptedRequestFragment.setArguments(bundle);
                changeFragment(acceptedRequestFragment, "Requests");
                break;
            case R.id.profile:
                changeFragment(new ProfileFragment(), getString(R.string.profile));
                break;
            case R.id.logout:
                sessionManager.logoutUser();
                finish();
                break;
            case R.id.refer:
                changeFragment(new refer(), "Refer A Friend");

                break;

            case R.id.terms:
            Uri uri = Uri.parse("http://xgofleet.com/terms.html"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
                 break;

            case R.id.privacy:
                Uri uri1 = Uri.parse("http://xgofleet.com/privacy.html"); // missing 'http://' will cause crashed
                Intent intent1 = new Intent(Intent.ACTION_VIEW, uri1);
                startActivity(intent1);
                break;

            default:
                break;
        }
        return true;
    }

    public void homefrag(View view)
    {
        changeFragment(new HomeFragment(), getString(R.string.home));

    }

    public void changeFragment(final Fragment fragment, final String fragmenttag) {
        if(fragmenttag.equals("Requests")||fragmenttag.equals("")){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    drawer_close();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().addToBackStack(null);
                    fragmentTransaction.replace(R.id.frame, fragment, fragmenttag);

                    //      Log.d("frag", "" + fragment);
                    fragmentTransaction.commit();
                    fragmentTransaction.addToBackStack(null);
                }
            }, 50);
        } catch (Exception e) {

        }

    }


    @Override
    public void update(String url) {
        if (!url.equals("")) {
            //  Glide.with(getApplicationContext()).load(url).error(R.drawable.images).into(avatar);
        }
    }

    @Override
    public void name(String name) {
        if (!name.equals("")) {
            username.setText(name);
        }
    }

    @SuppressLint("ParcelCreator")
    public class CustomTypefaceSpan extends TypefaceSpan {

        private final Typeface newType;

        public CustomTypefaceSpan(String family, Typeface type) {
            super(family);
            newType = type;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            applyCustomTypeFace(ds, newType);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            applyCustomTypeFace(paint, newType);
        }

        private void applyCustomTypeFace(Paint paint, Typeface tf) {
            int oldStyle;
            Typeface old = paint.getTypeface();
            if (old == null) {
                oldStyle = 0;
            } else {
                oldStyle = old.getStyle();
            }

            int fake = oldStyle & ~tf.getStyle();
            if ((fake & Typeface.BOLD) != 0) {
                paint.setFakeBoldText(true);
            }

            if ((fake & Typeface.ITALIC) != 0) {
                paint.setTextSkewX(-0.25f);
            }

            paint.setTypeface(tf);
        }
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "font/AvenirLTStd_Medium.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void fontToTitleBar(String title) {
        try {
            Typeface font = Typeface.createFromAsset(getAssets(), "font/AvenirLTStd_Book.otf");
            title = "<font color='#ffffff'>" + title + "</font>";
            SpannableString s = new SpannableString(title);
            s.setSpan(font, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                toolbar.setTitle(Html.fromHtml(String.valueOf(s), Html.FROM_HTML_MODE_LEGACY));
            } else {
                toolbar.setTitle((Html.fromHtml(String.valueOf(s))));
            }
        } catch (Exception e) {
            Log.e("catch", e.toString());
        }
    }


    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = HomeActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    public void BindView() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        sessionManager = new SessionManager(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        switchCompat = (SwitchCompat) navigationView.getHeaderView(0).findViewById(R.id.online);
        avatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.profile);
        imageView11 = (ImageView) findViewById(R.id.imageView11);
        linearLayout = (LinearLayout) navigationView.getHeaderView(0).findViewById(R.id.linear);
        is_online = (TextView) navigationView.getHeaderView(0).findViewById(R.id.is_online);
        username = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txt_name);
        navigationView.setCheckedItem(R.id.home);
        //Bundle bundle = getArguments();
        //pojo = (PendingRequestPojo) bundle.getSerializable("data");
        //getdetails=(RelativeLayout) findViewById(R.id.getdetail);
        onNavigationItemSelected(navigationView.getMenu().findItem(R.id.home));
        setupDrawer();
        try {
            Typeface font = Typeface.createFromAsset(getAssets(), "font/AvenirLTStd_Book.otf");
            username.setTypeface(font);
        } catch (Exception e) {

        }
        //toolbar.setTitle("Xgo");


        if (CheckConnection.haveNetworkConnection(getApplicationContext())) {
            getUserInfo();

        } else {
            Toast.makeText(HomeActivity.this, getString(R.string.network), Toast.LENGTH_LONG).show();

            SessionManager sessionManager = new SessionManager(getApplicationContext());
            HashMap<String, String> user = sessionManager.getUserDetails();
            if (user != null) {
                String name = user.get(SessionManager.KEY_NAME);
                String url = user.get(SessionManager.AVATAR);
                username.setText(name);
                Glide.with(HomeActivity.this).load(url).into(avatar);

            }

        }


        imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:+917338788823"));
                if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            }
        });


    }

      public void getUserInfo() {

        RequestParams params = new RequestParams();
        if (sessionManager == null) {
            SessionManager sessionManager = new SessionManager(getApplicationContext());
            HashMap<String, String> user = sessionManager.getUserDetails();
            if (user != null) {
                String uid = user.get(SessionManager.USER_ID);
                params.put("user_id", uid);
                Server.setHeader(sessionManager.getKEY());
            }
        } else {
            HashMap<String, String> user = sessionManager.getUserDetails();
            if (user != null) {
                String uid = user.get(SessionManager.USER_ID);
                params.put("user_id", uid);
                Server.setHeader(sessionManager.getKEY());
            }
        }
        Server.get("api/user/profile/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {

                        String name = response.getJSONObject("data").getString("name");
                        String email = response.getJSONObject("data").getString("email");
                        String user_id = response.getJSONObject("data").getString("user_id");
                        String url = response.getJSONObject("data").getString("avatar");
                        String mobile = response.getJSONObject("data").getString("mobile");
                        sessionManager.createLoginSession(name, email, user_id, url, mobile);

                        Glide.with(HomeActivity.this).load(avatar).into(avatar);

                        //  Glide.with(HomeActivity.this).load(url).error(R.drawable.images).into(avatar);
                        username.setText(name);


                    }
                } catch (Exception e) {

                }
            }

        });

    }



    @Override
    public void onBackPressed() {
//        getSupportFragmentManager().popBackStack();

        Log.d("closed", " " + getVisibleFragment());
        count = getSupportFragmentManager().getBackStackEntryCount();
        Fragment tag = getSupportFragmentManager().findFragmentByTag("Request");
        Log.d("closed", " " + count);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawer_close();
        }
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(R.drawable.xgo_icon);
            builder.setTitle(Html.fromHtml("<font color='#4678F6'>Exit Confirmation</font>"));
            builder.setMessage("Are you sure you want to exit? ");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    changeFragment(new HomeFragment(),getString(R.string.home));

                }
            });

            builder.show();
        }

        else {
//            changeFragment(new HomeFragment(), getString(R.string.home));
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setIcon(R.drawable.xgo_icon);
//            builder.setTitle(Html.fromHtml("<font color='#4678F6'>Exit Confirmation</font>"));
//            builder.setMessage("Are you sure you want to exit? ");
//            builder.setCancelable(false);
//            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    finish();
//                }
//            });
//
//            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    changeFragment(new HomeFragment(), getString(R.string.home));
//
//                }
//            });
//
//            builder.show();
//            getSupportFragmentManager().popBackStack();
            super.onBackPressed();
        }
//        else if (count >1) {
//
//            changeFragment(new HomeFragment(), getString(R.string.home));
//            getSupportFragmentManager().popBackStackImmediate();
//            getSupportFragmentManager().popBackStack();
//            Log.d("closed","count2");
//
//        } else {
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setIcon(R.drawable.xgo_icon);
//            builder.setTitle(Html.fromHtml("<font color='#4678F6'>Exit Confirmation</font>"));
//            builder.setMessage("Are you sure you want to exit? ");
//            builder.setCancelable(false);
//            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    finish();
//                }
//            });
//
//            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                }
//            });
//
//            builder.show();
//
//        }
    }

    public int getMyData() {
        return 1;
    }

    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            String myvl = ((com.XGO.customer.custom.MyApplication) this.getApplication()).myrideid();
            Toast.makeText(HomeActivity.this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            RequestParams params = new RequestParams();
            Double fare1 = ((com.XGO.customer.custom.MyApplication) this.getApplication()).myfare();
            Double driverfare = ((com.XGO.customer.custom.MyApplication) this.getApplication()).Driverfare();
            params.put("ride_id", myvl);
            params.put("payment_Status", "PAID");
            params.put("payment_receipt", razorpayPaymentID);
            params.put("payment_mode", "ONLINE");
            int fare = (int) Math.round(Double.parseDouble(Double.toString(fare1)));
            double drate = driverfare/100;
            double dfare = fare*drate;
            //double dfare = fare*0.82;
            int dfare1 = (int) Math.round(dfare);
            double cfare = 1 - drate;
            double xfare = fare*cfare;
            //double xfare = fare*0.18;
            int xfare1 = (int) Math.round(xfare);
            params.put("amount",fare);
            params.put("damount",dfare1);
            params.put("xamount",xfare1);
            Server.setContetntType();
            Server.setHeader(sessionManager.getKEY());
            Server.post("api/user/rides", params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    HomeFragment fragobj = new HomeFragment();
                    //  bundle.putString("runtime",runtime1);
                    //((HomeActivity) getActivity()).changeFragment(fragobj, "Home");
                    HomeActivity.this.changeFragment(fragobj,"Home");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    if (HomeActivity.this != null) {
                    }
                }
            });

        } catch (Exception e) {
            Log.e("Payment", "Exception in onPaymentSuccess", e);
        }
    }

    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(HomeActivity.this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Log.e("Payment", "Exception in onPaymentError", e);
        }
    }

}