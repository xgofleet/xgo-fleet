package com.XGO.customer.acitivities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.XGO.customer.R;
import com.XGO.customer.Server.Server;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class   passwordreset extends AppCompatActivity {
    Button button3;
    View view;
    String mobile,pass1;
    EditText otp,pass,repass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passwordreset);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Intent myIntent = getIntent(); // gets the previously created intent
        mobile = myIntent.getStringExtra("mobile");
        BindView();
    }

    private void BindView() {
        otp = findViewById(R.id.otp);
        pass = findViewById(R.id.pass);
        repass = findViewById(R.id.repass);
        button3 =findViewById(R.id.button3);


        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pass1 = repass.getText().toString();
                if(pass.getText().toString().equals(pass1)){
                RequestParams params = new RequestParams();
                String otp1 = otp.getText().toString().trim();
                Log.d("otp", otp1);

                params.put("mobile", mobile);
                params.put("otp", otp1);
                params.put("password", pass1);

                Server.get("user/forgot_pass2/format/json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);


                        try {
                            if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {

//                        Toast.makeText(RegisterActivity.this, "Verify Email account", Toast.LENGTH_LONG).show();
//                        sessionManager.setKEY(response.getJSONObject("data").getString("key"));
                                startActivity(new Intent(passwordreset.this, LoginActivity.class));
                                //finish();

                            } else {
                                Toast.makeText(passwordreset.this, response.getString("data"), Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            Toast.makeText(passwordreset.this, "error occurred", Toast.LENGTH_LONG).show();


                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Toast.makeText(passwordreset.this, "error occurred", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);


                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }

                });

            }
             else{
                    Toast.makeText(passwordreset.this, "Check password", Toast.LENGTH_LONG).show();
                }
            }

        });
    }
}
