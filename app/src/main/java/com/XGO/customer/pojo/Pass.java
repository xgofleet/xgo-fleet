package com.XGO.customer.pojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.location.places.Place;

import java.io.Serializable;

/**
 * Created by android on 13/10/17.
 */

public class Pass implements Serializable,Parcelable {
    private Place fromPlace;
    private Place toPlace;
    private String driverId;
    private String driverName;
    private String fare;
    private String vehicleName;
    private String payment ;
    private String goodstype;
    private String insurance;
    private String date;
    private String helper;
    private String time;
    private int flag;
    private String s;
    private String t;
    private String vin;
    private String ride1;
    private String settaxi;
    private Place toPlace2;
    private Place toPlace1;
    private Place toPlace3;
    private Place toPlace4;
    private Place toPlace5;
    private Place toPlace6;
    private String dl1;
    private Place toPlace7;
    private Place toPlace8;
    private Double currentLatitude;
    private Double currentLongitude;
    private Double droplatitude;
    private Double droplongitude;
    private Double droplatitude1;
    private Double droplongitude1;
    private Double droplatitude2;
    private Double droplongitude2;
    private Double droplatitude3;
    private Double droplongitude3;
    private Double droplatitude4;
    private Double droplongitude4;
    private Double droplatitude5;
    private Double droplongitude5;
    private Double droplatitude6;
    private Double droplongitude6;
    private Double droplatitude7;
    private Double droplongitude7;
    private Double droplatitude8;
    private Double droplongitude8;


    public int getFlag()
    {
        return  flag;
    }

    public void setFlag(int flag)
    {
        this.flag=flag;
    }

    public String getDate()
    {
        return  date;
    }

    public void setDate(String date)
    {
        this.date=date;
    }

    public String getRide1()
    {
        return  ride1;
    }

    public void setRide1(String ride1)
    {
        this.ride1=ride1;
    }

    public String getTime()
    {
        return  time;
    }

    public void setTime(String time)
    {
        this.time=time;
    }

    public String getPayment() {
        return payment;
    }

    public void setpayment(String payment) {
        this.payment = payment;
    }

    public String getgoodstype() {
        return goodstype;
    }

    public void setgoodstype(String goodstype) {
        this.goodstype = goodstype;}

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String gethelper() {
        return helper;
    }
    public void sethelper(String helper) {
        this.helper = helper;
    }

    public String gettype() {
        return s;
    }
    public void settype(String s) {
        this.s = s;
    }

    public String gettype1() {
        return t;
    }
    public void settype1(String t) {
        this.t = t;
    }

    public String getdl() {
        return dl1;
    }
    public void setdl(String dl1) {
        this.dl1 = dl1;
    }

    public String gettype2() {
        return vin;
    }
    public void settype2(String vin) {
        this.vin = vin;
    }


    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public Pass() {
    }

    protected Pass(Parcel in) {
        driverId = in.readString();
        driverName = in.readString();
        fare = in.readString();
    }

    public static final Creator<Pass> CREATOR = new Creator<Pass>() {
        @Override
        public Pass createFromParcel(Parcel in) {
            return new Pass(in);
        }

        @Override
        public Pass[] newArray(int size) {
            return new Pass[size];
        }
    };

    public Place getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(Place fromPlace) {
        this.fromPlace = fromPlace;
    }

    public Place getToPlace() {
        return toPlace;
    }

    public void setToPlace(Place toPlace) {
        this.toPlace = toPlace;
    }

    public Place getToPlace1() {
        return toPlace1;
    }

    public void setToPlace1(Place toPlace1) {
        this.toPlace1 = toPlace1;
    }

    public Place getToPlace2() {
        return toPlace2;
    }

    public void setToPlace2(Place toPlace2) {
        this.toPlace2 = toPlace2;
    }



    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(driverId);
        parcel.writeString(driverName);
        parcel.writeString(fare);
    }


    public String gettaxi() {
        return settaxi;
    }

    public void settaxi(String settaxi) {
        this.settaxi = settaxi;
    }
    public Place getToPlace3() {
        return toPlace3;
    }

    public void setToPlace3(Place toPlace3) {
        this.toPlace3 = toPlace3;
    }

    public Place getToPlace4() {
        return toPlace4;
    }

    public void setToPlace4(Place toPlace4) {
        this.toPlace4 = toPlace4;
    }

    public Place getToPlace5() {
        return toPlace5;
    }

    public void setToPlace5(Place toPlace5) {
        this.toPlace5 = toPlace5;
    }

    public Place getToPlace6() {
        return toPlace6;
    }

    public void setToPlace6(Place toPlace6) {
        this.toPlace6 = toPlace6;
    }

    public Place getToPlace7() {
        return toPlace7;
    }

    public void setToPlace7(Place toPlace7) {
        this.toPlace7 = toPlace7;
    }
    public Place getToPlace8() {
        return toPlace8;
    }

    public void setToPlace8(Place toPlace8) {
        this.toPlace8 = toPlace8;
    }

    public void setpickLat(Double currentLatitude) {
        this.currentLatitude = currentLatitude;
    }
    public Double getpickLat() {
        return currentLatitude;
    }

    public void setpickLong(Double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }
    public Double getpickLong() {
        return currentLongitude;
    }

    public void setdropLat(Double droplatitude) {this.droplatitude = droplatitude;
    }
    public Double getdropLat() {
        return droplatitude;
    }

    public void setdropLong(Double droplongitude) {
        this.droplongitude = droplongitude;
    }
    public Double getdropLong() {
        return droplongitude;
    }

    public void setdropLat1(Double droplatitude1) {this.droplatitude1 = droplatitude1;
    }
    public Double getdropLat1() {
        return droplatitude1;
    }

    public void setdropLong1(Double droplongitude1) {
        this.droplongitude1 = droplongitude1;
    }
    public Double getdropLong1() {
        return droplongitude1;
    }


    public void setdropLat2(Double droplatitude2) {this.droplatitude2= droplatitude2;
    }
    public Double getdropLat2() {
        return droplatitude2;
    }

    public void setdropLong2(Double droplongitude2) {
        this.droplongitude2 = droplongitude2;
    }
    public Double getdropLong2() {
        return droplongitude2;
    }

    public void setdropLat3(Double droplatitude3) {this.droplatitude3 = droplatitude3;
    }
    public Double getdropLat3() {
        return droplatitude3;
    }

    public void setdropLong3(Double droplongitude3) {
        this.droplongitude3 = droplongitude3;
    }
    public Double getdropLong3() {
        return droplongitude3;
    }

    public void setdropLat4(Double droplatitude4) {this.droplatitude4 = droplatitude4;
    }
    public Double getdropLat4() {
        return droplatitude4;
    }

    public void setdropLong4(Double droplongitude4) {
        this.droplongitude4 = droplongitude4;
    }
    public Double getdropLong4() {
        return droplongitude4;
    }

    public void setdropLat5(Double droplatitude5) {this.droplatitude5 = droplatitude5;
    }
    public Double getdropLat5() {
        return droplatitude5;
    }

    public void setdropLong5(Double droplongitude5) {
        this.droplongitude5 = droplongitude5;
    }
    public Double getdropLong5() {
        return droplongitude5;
    }

    public void setdropLat6(Double droplatitude6) {this.droplatitude6 = droplatitude6;
    }
    public Double getdropLat6() {
        return droplatitude6;
    }

    public void setdropLong6(Double droplongitude6) {
        this.droplongitude6 = droplongitude6;
    }
    public Double getdropLong6() {
        return droplongitude6;
    }

    public void setdropLat7(Double droplatitude7) {this.droplatitude7 = droplatitude7;
    }
    public Double getdropLat7() {
        return droplatitude7;
    }

    public void setdropLong7(Double droplongitude7) {
        this.droplongitude7 = droplongitude7;
    }
    public Double getdropLong7() {
        return droplongitude7;
    }

    public void setdropLat8(Double droplatitude8) {this.droplatitude8 = droplatitude8;
    }
    public Double getdropLat8() {
        return droplatitude8;
    }

    public void setdropLong8(Double droplongitude8) {
        this.droplongitude8 = droplongitude8;
    }
    public Double getdropLong8() {
        return droplongitude8;
    }
}
