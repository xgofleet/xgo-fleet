package com.XGO.customer.pojo;

import java.io.Serializable;

/**
 * Created by android on 15/3/17.
 */

public class PendingRequestPojo implements Serializable{
    private String ride_id;
    private String add_time;
    private String add_date;
    private String user_id;
    private String driver_id;
    private String pickup_adress;
    private String email;
    private String drop_address;
    private String pikup_location;
    private String drop_locatoin;
    private String distance;
    private String status;
    private String payment_status;
    private String amount;
    private String time;
    private String user_mobile;
    private String user_avatar;
    private String driver_avatar;
    private String user_name;
    private String driver_mobile;
    private String goodstype;
    private Object vehicle_info;
    private String runtime;
    private String helper;
    private String package1;
    private Object price;
    private String pack1;
    private String payment_receipt;
    private String drop_address1;
    private String drop_address2;
    private String drop_address3;
    private String drop_address4;
    private String drop_address5;
    private String drop_address6;
    private String drop_address7;
    private String drop_address8;
    private String drop_locatoin1;
    private String drop_locatoin2;
    private String drop_locatoin3;
    private String drop_locatoin4;
    private String drop_locatoin5;
    private String drop_locatoin6;
    private String drop_locatoin7;
    private String drop_locatoin8;

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    private String payment_mode;
    private String driver_name;


    public String getUser_mobile() {
        return user_mobile;
    }

    public void setUser_mobile(String user_mobile) {
        this.user_mobile = user_mobile;
    }

    public String getUser_email() {
        return email;
    }

    public void setUser_email(String email) {
        this.email = email;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getDriver_avatar() {
        return driver_avatar;
    }

    public void setDriver_avatar(String driver_avatar) {
        this.driver_avatar = driver_avatar;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDriver_mobile() {
        return driver_mobile;
    }

    public void setDriver_mobile(String driver_mobile) {
        this.driver_mobile = driver_mobile;
    }
    public String getDrop_address1() {
        return drop_address1;
    }
    public void setDrop_address1(String drop_address1) {
        this.drop_address1 = drop_address1;
    }

    public String getAdd_date() {
        return add_date;
    }
    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }
    public String getAdd_time() {
        return add_time;
    }
    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }
    public String getDrop_address2() {
        return drop_address2;
    }
    public void setDrop_address2(String drop_address2) {
        this.drop_address2 = drop_address2;
    }

    public String getDrop_address3() {
        return drop_address3;
    }
    public void setDrop_address3(String drop_address3) {
        this.drop_address3 = drop_address3;
    }

    public String getDrop_address4() {
        return drop_address4;
    }
    public void setDrop_address4(String drop_address4) {
        this.drop_address4 = drop_address4;
    }

    public String getDrop_address5() {
        return drop_address5;
    }
    public void setDrop_address5(String drop_address5) {
        this.drop_address5 = drop_address5;
    }

    public String getDrop_address6() {
        return drop_address6;
    }
    public void setDrop_address6(String drop_address6) {
        this.drop_address6 = drop_address6;
    }

    public String getDrop_address7() {
        return drop_address7;
    }
    public void setDrop_address7(String drop_address7) {
        this.drop_address7 = drop_address7;
    }

    public String getDrop_address8() {
        return drop_address8;
    }
    public void setDrop_address8(String drop_address8) {
        this.drop_address8 = drop_address8;
    }
    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }


    public PendingRequestPojo() {
    }

    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getPickup_adress() {
        return pickup_adress;
    }

    public void setPickup_adress(String pickup_adress) {
        this.pickup_adress = pickup_adress;
    }

    public String getDrop_address() {
        return drop_address;
    }

    public void setDrop_address(String drop_address) {
        this.drop_address = drop_address;
    }

    public String getPikup_location() {
        return pikup_location;
    }

    public void setPikup_location(String pikup_location) {
        this.pikup_location = pikup_location;
    }

    public String getDrop_locatoin() {
        return drop_locatoin;
    }

    public void setDrop_locatoin(String drop_locatoin) {
        this.drop_locatoin = drop_locatoin;
    }

    public String getDrop_locatoin1() {
        return drop_locatoin1;
    }

    public void setDrop_locatoin1(String drop_locatoin1) {
        this.drop_locatoin1 = drop_locatoin1;
    }
    public String getDrop_locatoin2() {
        return drop_locatoin2;
    }

    public void setDrop_locatoin2(String drop_locatoin2) {
        this.drop_locatoin2 = drop_locatoin2;
    }
    public String getDrop_locatoin3() {
        return drop_locatoin3;
    }

    public void setDrop_locatoin3(String drop_locatoin3) {
        this.drop_locatoin3 = drop_locatoin3;
    }
    public String getDrop_locatoin4() {
        return drop_locatoin4;
    }

    public void setDrop_locatoin4(String drop_locatoin4) {
        this.drop_locatoin4 = drop_locatoin4;
    }
    public String getDrop_locatoin5() {
        return drop_locatoin5;
    }

    public void setDrop_locatoin5(String drop_locatoin5) {
        this.drop_locatoin5 = drop_locatoin5;
    }
    public String getDrop_locatoin6() {
        return drop_locatoin6;
    }

    public void setDrop_locatoin6(String drop_locatoin6) {
        this.drop_locatoin6 = drop_locatoin6;
    }
    public String getDrop_locatoin7() {
        return drop_locatoin7;
    }

    public void setDrop_locatoin7(String drop_locatoin7) {
        this.drop_locatoin7 = drop_locatoin7;
    }
    public String getDrop_locatoin8() {
        return drop_locatoin8;
    }

    public void setDrop_locatoin8(String drop_locatoin8) {
        this.drop_locatoin8 = drop_locatoin8;
    }
    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getGoodstype() {
        return goodstype;
    }

    public void setGoodstype(String amount) {
        this.goodstype = goodstype;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    public String getruntime() {
        return runtime;
    }
    public void setruntime(String runtime) {
        this.runtime = runtime;
    }


    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public String getvehicle_info() {
        return (String) vehicle_info;
    }

    public void setvehicle_info(String vehicle_info) {
        this.vehicle_info = vehicle_info;
    }

    public void sethelper(String helper) {
        this.helper = helper;
    }

    public String gethelper() {
        return helper;

    }
    public void setpayment_receipt(String payment_receipt) {
        this.payment_receipt = payment_receipt;
    }

    public String getpayment_receipt() {
        return payment_receipt;

    }

    public void setpack1(String pack1) {
        this.pack1 = pack1;
    }

    public String getpack1() {
        return pack1;

    }

    public void setprice(Double price) {
        this.price = price;
    }

    public String getprice() {
        return (String) price;
    }
}
