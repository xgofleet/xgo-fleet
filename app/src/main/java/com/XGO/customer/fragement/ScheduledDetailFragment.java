package com.XGO.customer.fragement;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.XGO.customer.R;
import com.XGO.customer.Server.Server;
import com.XGO.customer.acitivities.HomeActivity;
import com.XGO.customer.custom.CheckConnection;
import com.XGO.customer.custom.GPSTracker;
import com.XGO.customer.custom.SetCustomFont;
import com.XGO.customer.pojo.NearbyData;
import com.XGO.customer.pojo.PendingRequestPojo;
import com.XGO.customer.session.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;
import com.thebrownarrow.permissionhelper.PermissionResult;
import com.thebrownarrow.permissionhelper.PermissionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
//import com.mapbox.services.commons.models.Position;

/**
 * Created by android on 14/3/17.
 */

public class ScheduledDetailFragment extends FragmentManagePermission implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private View view;
    AppCompatButton trackRide;
    private String mobile = "";
    AppCompatButton btn_cancel, btn_payment, btn_complete;
    LinearLayout linearChat;
    TextView title,HELPER21, status,drivername, mobilenumber, pickup_location, drop_location, fare, pay_reciept, payment_status, gtname,txt_goodstype, time1,date1,vdetail,txt_date, fare_text,drop_location1, drop_location2,drop_location3, drop_location4, drop_location5,drop_location6, drop_location7, drop_location8;
    Double fare123;
    SessionManager sessionManager;
    private AlertDialog alert;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final String CONFIG_ENVIRONMENT = Server.ENVIRONMENT;
    private static PayPalConfiguration config;
    PayPalPayment thingToBuy;
    LinearLayout droplocation_row1,droplocation_row2,droplocation_row3,droplocation_row4,droplocation_row5,droplocation_row6,droplocation_row7,droplocation_row8;
    TableRow mobilenumber_row,FARE23,helperlay,Drivern,paystatus,payreciept;
    PendingRequestPojo pojo;
    ImageView select1;
    Double b1,b2,b3,ap1,ap2,ap3,ac1,ac2,ac3,d11,d2,d3,t1,t23,t33;
    String vinfo;
    int runtime;
    Bundle bundle;
    private String helper = "def";
    private String ride_id = "";
    private String paymnt_status = "";
    private String paymnt_mode = "";
    String permissions[] = {PermissionUtils.Manifest_ACCESS_FINE_LOCATION, PermissionUtils.Manifest_ACCESS_COARSE_LOCATION};
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private SwipeRefreshLayout swipeRefreshLayout;
    ImageView driverphone;
    private String scheduledate ="";
    private String scheduletime ="";
    private String pick = "";
    private String drop = "";
    private String drop1 = "";
    private String drop2 = "";
    private String drop3 = "";
    private String drop4 = "";
    private String drop5 = "";
    private String drop6 = "";
    private String drop7 = "";
    private String drop8 = "";

    String runtime1 = "0";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.scheduled_detail_fragmnet, container, false);

        ((HomeActivity) getActivity()).fontToTitleBar(getString(R.string.passenger_info));
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        BindView();
        configPaypal();

        trackRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCompactPermissions(permissions, new PermissionResult() {
                    @Override
                    public void permissionGranted() {
                        if (GPSEnable()) {

                            try {
//                                String[] latlong = pojo.getPikup_location().split(",");
//                                double latitude = Double.parseDouble(latlong[0]);
//                                double longitude = Double.parseDouble(latlong[1]);
//                                String[] latlong1 = pojo.getDrop_locatoin().split(",");
//                                double latitude1 = Double.parseDouble(latlong1[0]);
//                                double longitude1 = Double.parseDouble(latlong1[1]);
//                                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + latitude1 + "," + longitude1));
//                                startActivity(intent);
//                                Position o = Position.fromCoordinates(latitude, longitude);
//                                Position d = Position.fromCoordinates(latitude1, longitude1);
//                                NavigationLauncher.startNavigation(getActivity(), o, d,
//                                        null, true);

                                track fragobj = new track();
                                //  bundle.putString("runtime",runtime1);
                                fragobj.setArguments(bundle);
                                ((HomeActivity) getActivity()).changeFragment(fragobj, "Track Ride");
                            } catch (Exception e) {
                                Toast.makeText(getActivity(), e.toString() + " ", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            turnonGps();
                        }
                    }

                    @Override
                    public void permissionDenied() {

                    }

                    @Override
                    public void permissionForeverDenied() {

                    }
                });
            }
        });

        return view;
    }

    public void NeaBy1() {
        RequestParams params = new RequestParams();
        params.put("ride_id", pojo.getRide_id());
        //  Log.d("sessionkey",""+sessionManager.getKEY());
        SessionManager sessionManager = new SessionManager(getActivity());
        Server.setHeader(sessionManager.getKEY());
        Server.setContetntType();
        Server.get("api/user/fare23/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {

                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new GsonBuilder().create();
                        List<NearbyData> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<NearbyData>>() {

                        }.getType());


                        if (response.has("data") ) {

                            // If you have array
                            // hold your JSON response in String
//


                            JSONArray resultArray1 = response.getJSONArray("data1"); // Here you will get the Array

                            for (int i = 0; i < resultArray1.length(); i++) {
                                // get value with the NODE key
                                JSONObject obj1 = resultArray1.getJSONObject(i);
                                String name1 =  obj1.getString("distance");
                                Double distance = Double.parseDouble(name1);

                                pojo.sethelper(name1);


                                Log.d("distance1",name1);


                            }

                            JSONArray resultArray2 = response.getJSONArray("data2"); // Here you will get the Array

                            //         Iterate the loop
                            for (int i = 0; i < resultArray2.length(); i++) {
                                // get value with the NODE key
                                JSONObject obj2 = resultArray2.getJSONObject(i);
                                String name3 =  obj2.getString("packages");
                                Double distance = Double.parseDouble(name3);

                                pojo.setpack1(name3);


                                Log.d("package",name3);


                            }



                        }




                    }
                } catch (JSONException e) {


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }
    }

    public void BindView() {
        btn_complete = (AppCompatButton) view.findViewById(R.id.btn_complete);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        mobilenumber_row = (TableRow) view.findViewById(R.id.mobilenumber_row);
        FARE23 = (TableRow) view.findViewById(R.id.FARE23);
        Drivern = (TableRow) view.findViewById(R.id.Drivern);
        paystatus = (TableRow) view.findViewById(R.id.paystatus);
        payreciept = (TableRow) view.findViewById(R.id.payreciept);
        linearChat = (LinearLayout) view.findViewById(R.id.linear_chat);
        title = (TextView) view.findViewById(R.id.title);
        status = (TextView) view.findViewById(R.id.status);
      //  drivername = (TextView) view.findViewById(R.id.dn);
       // mobilenumber = (TextView) view.findViewById(R.id.mn);
        pickup_location = (TextView) view.findViewById(R.id.txt_pickuplocation);
        drop_location = (TextView) view.findViewById(R.id.txt_droplocation);
        HELPER21 = (TextView) view.findViewById(R.id.HELPER21);
        fare = (TextView) view.findViewById(R.id.txt_basefare);
        fare_text = (TextView) view.findViewById(R.id.fare_text);
        trackRide = (AppCompatButton) view.findViewById(R.id.btn_trackride);
        btn_payment = (AppCompatButton) view.findViewById(R.id.btn_payment);
        btn_cancel = (AppCompatButton) view.findViewById(R.id.btn_cancel);
        payment_status = (TextView) view.findViewById(R.id.txt_paymentstatus);
        pay_reciept = (TextView) view.findViewById(R.id.txt_payreciept);
        //driverphone = (ImageView) view.findViewById(R.id.driverphone);
        txt_goodstype = (TextView) view.findViewById(R.id.txt_goodstype);
        gtname = (TextView) view.findViewById(R.id.gtname);
        txt_date = (TextView) view.findViewById(R.id.txt_date);
        vdetail = (TextView) view.findViewById(R.id.vdetail1);
        date1 = (TextView) view.findViewById(R.id.date1);
        time1 = (TextView) view.findViewById(R.id.time1);
        helperlay = view.findViewById(R.id.helper);
        droplocation_row1 = view.findViewById(R.id.droplocation_row1);
        droplocation_row2 = view.findViewById(R.id.droplocation_row2);
        droplocation_row3 = view.findViewById(R.id.droplocation_row3);
        droplocation_row4 = view.findViewById(R.id.droplocation_row4);
        droplocation_row5 = view.findViewById(R.id.droplocation_row5);
        droplocation_row6 = view.findViewById(R.id.droplocation_row6);
        droplocation_row7 = view.findViewById(R.id.droplocation_row7);
        droplocation_row8 = view.findViewById(R.id.droplocation_row8);
        drop_location1 = (TextView) view.findViewById(R.id.txt_droplocation1);
        drop_location2= (TextView) view.findViewById(R.id.txt_droplocation2);
        drop_location3 = (TextView) view.findViewById(R.id.txt_droplocation3);
        drop_location4 = (TextView) view.findViewById(R.id.txt_droplocation4);
        drop_location5 = (TextView) view.findViewById(R.id.txt_droplocation5);
        drop_location6 = (TextView) view.findViewById(R.id.txt_droplocation6);
        drop_location7 = (TextView) view.findViewById(R.id.txt_droplocation7);
        drop_location8 = (TextView) view.findViewById(R.id.txt_droplocation8);
        pickup_location.setSelected(true);
        drop_location.setSelected(true);
        drop_location1.setSelected(true);
        drop_location2.setSelected(true);
        drop_location3.setSelected(true);
        drop_location4.setSelected(true);
        drop_location5.setSelected(true);
        drop_location6.setSelected(true);
        drop_location7.setSelected(true);
        drop_location8.setSelected(true);
        sessionManager = new SessionManager(getActivity());
         bundle = getArguments();
        if (bundle != null) {
            pojo = (PendingRequestPojo) bundle.getSerializable("data");
            title.setText(getString(R.string.taxi));
            pick = pojo.getPickup_adress();
            scheduledate = pojo.getAdd_date();
            scheduletime = pojo.getAdd_time();
            pickup_location.setText(pick);
            drop = pojo.getDrop_address();
            drop_location.setText(drop);
           // drivername.setText("Will be Updated");
            fare.setText("₹" + " " + pojo.getAmount());
            txt_date.setText(pojo.getTime() + "");
            payment_status.setText("Pending");
            date1.setText(scheduledate);
           time1.setText(scheduletime);
           // mobile = mobilenumber.getText().toString();
            vinfo = pojo.getvehicle_info();
            droplocation_row1.setVisibility(View.GONE);
            droplocation_row2.setVisibility(View.GONE);
            droplocation_row3.setVisibility(View.GONE);
            droplocation_row4.setVisibility(View.GONE);
            droplocation_row5.setVisibility(View.GONE);
            droplocation_row6.setVisibility(View.GONE);
            droplocation_row7.setVisibility(View.GONE);
            droplocation_row8.setVisibility(View.GONE);
            drop1 = pojo.getDrop_address1();
            drop2 = pojo.getDrop_address2();
            drop3 = pojo.getDrop_address3();
            drop4 = pojo.getDrop_address4();
            drop5 = pojo.getDrop_address5();
            drop6 = pojo.getDrop_address6();
            drop7 = pojo.getDrop_address7();
            drop8 = pojo.getDrop_address8();
            helper =pojo.gethelper();
            paymnt_status = pojo.getPayment_status();
            ride_id = pojo.getRide_id();
            Log.d("drop1", ""+pojo.getDrop_address1());
            status.setText(pojo.getStatus()+"");
            int vinfo1 = Integer.parseInt(vinfo);
            if (vinfo1 == 1) {
                vdetail.setText("BIKE");
            }
            if (vinfo1 == 2) {
                vdetail.setText("APE");

            }
            if (vinfo1 == 3) {
                vdetail.setText("ACE");

            }
            if (vinfo1 == 4) {
                vdetail.setText("DOST");
            }
            if (vinfo1 == 5) {
                vdetail.setText("407");
            }
            paymnt_mode = pojo.getPayment_mode();



            if (!drop1.equals("def")) {
                droplocation_row1.setVisibility(View.VISIBLE);
                drop_location1.setText(drop1);
            }
            if (!drop2.equals("def")) {
                droplocation_row2.setVisibility(View.VISIBLE);
                drop_location2.setText(drop2);
            }
            if (!drop3.equals("def")) {
                droplocation_row3.setVisibility(View.VISIBLE);
                drop_location3.setText(drop3);
            }
            if (!drop4.equals("def")) {
                droplocation_row4.setVisibility(View.VISIBLE);
                drop_location4.setText(drop4);
            }
            if (!drop5.equals("def")) {
                droplocation_row5.setVisibility(View.VISIBLE);
                drop_location5.setText(drop5);
            }
            if (!drop6.equals("def")) {
                droplocation_row6.setVisibility(View.VISIBLE);
                drop_location6.setText(drop6);
            }
            if (!drop7.equals("def")) {
                droplocation_row7.setVisibility(View.VISIBLE);
                drop_location7.setText(drop7);
            }
            if (!drop8.equals("def")) {
                droplocation_row8.setVisibility(View.VISIBLE);
                drop_location8.setText(drop8);
            }

            if (mobile != null) {
               // mobilenumber.setText(mobile);
            }
            if (paymnt_mode == null) {
                paymnt_mode = "";
            }

            if(helper.equals("def"))
            {
                helperlay.setVisibility(View.GONE);
            }

            if(!helper.equals("def"))
            {
                helperlay.setVisibility(View.VISIBLE);
                HELPER21.setText("Required");
            }
            if (ride_id != null) {
                title.setText("Ride ID : " +ride_id);

            }
            String gt = pojo.getGoodstype();
            if(gt.equals("taxi"))
            {
                gtname.setText("Ride Type");
                txt_goodstype.setText(gt + " ");

            }
            if(!gt.equals("taxi"))
            {
                gtname.setText("Goods Type");
                txt_goodstype.setText(gt + " ");

            }
            if (pojo.getStatus().equalsIgnoreCase("PENDING")) {
                btn_cancel.setVisibility(View.VISIBLE);

            }
            if (pojo.getStatus().equalsIgnoreCase("CANCELLED")) {
                btn_complete.setVisibility(View.GONE);
                btn_cancel.setVisibility(View.GONE);
                btn_payment.setVisibility(View.GONE);
                trackRide.setVisibility(View.GONE);
            }
            if (pojo.getStatus().equalsIgnoreCase("ARRIVED")) {
                btn_complete.setVisibility(View.GONE);
                btn_cancel.setVisibility(View.VISIBLE);
                btn_payment.setVisibility(View.GONE);
                trackRide.setVisibility(View.VISIBLE);
                payment_status.setText("Pending");
            }
            if (pojo.getStatus().equalsIgnoreCase("STARTED")) {
                btn_complete.setVisibility(View.GONE);
                btn_cancel.setVisibility(View.VISIBLE);
                btn_payment.setVisibility(View.GONE);
                trackRide.setVisibility(View.VISIBLE);
                payment_status.setText("Pending");
            }
            if (pojo.getStatus().equalsIgnoreCase("COMPLETED")) {
                FARE23.setVisibility(View.GONE);

                btn_payment.setVisibility(View.VISIBLE);
                trackRide.setVisibility(View.GONE);
                btn_cancel.setVisibility(View.GONE);
                btn_complete.setVisibility(View.GONE);

                if (pojo.getPayment_status().equals("PAID") ) {
                    FARE23.setVisibility(View.VISIBLE);
                    fare_text.setText("Total Fare");
                    btn_payment.setVisibility(View.GONE);
                    paystatus.setVisibility(View.VISIBLE);
                    payreciept.setVisibility(View.VISIBLE);

                    payment_status.setText("PAID");
                    if(pojo.getPayment_mode().equalsIgnoreCase("OFFLINE")) {
                        pay_reciept.setText("Paid to driver");
                    }

                    if(!pojo.getPayment_mode().equalsIgnoreCase("OFFLINE")) {
                        payreciept.setVisibility(View.VISIBLE);
                        pay_reciept.setText(pojo.getpayment_receipt().toString());
                    }
                 //   mobilenumber_row.setVisibility(View.GONE);
                }

            }

            if (pojo.getStatus().equalsIgnoreCase("ACCEPTED")) {
                payment_status.setVisibility(View.VISIBLE);
                if (pojo.getPayment_status().equals("") && pojo.getPayment_mode().equals("")) {

                    btn_cancel.setVisibility(View.VISIBLE);
                    trackRide.setVisibility(View.VISIBLE);
                    btn_payment.setVisibility(View.GONE);
                    payment_status.setText("Pending");
                } else {
                    btn_complete.setVisibility(View.GONE);
                    trackRide.setVisibility(View.VISIBLE);
                    mobilenumber_row.setVisibility(View.VISIBLE);
                }
                if (!pojo.getPayment_status().equals("PAID") && pojo.getPayment_mode().equals("OFFLINE")) {
                    trackRide.setVisibility(View.VISIBLE);

                    btn_complete.setVisibility(View.GONE);
                    payment_status.setText("Pending");
                } else {
                }


            }

            if (pojo.getPayment_status().equals("") && pojo.getPayment_mode().equals("")) {

                payment_status.setText(getString(R.string.unpaid));

            } else {
                payment_status.setText(pojo.getPayment_status());

            }
            if (!pojo.getPayment_status().equals("PAID") && pojo.getPayment_mode().equals("OFFLINE")) {
                payment_status.setText(R.string.cash_on_hand);

            } else {
                payment_status.setText(pojo.getPayment_status());
            }
        }

        SetCustomFont setCustomFont = new SetCustomFont();
        setCustomFont.overrideFonts(getActivity(), view);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

       /* linearChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("name", pojo.getDriver_name());
                b.putString("id", pojo.getRide_id());
                b.putString("user_id", pojo.getDriver_id());
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setArguments(b);
                ((HomeActivity) getActivity()).changeFragment(chatFragment, "Messages");
            }
        });*/
//        driverphone.set


        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckConnection.haveNetworkConnection(getActivity())) {

                    new AlertDialog.Builder(getActivity()).setIcon(R.drawable.xgo_icon).setTitle(Html.fromHtml("<font color='#4678F6'>PAYMENT METHOD</font>")).setItems(R.array.payment_mode, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                paym fragobj = new paym();
                                //  bundle.putString("runtime",runtime1);
                                fragobj.setArguments(bundle);
                                ((HomeActivity) getActivity()).changeFragment(fragobj, "Approve Payment");
                                RequestParams params = new RequestParams();
                                params.put("ride_id", pojo.getRide_id());
                                params.put("payment_mode", "OFFLINE");
                                Server.setContetntType();
                                Server.setHeader(sessionManager.getKEY());
                                Server.post("api/user/rides", params, new JsonHttpResponseHandler() {
                                    @Override
                                    public void onStart() {
                                        swipeRefreshLayout.setRefreshing(true);
                                    }

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        super.onSuccess(statusCode, headers, response);

                                        pojo.setPayment_mode("OFFLINE");
                                        if (pojo.getPayment_mode().equals("OFFLINE")) {
                                            payment_status.setText(R.string.cash_on_hand);
                                        } else {
                                            payment_status.setText(pojo.getPayment_status());


                                        }

                                        btn_payment.setVisibility(View.GONE);
                                        Toast.makeText(getActivity(), getString(R.string.payment_update), Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                        super.onFailure(statusCode, headers, responseString, throwable);

                                    }

                                    @Override
                                    public void onFinish() {
                                        super.onFinish();
                                        if (getActivity() != null) {
                                            swipeRefreshLayout.setRefreshing(false);
                                        }
                                    }
                                });

                            } else {
                                MakePayment();
                                NeaBy1();
                            }
                        }
                    }).create().show();

                    //MakePayment();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.network), Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialogCreate(getString(R.string.ride_request_cancellation), getString(R.string.want_to_cancel), "CANCELLED");
            }
        });


        btn_complete.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialogCreate(getString(R.string.ride_request_com), getString(R.string.want_to_accept), "COMPLETED");
                    }
                });
    }


    public void Updatepayemt(String ride_id, String payment_status) {
        RequestParams params = new RequestParams();
        params.put("ride_id", ride_id);
        params.put("payment_status", payment_status);
        params.put("payment_mode", "PAYPAL");
        Server.setContetntType();
        Server.setHeader(sessionManager.getKEY());

        Server.post("api/user/rides", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Toast.makeText(getActivity(), getString(R.string.payment_update), Toast.LENGTH_LONG).show();
                ((HomeActivity) getActivity()).onBackPressed();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

                Toast.makeText(getActivity(), getString(R.string.error_payment), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getActivity(), getString(R.string.server_not_respond), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                    swipeRefreshLayout.setRefreshing(false);

                }
            }
        });

    }


    public void AlertDialogCreate(String title, String message, final String status) {
        Drawable drawable = ContextCompat.getDrawable(getActivity(), R.mipmap.ic_warning_white_24dp);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.RED);
        new AlertDialog.Builder(getActivity())
                .setIcon(drawable)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(getString(R.string.cancel), null)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sendStatus(pojo.getRide_id(), status);

                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    public void cancelAlert(String title, String message, final String status) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title);

        alertDialog.setMessage(message);
        alertDialog.setCancelable(true);
        Drawable drawable = ContextCompat.getDrawable(getActivity(), R.mipmap.ic_warning_white_24dp);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.RED);
        alertDialog.setIcon(drawable);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendStatus(pojo.getRide_id(), status);

            }
        });


        alertDialog.setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alert.cancel();
            }
        });
        alert = alertDialog.create();
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alert.show();
    }

    public void sendStatus(String ride_id, final String status) {

        RequestParams params = new RequestParams();
        params.put("ride_id", ride_id);
        params.put("status", status);
        SessionManager sessionManager = new SessionManager(getActivity());
        Server.setHeader(sessionManager.getKEY());
        Server.setContetntType();
        Server.get("api/user/fridescancel", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    ScheduledRequestfragment acceptedRequestFragment = new ScheduledRequestfragment();
                    Bundle bundle = null;
                    if (response.has("status") && response.getString("status").equals("success")) {
                        if (status.equalsIgnoreCase("COMPLETED")) {
                            ((HomeActivity) getActivity()).changeFragment(new HomeFragment(), getString(R.string.home));

                        } else {
                            Toast.makeText(getActivity(), getString(R.string.ride_request_cancelled), Toast.LENGTH_LONG).show();

                        }
                        acceptedRequestFragment.setArguments(bundle);
                        ((HomeActivity) getActivity()).changeFragment(new HomeFragment(), getString(R.string.home));
                    } else {
                        String error = response.getString("data");
                        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.error_occurred), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), getString(R.string.error_occurred), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

    }

    public void configPaypal() {
        config = new PayPalConfiguration()
                .environment(CONFIG_ENVIRONMENT)
                .clientId(Server.PAYPAL_KEY)
                .merchantName(getString(R.string.merchant_name))
                .merchantPrivacyPolicyUri(
                        Uri.parse(getString(R.string.merchant_privacy)))
                .merchantUserAgreementUri(
                        Uri.parse(getString(R.string.merchant_user_agreement)));
    }

    public void MakePayment() {

        if (pojo.getAmount() != null && !pojo.getAmount().equals("")) {


            final RequestParams params = new RequestParams();
            params.put("ride_id", pojo.getRide_id());
            Log.d("ride_id", pojo.getRide_id());
            params.put("vehicle_info",vinfo);

            Server.setHeader(sessionManager.getKEY());

            Server.get("api/user/runtime/format/json", params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    String jsonStr = null;
                    try {
                        Gson gson = new GsonBuilder().create();

                        if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                            List<PendingRequestPojo> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<PendingRequestPojo>>() {
                            }.getType());
                            if (response.has("data") ) {


                                // If you have array
                                  // hold your JSON response in String

                                JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array

                        //         Iterate the loop
                                for (int i = 0; i < resultArray.length(); i++) {
                                    // get value with the NODE key
                                    JSONObject obj = resultArray.getJSONObject(i);
                                    String name =  obj.getString("runtime");
                                   runtime = Integer.parseInt(name);

                                    pojo.setruntime(name);





                                }
                                String error = response.getString("data");

                                JSONArray resultArray1 = response.getJSONArray("data1"); // Here you will get the Array

                                //         Iterate the loop
                                for (int i = 0; i < resultArray1.length(); i++) {
                                    // get value with the NODE key
                                    JSONObject obj1 = resultArray1.getJSONObject(i);
                                    String name1 =  obj1.getString("distance");
                                    Double distance = Double.parseDouble(name1);

                                    pojo.setDistance(name1);


Log.d("distance1",name1);


                                }

                                JSONArray resultArray2 = response.getJSONArray("data2"); // Here you will get the Array

                                //         Iterate the loop
                                for (int i = 0; i < resultArray2.length(); i++) {
                                    // get value with the NODE key
                                    JSONObject obj2 = resultArray2.getJSONObject(i);
                                    String name2 =  obj2.getString("vehicle_info");
                                    Double distance = Double.parseDouble(name2);

                                    pojo.setvehicle_info(name2);


                                    Log.d("vinfo34",name2);


                                }


                            } else {

                            }


                        } else {

                            Toast.makeText(getActivity(),"hi", Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {

                        Toast.makeText(getActivity(), getString(R.string.contact_admin), Toast.LENGTH_LONG).show();


                    }

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    if (getActivity() != null) {
                    }
                }
            });
        }
        razorpay fragobj = new razorpay();
        //  bundle.putString("runtime",runtime1);
        fragobj.setArguments(bundle);
        ((HomeActivity) getActivity()).changeFragment(fragobj, getString(R.string.payment));



//            Intent intent = new Intent(getActivity(), payment.class);
//            startActivity(intent);

        }


    public void getruntime23() {

        final RequestParams params = new RequestParams();
        params.put("ride_id", pojo.getRide_id());
        Server.setHeader(sessionManager.getKEY());

        Log.d("sessionManager.getKEY()","" +sessionManager.getKEY());
        Server.get("api/user/fare23/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Log.d("fare", "hmm");

                try {
                    Gson gson = new GsonBuilder().create();

                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        List<PendingRequestPojo> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<PendingRequestPojo>>() {
                        }.getType());


                        if (response.has("data") ) {





                            JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array

                            String error = response.getString("data");

                            JSONArray resultArray1 = response.getJSONArray("data1"); // Here you will get the Array

                            //         Iterate the loop
                            for (int i = 0; i < resultArray1.length(); i++) {
                                // get value with the NODE key
                                JSONObject obj1 = resultArray1.getJSONObject(i);
                                String name1 =  obj1.getString("helper");
                                Double distance = Double.parseDouble(name1);

                                pojo.sethelper(name1);


                                Log.d("distance1",name1);


                            }

                            JSONArray resultArray2 = response.getJSONArray("data2"); // Here you will get the Array

                            //         Iterate the loop
                            for (int i = 0; i < resultArray2.length(); i++) {
                                // get value with the NODE key
                                JSONObject obj2 = resultArray2.getJSONObject(i);
                                String name3 =  obj2.getString("packages");
                                Double distance = Double.parseDouble(name3);

                                pojo.setpack1(name3);


                                Log.d("package",name3);


                            }

                            Log.d("fare", error);
                        } else {

                        }

                    } else {

                        Toast.makeText(getActivity(), getString(R.string.contact_admin), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {

                    Toast.makeText(getActivity(), getString(R.string.contact_admin), Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                }
            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                //  String.valueOf(finalfare)
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject()
                                .toString(4));
                        Updatepayemt(pojo.getRide_id(), "PAID");

                    } catch (JSONException e) {

                        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), getString(R.string.payment_hbeen_cancelled), Toast.LENGTH_LONG).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(getActivity(), getString(R.string.error_occurred), Toast.LENGTH_LONG).show();
                //  Log.d("payment", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject()
                                .toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.d("FuturePaymentExample", authorization_code);

                        /*sendAuthorizationToServer(auth);
                        Toast.makeText(getActivity(),
                                "Future Payment code received from PayPal",
                                Toast.LENGTH_LONG).show();*/
                        Log.e("paypal", "future Payment code received from PayPal  :" + authorization_code);

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), "failure Occurred", Toast.LENGTH_LONG).show();
                        Log.e("FuturePaymentExample",
                                "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), getString(R.string.payment_hbeen_cancelled), Toast.LENGTH_LONG).show();

                Log.d("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {

                Toast.makeText(getActivity(), getString(R.string.error_occurred), Toast.LENGTH_LONG).show();

                Log.d("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }

    }

    public Boolean GPSEnable() {
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        if (gpsTracker.canGetLocation()) {
            return true;

        } else {
            return false;
        }


    }

    public void turnonGps() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(30 * 1000);
            mLocationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and setting the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {

                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
