package com.XGO.customer.fragement;

import android.graphics.Bitmap;
import android.net.Uri;

class ContactModel {
    public String id;
    public String name;
    public String mobileNumber;
    public Bitmap photo;
    public Uri photoURI;
}
