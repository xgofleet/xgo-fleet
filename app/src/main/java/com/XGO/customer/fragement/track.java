package com.XGO.customer.fragement;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.XGO.customer.pojo.PendingRequestPojo;
import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.model.Direction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.XGO.customer.Server.Server;
import com.XGO.customer.custom.CheckConnection;
import com.XGO.customer.pojo.NearbyData;
import com.XGO.customer.session.SessionManager;
import com.loopj.android.http.JsonHttpResponseHandler;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import com.XGO.customer.R;
import com.XGO.customer.acitivities.HomeActivity;
import com.XGO.customer.custom.GPSTracker;
import com.loopj.android.http.RequestParams;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;
import com.thebrownarrow.permissionhelper.PermissionResult;
import com.thebrownarrow.permissionhelper.PermissionUtils;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


/**
 * Created by android on 7/3/17.
 */

public class track extends FragmentManagePermission implements OnMapReadyCallback, View.OnLongClickListener, DirectionCallback, Animation.AnimationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public JSONArray value;
    String tinfo;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1234;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Double currentLatitude, droplatitude, droplatitude1, droplatitude2, droplatitude3, droplatitude4, droplatitude5, droplatitude6, droplatitude7, droplatitude8, droplatitude9, droplatitude10;
    private Double currentLongitude, droplongitude, droplongitude1, droplongitude2, droplongitude3, droplongitude4, droplongitude5, droplongitude6, droplongitude7, droplongitude8, droplongitude9, droplongitude10;
    private Double currentLatitude1 = 0.0;
    private Double currentLongitude1 = 0.0;
    private View rootView;
    Bundle bundle;
    private final int FIVE_SECONDS = 5000;
    Location loc1;
    Boolean flag = false;
    GoogleMap myMap1;
    MapView mview;
    PendingRequestPojo pojo;

    TextView pickup_location, drop_location, drop_location1, drop_location2, drop_location3, drop_location4, drop_location5, drop_location6, drop_location7, drop_location8, goodstext;

    SessionManager sessionManager;
    String[] permissionAsk = {PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE, PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_ACCESS_FINE_LOCATION, PermissionUtils.Manifest_ACCESS_COARSE_LOCATION};
    MapView mMapView;
    com.google.android.gms.location.places.Place pickup;
    ProgressBar progressBar;
    Marker marker1, marker2, marker3, marker4, marker5, marker6, marker7, marker8, marker9, marker10;
    String drop = "";
    String drop1 = "";
    String drop2 = "";
    String drop3 = "";
    String drop4 = "";
    String drop5 = "";
    String drop6 = "";
    String drop7 = "";
    String drop8 = "";

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //  MapsInitializer.initialize(this.getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            rootView = inflater.inflate(R.layout.track, container, false);
            ((HomeActivity) getActivity()).fontToTitleBar("Track Ride");
            bindView(savedInstanceState);
            if (!CheckConnection.haveNetworkConnection(getActivity())) {
                Toast.makeText(getActivity(), getString(R.string.network), Toast.LENGTH_LONG).show();
            }
//            String apiKey = "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M";
//            Places.initialize(getActivity(), apiKey);
            scheduleSendLocation();
            NeaBy1();
            setCurrentLocation();
            //  getdetail.setVisibility(View.VISIBLE);
            mGoogleApiClient = new GoogleApiClient
                    .Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            onBackPressed1();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                askCompactPermissions(permissionAsk, new PermissionResult() {
                    @Override
                    public void permissionGranted() {
                        if (!GPSEnable()) {
                            tunonGps();
                        } else {
                            getcurrentlocation();
                        }
                    }

                    @Override
                    public void permissionDenied() {

                    }

                    @Override
                    public void permissionForeverDenied() {

                        openSettingsApp(getActivity());
                    }
                });

            } else {
                if (!GPSEnable()) {
                    tunonGps();
                } else {
                    getcurrentlocation();
                }

            }






        } catch (InflateException e) {

        }

        return rootView;


    }

    public void onBackPressed1() {


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("result");
                getcurrentlocation();
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }  else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//                drop = PlaceAutocomplete.getPlace(getActivity(), data).freeze();
//                drop_location.setText(drop.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                //  Status status = AutocompleteActivity.(getActivity(), data);
                //   Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null) {
            mMapView.onPause();
        }
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.getFusedLocationProviderClient(getContext());
                mGoogleApiClient.disconnect();
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null) {
            mMapView.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mMapView != null) {

            mMapView.onStart();
            if (currentLatitude != null && !currentLatitude.equals(0.0) && currentLongitude != null && !currentLongitude.equals(0.0)) {

            }
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {

            mMapView.onResume();

        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }





    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {


    }

    @Override
    public void onDirectionFailure(Throwable t) {

    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @SuppressLint("ClickableViewAccessibility")
    public void bindView(Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        mMapView = rootView.findViewById(R.id.mapview);
if(mMapView != null)
{
    mMapView.onCreate(null);
    mMapView.onResume();
    mMapView.getMapAsync(this);
}
        progressBar = rootView.findViewById(R.id.progressBar);

        //getdetail = rootView.findViewById(R.id.getdetail);
        sessionManager = new SessionManager(getActivity());

        Bundle bundle = getArguments();

        pojo = (PendingRequestPojo) bundle.getSerializable("data");
        Log.d("Ride_id",""+pojo.getRide_id()+"");
        drop = pojo.getDrop_address();
        drop1 = pojo.getDrop_address1();
        drop2 = pojo.getDrop_address2();
        drop3 = pojo.getDrop_address3();
        drop4 = pojo.getDrop_address4();
        drop5 = pojo.getDrop_address5();
        drop6 = pojo.getDrop_address6();
        drop7 = pojo.getDrop_address7();
        drop8 = pojo.getDrop_address8();

        // mGeoDataClient = Places.getGeoDataClient(getContext(),null);
        // Construct a PlaceDetectionClient.
        //    mPlaceDetectionClient = Places.getPlaceDetectionClient(getContext(),null);
        //Mapbox.getInstance(getContext() , "pk.eyJ1IjoiaWNhbnN0dWRpb3oiLCJhIjoiY2oyMXQ3dGRpMDAwdDJ3bXpmZHRkdTBtNyJ9.PxslIcrVRj_gVgiv-Y-jog");


//    Places.initialize(getApplicationContext(), "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M");
////Places.initialize(getApplicationContext(), "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M");
//        // mMapView.getMapAsync(this);
//        PlacesClient placesClient = Places.createClient(getContext());

        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        myMap1.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));
        // Zoom in, animating the camera.
        myMap1.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        myMap1.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        int result = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            //     android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            FusedLocationProviderClient location = LocationServices.getFusedLocationProviderClient(getContext());

//            if (location == null) {
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//            } else {
//                //If everything went fine lets get latitude and longitude
//                currentLatitude = location.getLatitude();
//                currentLongitude = location.getLongitude();
//                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
//                    if (!flag) {
//                        NeaBy(String.valueOf(currentLatitude), String.valueOf(currentLongitude));
//                    }

            location.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                currentLatitude = location.getLatitude();
                                currentLongitude = location.getLongitude();


                                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
                                    if (!flag) {
                                    }
                                }
                            } else {

                                Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        } else {
            askCompactPermissions(permissionAsk, new PermissionResult() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionDenied() {
                }

                @Override
                public void permissionForeverDenied() {
                    Snackbar.make(rootView, getString(R.string.allow_permission), Snackbar.LENGTH_LONG).show();
                    openSettingsApp(getActivity());
                }
            });

        }


    }

    private void setCurrentLocation() {
        if (!GPSEnable()) {
            tunonGps();

        } else {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {


                try {
                    int result = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        //     android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        FusedLocationProviderClient location = LocationServices.getFusedLocationProviderClient(getContext());

                        location.getLastLocation()
                                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations this can be null.
                                        if (location != null) {
                                            // Logic to handle location object
                                            currentLatitude = location.getLatitude();
                                            currentLongitude = location.getLongitude();


                                            loc1 = location;
                                            try {
                                                List<Address> address3,address23;
                                                List<Place> pal;
                                                Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
                                                address3 = geocoder1.getFromLocation(currentLatitude,currentLongitude,1);

                                                Log.d("Address3",""+address3.get(0).getAddressLine(0).toString());
                                                pickup_location.setText(address3.get(0).getAddressLine(0));

                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
                                                LatLng latLng1 = new LatLng(currentLatitude, currentLongitude);
                                                myMap1.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


                                            }
                                        } else {

                                            Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                        String placeId = "INSERT_PLACE_ID_HERE";



// Call findCurrentPlace and handle the response (first check that the user has granted permission).


                    } else {
                        askCompactPermissions(permissionAsk, new PermissionResult() {
                            @Override
                            public void permissionGranted() {

                            }

                            @Override
                            public void permissionDenied() {
                            }

                            @Override
                            public void permissionForeverDenied() {
                                Snackbar.make(rootView, getString(R.string.allow_permission), Snackbar.LENGTH_LONG).show();
                                openSettingsApp(getActivity());
                            }
                        });

                    }
                } catch (Exception e) {

                }

            }
        }





    }






    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {

                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
        }

    }



    public void applyfonts() {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "font/AvenirLTStd_Medium.otf");
        Typeface font1 = Typeface.createFromAsset(getActivity().getAssets(), "font/AvenirLTStd_Book.otf");
        pickup_location.setTypeface(font);
        drop_location.setTypeface(font);
//        txt_vehicleinfo.setTypeface(font1);
//        rate.setTypeface(font1);

//        txt_color.setTypeface(font);
//        txt_address.setTypeface(font);
//        request_ride.setTypeface(font1);


    }



    public void getcurrentlocation() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
    }


    public void tunonGps() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(30 * 1000);
            mLocationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            getcurrentlocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and setting the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

    }

    public Boolean GPSEnable() {
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        return gpsTracker.canGetLocation();


    }

    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());
        myMap1 = googleMap;
        myMap1.setMapType(GoogleMap.MAP_TYPE_NORMAL);
         String[] latlong0 = pojo.getPikup_location().split(",");

        currentLatitude = Double.parseDouble(latlong0[0]);
        currentLongitude = Double.parseDouble(latlong0[1]);

        myMap1.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Pickup Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_light)));
        myMap1.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude),15));

        String[] latlong = pojo.getDrop_locatoin().split(",");

        droplatitude = Double.parseDouble(latlong[0]);
        droplongitude = Double.parseDouble(latlong[1]);

        myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude, droplongitude)).title("Drop Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

          if (pojo.getvehicle_info() == "1") {
              marker1 = myMap1.addMarker(new MarkerOptions().position(new LatLng(currentLatitude1, currentLongitude1)).title("Driver Location").icon(BitmapDescriptorFactory.fromResource(R.mipmap.bike)));

          } else {
              marker1 = myMap1.addMarker(new MarkerOptions().position(new LatLng(currentLatitude1, currentLongitude1)).title("Driver Location").icon(BitmapDescriptorFactory.fromResource(R.mipmap.truck)));

          }




        if (!drop1.equals("def")) {
            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


        }
        if (!drop2.equals("def")) {

            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[0]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

        }
        if (!drop3.equals("def")) {

            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong4 = pojo.getDrop_locatoin3().split(",");

            droplatitude3 = Double.parseDouble(latlong4[0]);
            droplongitude3 = Double.parseDouble(latlong4[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude3, droplongitude3)).title("Drop Location 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


        }
        if (!drop4.equals("def")) {

            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong4 = pojo.getDrop_locatoin3().split(",");

            droplatitude3 = Double.parseDouble(latlong4[0]);
            droplongitude3 = Double.parseDouble(latlong4[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude3, droplongitude3)).title("Drop Location 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong5 = pojo.getDrop_locatoin4().split(",");

            droplatitude4 = Double.parseDouble(latlong5[0]);
            droplongitude4 = Double.parseDouble(latlong5[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude4, droplongitude4)).title("Drop Location 4").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));
        }
        if (!drop5.equals("def")) {

            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong4 = pojo.getDrop_locatoin3().split(",");

            droplatitude3 = Double.parseDouble(latlong4[0]);
            droplongitude3 = Double.parseDouble(latlong4[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude3, droplongitude3)).title("Drop Location 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong5 = pojo.getDrop_locatoin4().split(",");

            droplatitude4 = Double.parseDouble(latlong5[0]);
            droplongitude4 = Double.parseDouble(latlong5[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude4, droplongitude4)).title("Drop Location 4").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong6 = pojo.getDrop_locatoin5().split(",");

            droplatitude5 = Double.parseDouble(latlong6[0]);
            droplongitude5 = Double.parseDouble(latlong6[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude5, droplongitude5)).title("Drop Location 5").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

        }
        if (!drop6.equals("def")) {

            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong4 = pojo.getDrop_locatoin3().split(",");

            droplatitude3 = Double.parseDouble(latlong4[0]);
            droplongitude3 = Double.parseDouble(latlong4[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude3, droplongitude3)).title("Drop Location 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong5 = pojo.getDrop_locatoin4().split(",");

            droplatitude4 = Double.parseDouble(latlong5[0]);
            droplongitude4 = Double.parseDouble(latlong5[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude4, droplongitude4)).title("Drop Location 4").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong6 = pojo.getDrop_locatoin5().split(",");

            droplatitude5 = Double.parseDouble(latlong6[0]);
            droplongitude5 = Double.parseDouble(latlong6[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude5, droplongitude5)).title("Drop Location 5").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong7 = pojo.getDrop_locatoin6().split(",");

            droplatitude6 = Double.parseDouble(latlong7[0]);
            droplongitude6 = Double.parseDouble(latlong7[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude6, droplongitude6)).title("Drop Location 6").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

        }
        if (!drop7.equals("def")) {
            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong4 = pojo.getDrop_locatoin3().split(",");

            droplatitude3 = Double.parseDouble(latlong4[0]);
            droplongitude3 = Double.parseDouble(latlong4[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude3, droplongitude3)).title("Drop Location 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong5 = pojo.getDrop_locatoin4().split(",");

            droplatitude4 = Double.parseDouble(latlong5[0]);
            droplongitude4 = Double.parseDouble(latlong5[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude4, droplongitude4)).title("Drop Location 4").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong6 = pojo.getDrop_locatoin5().split(",");

            droplatitude5 = Double.parseDouble(latlong6[0]);
            droplongitude5 = Double.parseDouble(latlong6[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude5, droplongitude5)).title("Drop Location 5").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong7 = pojo.getDrop_locatoin6().split(",");

            droplatitude6 = Double.parseDouble(latlong7[0]);
            droplongitude6 = Double.parseDouble(latlong7[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude6, droplongitude6)).title("Drop Location 6").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong8 = pojo.getDrop_locatoin7().split(",");

            droplatitude7 = Double.parseDouble(latlong8[0]);
            droplongitude7 = Double.parseDouble(latlong8[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude7, droplongitude7)).title("Drop Location 7").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

        }
        if (!drop8.equals("def")) {
            String[] latlong2 = pojo.getDrop_locatoin1().split(",");

            droplatitude1 = Double.parseDouble(latlong2[0]);
            droplongitude1 = Double.parseDouble(latlong2[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude1, droplongitude1)).title("Drop Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong3 = pojo.getDrop_locatoin2().split(",");

            droplatitude2 = Double.parseDouble(latlong3[0]);
            droplongitude2 = Double.parseDouble(latlong3[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude2, droplongitude2)).title("Drop Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong4 = pojo.getDrop_locatoin3().split(",");

            droplatitude3 = Double.parseDouble(latlong4[0]);
            droplongitude3 = Double.parseDouble(latlong4[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude3, droplongitude3)).title("Drop Location 3").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));


            String[] latlong5 = pojo.getDrop_locatoin4().split(",");

            droplatitude4 = Double.parseDouble(latlong5[0]);
            droplongitude4 = Double.parseDouble(latlong5[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude4, droplongitude4)).title("Drop Location 4").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong6 = pojo.getDrop_locatoin5().split(",");

            droplatitude5 = Double.parseDouble(latlong6[0]);
            droplongitude5 = Double.parseDouble(latlong6[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude5, droplongitude5)).title("Drop Location 5").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong7 = pojo.getDrop_locatoin6().split(",");

            droplatitude6 = Double.parseDouble(latlong7[0]);
            droplongitude6 = Double.parseDouble(latlong7[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude6, droplongitude6)).title("Drop Location 6").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong8 = pojo.getDrop_locatoin7().split(",");

            droplatitude7 = Double.parseDouble(latlong8[0]);
            droplongitude7 = Double.parseDouble(latlong8[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude7, droplongitude7)).title("Drop Location 7").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

            String[] latlong9 = pojo.getDrop_locatoin8().split(",");

            droplatitude8 = Double.parseDouble(latlong9[0]);
            droplongitude8 = Double.parseDouble(latlong9[1]);

            myMap1.addMarker(new MarkerOptions().position(new LatLng(droplatitude8, droplongitude8)).title("Drop Location 8").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));

        }



        if (myMap1 != null) {

            tunonGps();
        }

    }




    private String getAdd(double latitude, double longitude) {
        String finalAddress = null;
        try {

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            finalAddress = address + ", " + city + "," + state + "," + country;


        } catch (Exception e) {

        }
        return finalAddress;
    }


    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

  Handler handler = new Handler();


    public void scheduleSendLocation() {
        handler.postDelayed(new Runnable() {
            public void run() {
                NeaBy1();          // this method will contain your almost-finished HTTP calls
                handler.postDelayed(this, FIVE_SECONDS);
            }
        }, FIVE_SECONDS);
    }
    public void NeaBy1() {
        RequestParams params = new RequestParams();
        params.put("ride_id",pojo.getRide_id());
        //  Log.d("sessionkey",""+sessionManager.getKEY());
        Server.setHeader(sessionManager.getKEY());
        Server.get("api/user/clocation/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> address;

                try {

                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new GsonBuilder().create();
                        List<NearbyData> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<NearbyData>>() {

                        }.getType());


                        if (response.has("data") ) {

                            // If you have array
                            // hold your JSON response in String
//
                            JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array
                            JSONObject obj = resultArray.getJSONObject(0);
                            String name = obj.getString("cloc");

                            if(!name.equals("def"))
                            {
                                String[] latlong = name.split(",");

                                currentLatitude1 = Double.parseDouble(latlong[0]);
                                currentLongitude1 = Double.parseDouble(latlong[1]);

                                marker1.setPosition(new LatLng(currentLatitude1, currentLongitude1));
                                myMap1.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude1, currentLongitude1),10));

                            }

                            else{
                                Toast.makeText(getActivity(),"Driver Location Not Available", Toast.LENGTH_LONG).show();

                            }
                            //         Iterate the loop


                            }










                    }
                } catch (JSONException e) {


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                }
            }
        });
    }
}


