package com.XGO.customer.fragement;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.model.Direction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.XGO.customer.Server.Server;
import com.XGO.customer.custom.CheckConnection;
import com.XGO.customer.pojo.NearbyData;
import com.XGO.customer.pojo.Pass;
import com.XGO.customer.session.SessionManager;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.google.android.libraries.places.api.net.PlacesClient;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import com.XGO.customer.R;
import com.XGO.customer.acitivities.HomeActivity;
import com.XGO.customer.custom.GPSTracker;
import com.loopj.android.http.RequestParams;
import com.mapbox.mapboxsdk.Mapbox;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;
import com.thebrownarrow.permissionhelper.PermissionResult;
import com.thebrownarrow.permissionhelper.PermissionUtils;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.loopj.android.http.AsyncHttpClient.log;
import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;


/**
 * Created by android on 7/3/17.
 */

public class HomeFragment extends FragmentManagePermission implements OnMapReadyCallback, View.OnLongClickListener, DirectionCallback, Animation.AnimationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap. OnMarkerDragListener,GoogleMap. OnMarkerClickListener,
        LocationListener {
    private String driver_id;
    public JSONArray value;
    String tinfo;
    private String cost;
    private String unit;
    private int PLACE_PICKER_REQUEST = 7896;
    private RelativeLayout getdetail;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1234;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    Double b1,b2,b3,ap1,ap2,ap3,ac1,ac2,ac3,d1,d2,d3,t1,t2,t3;
    LinearLayout addpointlay;
    private GoogleApiClient mGoogleApiClient;
    private PlacesClient placesClient;
    private LocationRequest mLocationRequest;
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;
    private Double currentLatitude ,droplatitude,droplatitude1,droplatitude2,droplatitude3,droplatitude4,droplatitude5,droplatitude6,droplatitude7,droplatitude8,droplatitude9,droplatitude10;
    private Double currentLongitude,droplongitude,droplongitude1,droplongitude2,droplongitude3,droplongitude4,droplongitude5,droplongitude6,droplongitude7,droplongitude8,droplongitude9,droplongitude10 ;
    private Double currentLatitude1;
    private Double currentLongitude1;
    private View rootView;
    ArrayAdapter<String> adapter;
    String help = "No";
    String taxi1 = "40";
    String ride1;
    Location loc1;
    Boolean flag = false;
    boolean flag1 = true;
    ListView listItemView;
    Dialog dialog1;
    Button button5;
    private int mYear, mMonth, mDay, mHour, mMinute, date_flag;
    GoogleMap myMap;
    ImageView current_location, clear, imageView12, imageButton8, close2, vehi1,clear1,clear2,clear3,clear6,clear7,clear8,clear9,clear10,remove,remove1,remove2,remove3,remove4,remove5,remove6,remove7,remove8,remove9,remove10;
    FloatingActionButton support;
    ImageButton imageButton2, imageButton7, phone;
    // Marker marker1,marker2;
    private int btn = 1;
    //    PlaceDetectionClient mPlaceDetectionClient;
//    GeoDataClient mGeoDataClient;
    private RelativeLayout header, footer;
    Animation animFadeIn, animFadeOut;
    TextView pickup_location, drop_location,drop_location1,drop_location2,drop_location3,drop_location4,drop_location5,drop_location6,drop_location7,drop_location8, goodstext;
    RelativeLayout relative_drop,relative_drop1,relative_drop2,relative_drop3,relative_drop4,relative_drop5,relative_drop6,relative_drop7,relative_drop8;
    RelativeLayout linear_pickup;
    TextView txt_vehicleinfo, rate, txt_info, txt_cost, txt_color, txt_address, request_ride, open1, close1, any, textView8, helper_text5;

    SessionManager sessionManager;
    String[] permissionAsk = {PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE, PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_ACCESS_FINE_LOCATION, PermissionUtils.Manifest_ACCESS_COARSE_LOCATION};
    private String drivername;
    MapView mMapView;
    Pass pass;
    com.google.android.gms.location.places.Place pickup;
    com.google.android.gms.location.places.Place drop,drop1,drop2,drop3,drop4,drop5,drop6,drop7,drop8,drop9,drop10;
    ProgressBar progressBar;
    Marker marker,marker1,marker2,marker3,marker4,marker5,marker6,marker7,marker8,marker9,marker10;
    String[] goods;
    String goodstype, vectype, insurance, helper, payment, date, time, s;
    String t = "3";
    int pi;
    String vin = "0";
    String pickup_loc, drop_loc;
    ImageView bike_img, micro_img, mini_img, sedan_img, xuv_img;
    JSONObject response2;
    String[] val = new String[]{"Furniture", "Food and Beverages", "House Shifting", "Machines", "Wood", "Courier", "Vehicles", "Chemicals", "Tiles", "Glassware"};
    String[] val1 = new String[]{"Food from Hotel", "Food from Home", "Documents", "Couriers", "Auto Spares(2.5Kg Max)", "Small Electronics", "Others Items (2.5Kg Max)"};
    private WindowManager windowManager;
    int dl=0;

    String in = "0";

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //  MapsInitializer.initialize(this.getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        try {
            rootView = inflater.inflate(R.layout.home_fragment, container, false);
            ((HomeActivity) getActivity()).fontToTitleBar(getString(R.string.ride_select));
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            Mapbox.getInstance(getContext() , "pk.eyJ1IjoiaWNhbnN0dWRpb3oiLCJhIjoiY2oyMXQ3dGRpMDAwdDJ3bXpmZHRkdTBtNyJ9.PxslIcrVRj_gVgiv-Y-jog");
            Places.initialize(getApplicationContext(), "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M");
//Places.initialize(getApplicationContext(), "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M");
            // mMapView.getMapAsync(this);
//            PlacesClient placesClient = Places.createClient(getContext());
            bindView(savedInstanceState);
            if (!CheckConnection.haveNetworkConnection(getActivity())) {
                Toast.makeText(getActivity(), getString(R.string.network), Toast.LENGTH_LONG).show();
            }


//            String apiKey = "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M";
//            Places.initialize(getActivity(), apiKey);
            setCurrentLocation();
//            getActivity().getSupportFragmentManager().bac
            //  getdetail.setVisibility(View.VISIBLE);
            addpointlay.setVisibility(View.GONE);

            mGoogleApiClient = new GoogleApiClient
                    .Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

// Create a new Places client instance.
            //PlacesClient placesClient = Places.createClient(getActivity());
//            mGeoDataClient = Places.getGeoDataClient(getActivity());
//
//            // Construct a PlaceDetectionClient.
//            mPlaceDetectionClient = Places.getPlaceDetectionClient(getActivity(), null);
            onBackPressed1();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                askCompactPermissions(permissionAsk, new PermissionResult() {
                    @Override
                    public void permissionGranted() {
                        if (!GPSEnable()) {
                            tunonGps();
                        } else {
                            getcurrentlocation();
                        }
                    }

                    @Override
                    public void permissionDenied() {

                    }

                    @Override
                    public void permissionForeverDenied() {

                        openSettingsApp(getActivity());
                    }
                });

            } else {
                if (!GPSEnable()) {
                    tunonGps();
                } else {
                    getcurrentlocation();
                }

            }



            pickup_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,35);
                }
            });
//            pickup_location.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    try {
//                        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
//
//// Start the autocomplete intent.
//                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                        pi =1;
//                        startActivityForResult(builder.build(getActivity()), 35);
//                    } catch (GooglePlayServicesNotAvailableException e) {
//                        e.printStackTrace();
//                    } catch (GooglePlayServicesRepairableException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
            drop_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,34);
                }
            });
            drop_location1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,36);
                }
            });
            drop_location2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,37);
                }
            });
            drop_location3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,38);
                }
            });
            drop_location4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,39);
                }
            });

            drop_location5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,40);
                }
            });

            drop_location6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,41);
                }
            });
            drop_location7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,42);
                }
            });
            drop_location8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ID,Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(((HomeActivity) getActivity()));

                    startActivityForResult(intent,43);
                }
            });

        } catch (InflateException e) {

        }

        return rootView;


    }

    public void onBackPressed1() {



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("result");
                getcurrentlocation();
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        } else if (requestCode == 35) {
            if (resultCode == RESULT_OK) {
                log.d("Resultcode",Integer.toString(resultCode));

//                pickup = Autocomplete.getPlace(getActivity(), data).freeze();
//                pickup_location.setText(pickup.getAddress());
                Place place = Autocomplete.getPlaceFromIntent(data);
                String toastMsg1 = String.format("%s", place.getAddress());


                pickup_location.setText(toastMsg1);

//                currentLatitude = place.getLatLng().latitude;
//                currentLongitude = place.getLatLng().longitude;

                currentLatitude1 = place.getLatLng().latitude; //Added by shameem
                currentLongitude1 = place.getLatLng().longitude; //Added by shameem
//
//                List<Address> myaddress;
//                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
//                try {
//                    myaddress = geocoder.getFromLocation(currentLatitude1,currentLongitude1,1);
//                    pickup_location.setText(myaddress.get(0).getAddressLine(0).toString());
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }



                pass.setpickLat(currentLatitude1);  // Added by shameem 26/12/2019
                pass.setpickLong(currentLongitude1);// Added by shameem 26/12/2019

                marker1.setPosition(new LatLng(currentLatitude1, currentLongitude1));
                marker1.setTag(current_location);
                marker1.setDraggable(true);
                marker1.setTitle("Pickup Location");

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude1, currentLongitude1), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("error1", status.getStatusMessage());
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//                drop = PlaceAutocomplete.getPlace(getActivity(), data).freeze();
//                drop_location.setText(drop.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                //  Status status = AutocompleteActivity.(getActivity(), data);
                //   Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_LONG).show();

            }
        }
        else  if (requestCode == 34) {
            if (resultCode == RESULT_OK) {
//                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
//                String toastMsg = String.format("Place: %s", place.getAddress());
//                drop =  PlacePicker.getPlace(getContext(),data);
//                Log.d("placedrop",""+drop.getLatLng());
//                // Log.d("placeqq",toastMsg );
//                //drop_location.setText(drop.getName().toString() );
                Place place = Autocomplete.getPlaceFromIntent(data);
                String toastMsg = String.format("%s", place.getAddress());
                log.d("Address111",toastMsg);


                drop_location.setText(toastMsg);
                //dl=1;

                droplatitude = place.getLatLng().latitude;
                droplongitude = place.getLatLng().longitude;
//                List<Address> address5;
//                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
//                try {
//                    address5 = geocoder.getFromLocation(droplatitude,droplongitude,1);
//                    drop_location.setText(address5.get(0).getAddressLine(0).toString());
//
//
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                marker2 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude, droplongitude))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                marker2.setTitle("Drop Location 1");
                marker2.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude, droplongitude), 18);
                myMap.animateCamera(camera);
                addpointlay.setVisibility(View.VISIBLE);
                clear.setVisibility(View.VISIBLE);
                clear.setClickable(true);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }

        else  if (requestCode == 36) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop1 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
               // drop_location1.setText(drop1.getName().toString() );

                droplatitude1 = drop1.getLatLng().latitude;
                droplongitude1 = drop1.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude1,droplongitude1,1);
                    drop_location1.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker3 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude1, droplongitude1))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                marker3.setTitle("Drop Location 2");
                marker3.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude1, droplongitude1), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }

        else  if (requestCode == 37) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop2 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
               // drop_location2.setText(drop2.getName().toString() );

                droplatitude2 = drop2.getLatLng().latitude;
                droplongitude2 = drop2.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude2,droplongitude2,1);
                    drop_location2.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker4 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude2, droplongitude2))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                marker4.setTitle("Drop Location 3");
                marker4.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude2, droplongitude2), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), "Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }

        else  if (requestCode == 38) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop3 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
               // drop_location3.setText(drop3.getName().toString() );

                droplatitude3 = drop3.getLatLng().latitude;
                droplongitude3 = drop3.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude3,droplongitude3,1);
                    drop_location3.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker5 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude3, droplongitude3))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
                marker5.setTitle("Drop Location 4");
                marker5.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude3, droplongitude3), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }

        else  if (requestCode == 39) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop4 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
              //  drop_location4.setText(drop4.getName().toString() );

                droplatitude4 = drop4.getLatLng().latitude;
                droplongitude4 = drop4.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude4,droplongitude4,1);
                    drop_location4.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker6 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude4, droplongitude4))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
                marker6.setTitle("Drop Location 5");
                marker6.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude4, droplongitude4), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }

        else  if (requestCode == 40) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop5 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
               // drop_location5.setText(drop5.getName().toString() );

                droplatitude5 = drop5.getLatLng().latitude;
                droplongitude5 = drop5.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude5,droplongitude5,1);
                    drop_location5.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker7 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude5, droplongitude5))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                marker7.setTitle("Drop Location 6");
                marker7.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude5, droplongitude5), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }

        else  if (requestCode == 41) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop6 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
            //    drop_location6.setText(drop6.getName().toString() );
                droplatitude6 = drop6.getLatLng().latitude;
                droplongitude6 = drop6.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude6,droplongitude6,1);
                    drop_location6.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker8 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude6, droplongitude6))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                marker8.setTitle("Drop Location 7");
                marker8.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude6, droplongitude6), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }
        else  if (requestCode == 42) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop7 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
             //   drop_location7.setText(drop6.getName().toString() );
                droplatitude7 = drop7.getLatLng().latitude;
                droplongitude7 = drop7.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude7,droplongitude7,1);
                    drop_location7.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker9 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude7, droplongitude7))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                marker9.setTitle("Drop Location 8");
                marker9.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude7, droplongitude7), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }
        else  if (requestCode == 43) {
            if (resultCode == RESULT_OK) {
                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext() ,data);
                String toastMsg = String.format("Place: %s", place.getAddress());
                drop8 =  PlacePicker.getPlace(getContext(),data);
                // Log.d("placeqq",toastMsg );
             //   drop_location8.setText(drop8.getName().toString() );
                droplatitude8 = drop8.getLatLng().latitude;
                droplongitude8 = drop8.getLatLng().longitude;
                List<Address> address5;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address5 = geocoder.getFromLocation(droplatitude8,droplongitude8,1);
                    drop_location8.setText(address5.get(0).getAddressLine(0).toString());



                } catch (IOException e) {
                    e.printStackTrace();
                }
                marker10 = myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(droplatitude8, droplongitude8))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));
                marker10.setTitle("Drop Location 9");
                marker10.setDraggable(true);

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(droplatitude8, droplongitude8), 18);
                myMap.animateCamera(camera);
                Toast.makeText(getActivity(), " Drag Marker To Change Location ", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMapView != null) {
            mMapView.onPause();
        }
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.getFusedLocationProviderClient(getContext());
                mGoogleApiClient.disconnect();
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMapView != null) {
            mMapView.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mMapView != null) {

            mMapView.onStart();
            if (currentLatitude != null && !currentLatitude.equals(0.0) && currentLongitude != null && !currentLongitude.equals(0.0)) {
                NeaBy(String.valueOf(currentLatitude), String.valueOf(currentLongitude));

            }
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {

            mMapView.onResume();

        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public void multipleMarker(List<NearbyData> list) {
        if (list != null) {
            for (NearbyData location : list) {
                Double latitude = null;
                Double longitude = null;
                try {
                    latitude = Double.valueOf(location.getLatitude());
                    longitude = Double.valueOf(location.getLongitude());
                    //   NearbyData nearbyData = (NearbyData) marker.getTag();
                    tinfo = location.getVehicle_info();
                    Log.d("tinfo",tinfo);
                    if(tinfo == "1") {
                        Marker marker = myMap.addMarker(new MarkerOptions()
                                .position(new LatLng(latitude, longitude))
                                .title(location.getName())
                                .snippet(location.getVehicle_info())
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.bike)));
                        marker.setTag(location);
                    }

                    else
                    {
                        Marker marker = myMap.addMarker(new MarkerOptions()
                                .position(new LatLng(latitude, longitude))
                                .title(location.getName())
                                .snippet(location.getVehicle_info())
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.truck)));
                        marker.setTag(location);
                    }

                } catch (NumberFormatException e) {

                }



                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 18);
                myMap.animateCamera(camera);






            }
        }

    }


    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {


    }

    @Override
    public void onDirectionFailure(Throwable t) {

    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @SuppressLint("ClickableViewAccessibility")
    public void bindView(Bundle savedInstanceState) {
        progressBar = rootView.findViewById(R.id.progressBar);
        MapsInitializer.initialize(this.getActivity());
        current_location = rootView.findViewById(R.id.current_location);
        clear = rootView.findViewById(R.id.clear);
        clear1 = rootView.findViewById(R.id.clear1);
        clear2 = rootView.findViewById(R.id.clear2);
        clear3 = rootView.findViewById(R.id.clear3);
        clear6 = rootView.findViewById(R.id.clear6);
        clear7 = rootView.findViewById(R.id.clear7);
        clear8 = rootView.findViewById(R.id.clear8);
        clear9 = rootView.findViewById(R.id.clear9);
        clear10 = rootView.findViewById(R.id.clear10);
        remove = rootView.findViewById(R.id.remove);
        remove1 = rootView.findViewById(R.id.remove2);
        remove2 = rootView.findViewById(R.id.remove3);
        remove3 = rootView.findViewById(R.id.remove4);
        remove4 = rootView.findViewById(R.id.remove5);
        remove5 = rootView.findViewById(R.id.remove6);
        remove6 = rootView.findViewById(R.id.remove7);
        support = rootView.findViewById(R.id.support);
        txt_vehicleinfo = rootView.findViewById(R.id.txt_vehicleinfo);
        rate = rootView.findViewById(R.id.rate);
        open1 = rootView.findViewById(R.id.open1);
        textView8 = rootView.findViewById(R.id.textView8);
        close1 = rootView.findViewById(R.id.close1);
        any = rootView.findViewById(R.id.any);
        txt_info = rootView.findViewById(R.id.txt_info);
        txt_address = rootView.findViewById(R.id.txt_addresss);
        request_ride = rootView.findViewById(R.id.request_rides);
        txt_color = rootView.findViewById(R.id.txt_color);
        txt_cost = rootView.findViewById(R.id.txt_cost);
        button5 = rootView.findViewById(R.id.button5);

        //getdetail = rootView.findViewById(R.id.getdetail);
        sessionManager = new SessionManager(getActivity());

        mMapView = rootView.findViewById(R.id.mapview);

        header = rootView.findViewById(R.id.header);
        goodstext = rootView.findViewById(R.id.goodstext);
        pickup_location = rootView.findViewById(R.id.pickup_location);
        drop_location = rootView.findViewById(R.id.drop_location);
        drop_location1 = rootView.findViewById(R.id.drop_location1);
        drop_location2 = rootView.findViewById(R.id.drop_location2);
        drop_location3 = rootView.findViewById(R.id.drop_location3);
        drop_location4 = rootView.findViewById(R.id.drop_location4);
        drop_location5 = rootView.findViewById(R.id.drop_location5);
        drop_location6 = rootView.findViewById(R.id.drop_location6);
        drop_location7 = rootView.findViewById(R.id.drop_location7);
        drop_location8 = rootView.findViewById(R.id.drop_location8);
        linear_pickup = rootView.findViewById(R.id.linear_pickup);
        addpointlay = rootView.findViewById(R.id.addpointlay);
        relative_drop = rootView.findViewById(R.id.relative_drop);
        relative_drop1 = rootView.findViewById(R.id.relative_drop1);
        relative_drop2 = rootView.findViewById(R.id.relative_drop2);
        relative_drop3 = rootView.findViewById(R.id.relative_drop3);
        relative_drop4 = rootView.findViewById(R.id.relative_drop4);
        relative_drop5 = rootView.findViewById(R.id.relative_drop5);
        relative_drop6 = rootView.findViewById(R.id.relative_drop6);
        relative_drop7 = rootView.findViewById(R.id.relative_drop7);
        relative_drop8 = rootView.findViewById(R.id.relative_drop8);



        imageButton7 = rootView.findViewById(R.id.imageButton7);
        imageButton8 = rootView.findViewById(R.id.imageButton8);
        imageView12 = rootView.findViewById(R.id.imageView12);
        listItemView = rootView.findViewById(R.id.packagesgoods);

        // mGeoDataClient = Places.getGeoDataClient(getContext(),null);
        // Construct a PlaceDetectionClient.
        //    mPlaceDetectionClient = Places.getPlaceDetectionClient(getContext(),null);
        //Mapbox.getInstance(getContext() , "pk.eyJ1IjoiaWNhbnN0dWRpb3oiLCJhIjoiY2oyMXQ3dGRpMDAwdDJ3bXpmZHRkdTBtNyJ9.PxslIcrVRj_gVgiv-Y-jog");

        mMapView.getMapAsync(this);
        String apiKey = getResources().getString(R.string.google_android_map_api_key);

//    Places.initialize(getApplicationContext(), "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M");
////Places.initialize(getApplicationContext(), "AIzaSyBoye1WJlCR-Pih_CrHwin8DyqnfRnNJ7M");
//        // mMapView.getMapAsync(this);
//        PlacesClient placesClient = Places.createClient(getContext());

        mMapView.onCreate(savedInstanceState);
        pass = new Pass();
        // load animations
        animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(getActivity(),
                R.anim.fade_out);
        animFadeIn.setAnimationListener(this);
        animFadeOut.setAnimationListener(this);
        applyfonts();
        any.setBackgroundColor(getResources().getColor(R.color.red1));
        any.setTextColor(getResources().getColor(R.color.white));

        relative_drop1.setVisibility(View.GONE);
        relative_drop2.setVisibility(View.GONE);

        relative_drop3.setVisibility(View.GONE);
        relative_drop7.setVisibility(View.GONE);
        relative_drop8.setVisibility(View.GONE);

        clear.setClickable(false);

        addpointlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dl==0)
                {
                    Toast.makeText(getActivity(), "Add Atleat One Location", Toast.LENGTH_LONG).show();
                }


                if(dl==1)
                {
                    relative_drop1.setVisibility(View.VISIBLE);
                    dl = 2;
                }
                else if(dl==2)
                {
                    relative_drop2.setVisibility(View.VISIBLE);
                    dl = 3;
                }
                else if(dl==3)
                {
                    relative_drop3.setVisibility(View.VISIBLE);
                    dl = 4;
                }
                else if(dl==4)
                {
                    relative_drop4.setVisibility(View.VISIBLE);
                    dl = 5;
                }
                else if(dl==5)
                {
                    relative_drop5.setVisibility(View.VISIBLE);
                    dl = 6;
                }
                else if(dl==6)
                {
                    relative_drop6.setVisibility(View.VISIBLE);
                    dl = 7;
                }
                else if(dl==7)
                {
                    relative_drop7.setVisibility(View.VISIBLE);
                    dl = 8;
                }
                else  if(dl==8)
                {
                    relative_drop8.setVisibility(View.VISIBLE);
                    dl = 9;
                }
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date_flag = 0;


                NeaBy1();



            }
        });


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drop_location.setText("");
                marker2.remove();
                addpointlay.setVisibility(View.GONE);

            }
        });



        clear1.setClickable(true);

        clear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drop_location1.getText().toString().matches("")) {
                    relative_drop1.setVisibility(View.GONE);
                    drop_location1.setText("");
                } else {
                    marker3.remove();
                    relative_drop1.setVisibility(View.GONE);
                    drop_location1.setText("");

                }
                dl =1;


            }
        });
        relative_drop3.setVisibility(View.GONE);
        clear2.setClickable(true);

        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!drop_location2.getText().toString().matches("")) {
                    marker4.remove();
                    relative_drop2.setVisibility(View.GONE);
                    drop_location2.setText("");

                }


                else
                {
                    relative_drop2.setVisibility(View.GONE);
                    drop_location2.setText("");
                }

                dl =2;


            }
        });
        relative_drop4.setVisibility(View.GONE);
        clear3.setClickable(true);

        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!drop_location3.getText().toString().matches("")) {
                    marker5.remove();
                    relative_drop3.setVisibility(View.GONE);
                    drop_location3.setText("");

                }


                else
                {
                    relative_drop3.setVisibility(View.GONE);
                    drop_location3.setText("");
                }


                dl =3;

            }
        });

        relative_drop5.setVisibility(View.GONE);
        clear6.setClickable(true);

        clear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!drop_location4.getText().toString().matches("")) {
                    marker6.remove();
                    relative_drop4.setVisibility(View.GONE);
                    drop_location4.setText("");

                }


                else
                {
                    relative_drop4.setVisibility(View.GONE);
                    drop_location4.setText("");
                }

                dl =4;

            }
        });

        relative_drop6.setVisibility(View.GONE);
        clear7.setClickable(true);

        clear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!drop_location5.getText().toString().matches("")) {
                    marker7.remove();
                    relative_drop5.setVisibility(View.GONE);
                    drop_location5.setText("");

                }


                else
                {
                    relative_drop5.setVisibility(View.GONE);
                    drop_location5.setText("");
                }

                dl =5;

            }
        });

        clear8.setClickable(true);

        clear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!drop_location6.getText().toString().matches("")) {
                    marker8.remove();
                    relative_drop6.setVisibility(View.GONE);
                    drop_location6.setText("");

                }


                else
                {
                    relative_drop6.setVisibility(View.GONE);
                    drop_location6.setText("");
                }


                dl =6;

            }
        });
        clear9.setClickable(true);

        clear9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!drop_location7.getText().toString().matches("")) {

                    marker9.remove();
                    relative_drop7.setVisibility(View.GONE);
                    drop_location7.setText("");

                }


                else
                {
                    relative_drop7.setVisibility(View.GONE);
                    drop_location7.setText("");
                }


               dl =7;

            }
        });
        clear10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!drop_location8.getText().toString().matches("")) {

                    marker10.remove();
                    relative_drop8.setVisibility(View.GONE);
                    drop_location8.setText("");

                }


                else
                {
                    relative_drop8.setVisibility(View.GONE);
                    drop_location8.setText("");
                }


                dl =8;
            }
        });






        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCompactPermission(PermissionUtils.Manifest_CALL_PHONE, new PermissionResult() {
                    @Override
                    public void permissionGranted() {


                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:9940529564"));
                        startActivity(callIntent);

                    }

                    @Override
                    public void permissionDenied() {

                    }

                    @Override
                    public void permissionForeverDenied() {

                    }
                });


            }
        });









//        pick_later.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                final Dialog dialog = new Dialog(getContext());
//                dialog.setContentView(R.layout.dialog2);
//
//                ImageButton select_date = dialog.findViewById(R.id.select_date);
//                ImageButton select_time = dialog.findViewById(R.id.select_time);
//                final EditText pickup_date = dialog.findViewById(R.id.pickup_date);
//                final EditText pickup_time = dialog.findViewById(R.id.pickup_time);
//                Button next = dialog.findViewById(R.id.btn_next);
//
//
//                select_date.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        final Calendar c = Calendar.getInstance();
//                        mYear = c.get(Calendar.YEAR);
//                        mMonth = c.get(Calendar.MONTH);
//                        mDay = c.get(Calendar.DAY_OF_MONTH);
//
//                        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
//                                new DatePickerDialog.OnDateSetListener() {
//
//                                    @Override
//                                    public void onDateSet(DatePicker view, int year,
//                                                          int monthOfYear, int dayOfMonth) {
//
//                                        pickup_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
//                                        date = pickup_date.getText().toString();
//
//
//                                    }
//                                }, mYear, mMonth, mDay);
//                        datePickerDialog.show();
//                    }
//                });
//
//                select_time.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        final Calendar c = Calendar.getInstance();
//                        mHour = c.get(Calendar.HOUR_OF_DAY);
//                        mMinute = c.get(Calendar.MINUTE);
//
//                        // Launch Time Picker Dialog
//                        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
//                                new TimePickerDialog.OnTimeSetListener() {
//
//                                    @Override
//                                    public void onTimeSet(TimePicker view, int hourOfDay,
//                                                          int minute) {
//
//                                        pickup_time.setText(hourOfDay + ":" + minute);
//                                        time = pickup_time.getText().toString();
//                                    }
//                                }, mHour, mMinute, false);
//                        timePickerDialog.show();
//                    }
//                });
//
//                next.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//
//
//
//
//
//
//
//
//
//
//
//                        //making date and time row visible in request_ride.xml
//                        date_flag = 1;
//                        ride1 = "1";
//                        if (drop_location.getText().toString().matches("")) {
//                            Toast.makeText(getActivity(), "Enter Drop Location", Toast.LENGTH_LONG).show(); // not null not empty
//                            Log.d("dp", "true");
//
//                        } else if (goodstext.getText().toString().matches("Select Goods Type")) {
//                            Toast.makeText(getActivity(), "Select Goods Type", Toast.LENGTH_LONG).show(); // not null not empty
//                            Log.d("dp", "true");
//
//                        } else {
//
//                            Log.d("dp", "false");
//                            request_ride_method1();
//
//                        }
//                        dialog.dismiss();
//
//
//                    }
//                });
//
//                dialog.show();
//
//
//            }
//        });



        textView8.setOnClickListener(new View.OnClickListener() {

            int s1 = 10;

            @Override
            public void onClick(View v) {
                imageButton2.setVisibility(View.GONE);
                // close2.setVisibility(View.VISIBLE);
                s1 = Integer.parseInt(s);
                Log.d("testdata", s);
                if (s1 == 1) {
                    Toast.makeText(getActivity(), "No package avaiable For Bikes", Toast.LENGTH_LONG).show();
                }
                if (s1 == 2) {
                    Log.d("testdataaass", s);

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.packages);
                    Log.d("testdatasssss", s);

                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);

                    title.setText("3 hours Package");
                    contenta.setText("₹ 13/km after 40 Kms");
                    contentb.setText("₹ 3.5/min after 3.5hrs");
                    button4.setText(" ₹ 700");

                    title1.setText("7 hours Package");
                    content1.setText("₹ 10/km after 70 Kms");
                    content2.setText("₹ 1.75/min after 7hrs");
                    priceb.setText(" ₹ 1300");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "1";
                            textView8.setText("3 hours Package - Tata Ace");
                            dialog.dismiss();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "2";
                            textView8.setText("7 hours Package - Tata Ace");
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
                if (s1 == 3) {
                    Toast.makeText(getActivity(), "No package avaiable For Tata Ape", Toast.LENGTH_LONG).show();
                }
                if (s1 == 4) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.packages);

                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();

                    title.setText("5.5 hours Package");
                    contenta.setText("₹ 21/km after 50 Kms");
                    contentb.setText("₹ 2.25/min after 5.5hrs");
                    button4.setText(" ₹ 1650");

                    title1.setText("9 hours Package");
                    content1.setText("₹ 21/km after 70 Kms");
                    content2.setText("₹ 2.25/min after 9hrs");
                    priceb.setText(" ₹ 2850");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "3";
                            textView8.setText("5.5 hours Package - Tata 407");
                            dialog.dismiss();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "4";
                            textView8.setText("9 hours Package - Tata 407");
                            dialog.dismiss();

                        }
                    });
                }

                if (s1 == 5) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.packages);

                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();

                    title.setText("3.5 hours Package");
                    contenta.setText("₹ 14/km after 40 Kms");
                    contentb.setText("₹ 4/min after 3.5hrs");
                    button4.setText(" ₹ 850");

                    title1.setText("7 hours Package");
                    content1.setText("₹ 11/km after 70 Kms");
                    content2.setText("₹ 2.25/min after 7hrs");
                    priceb.setText(" ₹ 1500");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "5";
                            textView8.setText("3.5 hours Package - Tata Dost");
                            dialog.dismiss();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "6";
                            textView8.setText("7 hours Package - Tata Dost");
                            dialog.dismiss();

                        }
                    });
                }

            }
        });


        current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("check34", "yes: ");
                pickup_loc = pickup_location.getText().toString();
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> address;

                if (!pickup_loc.matches("")) {

                    try {

//                        address=geocoder.getFromLocationName(pickup_loc,1);
//                        Address location=address.get(0);
//                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                   //     pickup_location.setText(location.toString());
////                        MarkerOptions options= new MarkerOptions().position(latLng);
////                        myMap.clear();
////                        myMap.addMarker(options);
////                        moveToCurrentLocation(latLng);
//
//                        pickup_location.setText(location.getAddressLine(1));

                        setCurrentLocation();
                        Log.d("hi", "onClick:try2");
                        String current_loc = pickup_location.getText().toString();
                        address = geocoder.getFromLocationName(current_loc, 1);
                        Address location = address.get(0);

                        pickup_location.setText(location.getAddressLine(1));


                    } catch (IOException e) {
                    }

                } else {
                    try {

                        setCurrentLocation();
                        Log.d("hi", "onClick:try2");
                        String current_loc = pickup_location.getText().toString();
                        address = geocoder.getFromLocationName(current_loc, 1);
                        Address location = address.get(0);

                        pickup_location.setText(location.getAddressLine(1));
//                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                        MarkerOptions options = new MarkerOptions().position(latLng);
//                        myMap.clear();
//                        myMap.addMarker(options);
//                        myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
//                        // Zoom in, animating the camera.
//                        myMap.animateCamera(CameraUpdateFactory.zoomIn());
//                        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
//                        myMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                        //      pickup_location.setText(location.toString());
                    } catch (IOException e) {

                    }


                }


            }
        });

    }

    private void moveToCurrentLocation(LatLng currentLocation) {
        myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));
        // Zoom in, animating the camera.
        myMap.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        myMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        int result = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            //     android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            FusedLocationProviderClient location = LocationServices.getFusedLocationProviderClient(getContext());

//            if (location == null) {
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//            } else {
//                //If everything went fine lets get latitude and longitude
//                currentLatitude = location.getLatitude();
//                currentLongitude = location.getLongitude();
//                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
//                    if (!flag) {
//                        NeaBy(String.valueOf(currentLatitude), String.valueOf(currentLongitude));
//                    }

            location.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.

                            if (location != null) {
                                // Logic to handle location object
                                currentLatitude = location.getLatitude();
                                currentLongitude = location.getLongitude();


                                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
                                    if (!flag) {
                                        NeaBy(String.valueOf(currentLatitude), String.valueOf(currentLongitude));
                                    }
                                }
                            } else {

                                Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        } else {
            askCompactPermissions(permissionAsk, new PermissionResult() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionDenied() {
                }

                @Override
                public void permissionForeverDenied() {
                    Snackbar.make(rootView, getString(R.string.allow_permission), Snackbar.LENGTH_LONG).show();
                    openSettingsApp(getActivity());
                }
            });

        }


    }

    private void setCurrentLocation() {
        if (!GPSEnable()) {
            tunonGps();

        } else {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {


                try {
                    int result = ContextCompat.checkSelfPermission(getActivity(), ACCESS_FINE_LOCATION);
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        //     android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        FusedLocationProviderClient location = LocationServices.getFusedLocationProviderClient(getContext());

                        location.getLastLocation()
                                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations this can be null.
                                        if (location != null) {
                                            // Logic to handle location object
                                            currentLatitude = location.getLatitude();
                                            currentLongitude = location.getLongitude();

                                            pass.setpickLat(currentLatitude);  // Added by shameem 26/12/2019
                                            pass.setpickLong(currentLongitude);// Added by shameem 26/12/2019

                                            loc1 = location;
                                            try {
                                                List<Address> address3,address23;
                                                List<Place> pal;
                                                Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
                                                address3 = geocoder1.getFromLocation(currentLatitude,currentLongitude,1);

                                                Log.d("Address3",""+address3.get(0).getAddressLine(0).toString());
                                                pickup_location.setText(address3.get(0).getAddressLine(0));

                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
                                                LatLng latLng1 = new LatLng(currentLatitude, currentLongitude);
                                             marker1 =  myMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                             marker1.setDraggable(true);

                                            }
                                        } else {

                                            Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                        String placeId = "INSERT_PLACE_ID_HERE";



// Call findCurrentPlace and handle the response (first check that the user has granted permission).


                    } else {
                        askCompactPermissions(permissionAsk, new PermissionResult() {
                            @Override
                            public void permissionGranted() {

                            }

                            @Override
                            public void permissionDenied() {
                            }

                            @Override
                            public void permissionForeverDenied() {
                                Snackbar.make(rootView, getString(R.string.allow_permission), Snackbar.LENGTH_LONG).show();
                                openSettingsApp(getActivity());
                            }
                        });

                    }
                } catch (Exception e) {

                }

            }
        }





    }






    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {

                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
        }

    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        if (location != null) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

        }
    }


    public void applyfonts() {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "font/AvenirLTStd_Medium.otf");
        Typeface font1 = Typeface.createFromAsset(getActivity().getAssets(), "font/AvenirLTStd_Book.otf");
        pickup_location.setTypeface(font);
        drop_location.setTypeface(font);
//        txt_vehicleinfo.setTypeface(font1);
//        rate.setTypeface(font1);

//        txt_color.setTypeface(font);
//        txt_address.setTypeface(font);
//        request_ride.setTypeface(font1);


    }


    public void NeaBy(String latitude, String longitude) {
        flag = true;
        RequestParams params = new RequestParams();
        params.put("lat", latitude);
        params.put("long", longitude);
        Server.setHeader(sessionManager.getKEY());
        Log.d("sessionkey",""+sessionManager.getKEY());
        Server.get("api/user/nearby/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressBar.setVisibility(View.VISIBLE);

            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                response2=response;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> address;

                try {
                    Log.d("mullist", new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new GsonBuilder().create();
                        List<NearbyData> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<NearbyData>>() {

                        }.getType());
                        String[] arr = new String[list.size()];
                        for (int i =0; i < list.size(); i++)
                            arr[i] = list.get(i).toString();
                        for (String x : arr)
                            Log.d("mullist", "onSuccess:000000000000000000");


                        multipleMarker(list);
                        value=response.getJSONArray("data");

                        cost = response.getJSONObject("fair").getString("cost");
                        unit = response.getJSONObject("fair").getString("unit");
                        sessionManager.setUnit(unit);

                        setCurrentLocation();

                        Log.d("k", "onClick:try2");
                        String current_loc = pickup_location.getText().toString();
                        address = geocoder.getFromLocationName(current_loc, 1);
                        Address location = address.get(0);
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                        myMap.clear();
                        myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,17));
                        // Zoom in, animating the camera.
                        myMap.animateCamera(CameraUpdateFactory.zoomIn());
                        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
//                        myMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
//                        myMap.animateCamera(CameraUpdateFactory.zoomIn());
                    }
                } catch (JSONException e) {


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }


    public void getcurrentlocation() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
    }


    public void tunonGps() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(30 * 1000);
            mLocationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            getcurrentlocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and setting the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

    }

    public Boolean GPSEnable() {
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        return gpsTracker.canGetLocation();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myMap = googleMap;
        myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        myMap.setOnMarkerDragListener(this);
        myMap.setOnMarkerClickListener(this);

        myMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {

                return null;
            }

            @Override
            public View getInfoContents(final Marker marker) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.view_custom_marker, null);

                LatLng latLng = marker.getPosition();
                TextView title = v.findViewById(R.id.t);
                TextView t1 = v.findViewById(R.id.t1);
                TextView t2 = v.findViewById(R.id.t2);
                ImageView imageView = v.findViewById(R.id.profile_image);
                Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "font/AvenirLTStd_Medium.otf");
                t1.setTypeface(font);
                t2.setTypeface(font);
                String name = marker.getTitle();
                title.setText(name);
                String info = marker.getSnippet();
                t1.setText(info);

                NearbyData nearbyData = (NearbyData) marker.getTag();
                if (nearbyData != null) {
                    pass.setVehicleName(nearbyData.getVehicle_info());
                    txt_info.setText(nearbyData.getVehicle_info());
                    txt_address.setText("");
                    driver_id = nearbyData.getUser_id();
                    drivername = marker.getTitle();

                    t2.setVisibility(View.VISIBLE);
                } else {
                    t2.setVisibility(View.GONE);
                }
                txt_cost.setText(cost + "  " + unit);
                txt_address.setText(getAdd(Double.valueOf(nearbyData.getLatitude()), Double.valueOf(nearbyData.getLongitude())) + " ");


                return v;

            }
        });

     /*   myMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                NearbyData nearbyData = (NearbyData) marker.getTag();
                if (nearbyData != null) {
                    driver_id = nearbyData.getUser_id();
                    drivername = marker.getTitle();
                }


                if (header.getVisibility() == View.VISIBLE && footer.getVisibility() == View.VISIBLE) {
                    header.startAnimation(animFadeOut);
                    footer.startAnimation(animFadeOut);
                    header.setVisibility(View.GONE);
                    footer.setVisibility(View.GONE);
                } else {

                    header.setVisibility(View.VISIBLE);
                    footer.setVisibility(View.VISIBLE);
                    header.startAnimation(animFadeIn);
                    .startAnimation(animFadeIn);
                }

            }
        }); */

        if (myMap != null) {

            tunonGps();
        }

    }
    public void NeaBy1() {
        RequestParams params = new RequestParams();

        //  Log.d("sessionkey",""+sessionManager.getKEY());
        Server.setHeader(sessionManager.getKEY());
        Server.get("api/user/fare23/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> address;

                try {

                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new GsonBuilder().create();
                        List<NearbyData> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<NearbyData>>() {

                        }.getType());


                        if (response.has("data") ) {

                            // If you have array
                            // hold your JSON response in String
//
                            JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array

                            //         Iterate the loop
                            for (int i = 0; i < 15; i++) {

                                if(i==0) {
                                    // get value with the NODE key
                                    JSONObject obj = resultArray.getJSONObject(0);
                                    String name = obj.getString("name");

                                    String bike =  obj.getString("value");
                                    b1 = Double.parseDouble(bike);
                                }
                                if(i==1){
                                    JSONObject obj = resultArray.getJSONObject(7);
                                    String bikekm =  obj.getString("value");
                                    b2 = Double.parseDouble(bikekm);
                                }




                                if(i==2){
                                    JSONObject obj = resultArray.getJSONObject(8);
                                    String bikemin =  obj.getString("value");
                                    b3 = Double.parseDouble(bikemin);

                                }

                                if(i==3){
                                    JSONObject obj = resultArray.getJSONObject(9);
                                    String ape =  obj.getString("value");
                                    ap1 = Double.parseDouble(ape);

                                }
                                if(i==4){
                                    JSONObject obj = resultArray.getJSONObject(10);
                                    String apekm =  obj.getString("value");
                                    ap2 = Double.parseDouble(apekm);

                                }
                                if(i==5){
                                    JSONObject obj = resultArray.getJSONObject(11);
                                    String apemin =  obj.getString("value");
                                    ap3 = Double.parseDouble(apemin);

                                }

                                if(i==6){
                                    JSONObject obj = resultArray.getJSONObject(12);
                                    String ace =  obj.getString("value");
                                    ac1 = Double.parseDouble(ace);

                                }
                                if(i==7){
                                    JSONObject obj = resultArray.getJSONObject(13);
                                    String acekm =  obj.getString("value");
                                    ac2 = Double.parseDouble(acekm);

                                }
                                if(i==8){
                                    JSONObject obj = resultArray.getJSONObject(14);
                                    String acemin =  obj.getString("value");
                                    ac3 = Double.parseDouble(acemin);

                                }

                                if(i==9){
                                    JSONObject obj = resultArray.getJSONObject(15);
                                    String DOST =  obj.getString("value");
                                    d1 = Double.parseDouble(DOST);
                                    Log.d("dost45",""+d1);

                                }
                                if(i==10){
                                    JSONObject obj = resultArray.getJSONObject(16);
                                    String DOSTkm =  obj.getString("value");
                                    d2 = Double.parseDouble(DOSTkm);

                                }
                                if(i==11){
                                    JSONObject obj = resultArray.getJSONObject(17);
                                    String DOSTmin =  obj.getString("value");
                                    d3 = Double.parseDouble(DOSTmin);

                                }

                                if(i==12){
                                    JSONObject obj = resultArray.getJSONObject(18);
                                    String T407 =  obj.getString("value");
                                    t1 = Double.parseDouble(T407);

                                }
                                if(i==13){

                                    JSONObject obj = resultArray.getJSONObject(19);
                                    String T407km =  obj.getString("value");
                                    t2 = Double.parseDouble(T407km);

                                }
                                if(i==14){
                                    JSONObject obj = resultArray.getJSONObject(20);
                                    String T407min =  obj.getString("value");
                                    t3 = Double.parseDouble(T407min);


                                }

                            }
                            String error = response.getString("data");

                            Log.d("fare11", ""+b1);
                            Log.d("fare21", ""+b2);
                            Log.d("fare31", ""+b3);
                            Log.d("fare41", ""+ap1);
                            Log.d("fare51", ""+ap2);
                            Log.d("fare61", ""+ap3);
                            Log.d("fare71", ""+ac1);
                            Log.d("fare81", ""+ac2);
                            Log.d("fare91", ""+ac3);




                        }

                        request_ride_method();


                    }
                } catch (JSONException e) {


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                }
            }
        });
    }

    public void request_ride_method()
    {
        try {
            if (value == null || value.length() == 0) {
                Toast.makeText(getActivity(), getString(R.string.no_truck), Toast.LENGTH_LONG).show();
            } else {



                if (CheckConnection.haveNetworkConnection(getActivity())) {
                    if (pickup_location.getText().toString().trim().equals("") && drop_location.getText().toString().trim().equals("")) {
                        Toast.makeText(getActivity(), "Enter drop location", Toast.LENGTH_LONG).show();
//                    } else if (pickup == null && drop == null) {
//                        Toast.makeText(getActivity(), "Enter drop location", Toast.LENGTH_LONG).show();
                    }else if (cost == null || unit == null) {
                        Toast.makeText(getActivity(), getString(R.string.invalid_fare), Toast.LENGTH_SHORT).show();
                    }
// else if(insurance.matches("Insurance")){
//                        Toast.makeText(getActivity(),"Select Insurance" , Toast.LENGTH_SHORT).show();
//                    }
//                    else if(helper.matches("Helper")){
//                        Toast.makeText(getActivity(),"Select Helper" , Toast.LENGTH_SHORT).show();
//                    }
                    else {
//                        if (pickup==null) {
//                            currentLatitude1 = currentLatitude;
//                            currentLongitude1 = currentLongitude;
//                        }
                        Bundle bundle = new Bundle();
                        String dl1 = Integer.toString(dl);
                        pass.setdl(dl1);
                        if(dl == 1) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                        }
                        if(dl == 2) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                        }
                        if(dl == 3) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);

                        }
                        if(dl == 4) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);
                            pass.setdropLat3(droplatitude3);
                            pass.setdropLong3(droplongitude3);

                        }
                        if(dl == 5) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);
                            pass.setdropLat3(droplatitude3);
                            pass.setdropLong3(droplongitude3);
                            pass.setdropLat4(droplatitude4);
                            pass.setdropLong4(droplongitude4);
                        }
                        if(dl == 6) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);
                            pass.setdropLat3(droplatitude3);
                            pass.setdropLong3(droplongitude3);
                            pass.setdropLat4(droplatitude4);
                            pass.setdropLong4(droplongitude4);
                            pass.setdropLat5(droplatitude5);
                            pass.setdropLong5(droplongitude5);
                        }
                        if(dl == 7) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);
                            pass.setdropLat3(droplatitude3);
                            pass.setdropLong3(droplongitude3);
                            pass.setdropLat4(droplatitude4);
                            pass.setdropLong4(droplongitude4);
                            pass.setdropLat5(droplatitude5);
                            pass.setdropLong5(droplongitude5);
                            pass.setdropLat6(droplatitude6);
                            pass.setdropLong6(droplongitude6);
                        }
                        if(dl == 7) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);
                            pass.setdropLat3(droplatitude3);
                            pass.setdropLong3(droplongitude3);
                            pass.setdropLat4(droplatitude4);
                            pass.setdropLong4(droplongitude4);
                            pass.setdropLat5(droplatitude5);
                            pass.setdropLong5(droplongitude5);
                            pass.setdropLat6(droplatitude6);
                            pass.setdropLong6(droplongitude6);
                            pass.setdropLat7(droplatitude7);
                            pass.setdropLong7(droplongitude7);

                        }
                        if(dl == 8) {
//                            pass.setpickLat(currentLatitude1);
//                            pass.setpickLong(currentLongitude1);

                            pass.setdropLat(droplatitude);
                            pass.setdropLong(droplongitude);
                            pass.setdropLat1(droplatitude1);
                            pass.setdropLong1(droplongitude1);
                            pass.setdropLat2(droplatitude2);
                            pass.setdropLong2(droplongitude2);
                            pass.setdropLat3(droplatitude3);
                            pass.setdropLong3(droplongitude3);
                            pass.setdropLat4(droplatitude4);
                            pass.setdropLong4(droplongitude4);
                            pass.setdropLat5(droplatitude5);
                            pass.setdropLong5(droplongitude5);
                            pass.setdropLat6(droplatitude6);
                            pass.setdropLong6(droplongitude6);
                            pass.setdropLat7(droplatitude7);
                            pass.setdropLong7(droplongitude7);
                            pass.setdropLat8(droplatitude8);
                            pass.setdropLong8(droplongitude8);

                        }


                        // pass.settype2(vin);
                       // currentLongitude1 = null;
                        //currentLatitude1 = null;
                        drop = null;
                        //currentLongitude = null;
                        //currentLatitude = null;

                        bundle.putSerializable("data", pass);
                        RequestFragment fragobj = new RequestFragment();
                        fragobj.setArguments(bundle);
                        ((HomeActivity) getActivity()).changeFragment(fragobj, getString(R.string.request_ride));

                    }
                }
            }
        }
        catch (Error error) {
        }
    }
    public void request_ride_method1()
    {
        try {
            if (value == null || value.length() == 0) {
                Toast.makeText(getActivity(), getString(R.string.no_truck), Toast.LENGTH_LONG).show();
            } else {

                Log.d("mullist", new GsonBuilder().setPrettyPrinting().create().toJson(value));

                goodstype = goodstext.getText().toString();
                insurance=in;
                helper = help;

                drivername = value.getJSONObject(0).getString("name");
                Log.d("d", "drivername: " + drivername);
                driver_id = value.getJSONObject(0).getString("user_id");
                if (CheckConnection.haveNetworkConnection(getActivity())) {
                    if (pickup_location.getText().toString().trim().equals("") && drop_location.getText().toString().trim().equals("")) {
                        Toast.makeText(getActivity(), getString(R.string.invalid_location), Toast.LENGTH_LONG).show();
                    } else if (pickup == null && drop == null) {
                        Toast.makeText(getActivity(), "location df", Toast.LENGTH_LONG).show();
                    } else if (driver_id == null || drivername == null) {
                        Toast.makeText(getActivity(), getString(R.string.select_driver), Toast.LENGTH_LONG).show();
                    } else if (cost == null || unit == null) {
                        Toast.makeText(getActivity(), getString(R.string.invalid_fare), Toast.LENGTH_SHORT).show();
                    }  else if (vectype==null) {
                        Toast.makeText(getActivity(), "select vehicle", Toast.LENGTH_SHORT).show();
                    }
                    else if(goodstype.matches("Goods Type")){
                        Toast.makeText(getActivity(),"Select Goods type" , Toast.LENGTH_SHORT).show();
                    }
                    else if(date==null||time==null)
                    {
                        Toast.makeText(getActivity(),"select date and time", Toast.LENGTH_LONG).show();
                    }


// else if(insurance.matches("Insurance")){
//                        Toast.makeText(getActivity(),"Select Insurance" , Toast.LENGTH_SHORT).show();
//                    }
//                    else if(helper.matches("Helper")){
//                        Toast.makeText(getActivity(),"Select Helper" , Toast.LENGTH_SHORT).show();
//                    }
                    else {
                        Bundle bundle = new Bundle();
                        if(dl == 1) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace(drop);
                        }
                        if(dl == 2) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace(drop);
                            pass.setToPlace1(drop1);
                        }
                        if(dl == 3) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace(drop);
                            pass.setToPlace1(drop1);

                            pass.setToPlace2(drop2);

                        }
                        if(dl == 4) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace(drop);
                            pass.setToPlace1(drop1);

                            pass.setToPlace2(drop2);
                            pass.setToPlace3(drop3);
                        }
                        if(dl == 5) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace1(drop1);

                            pass.setToPlace2(drop2);
                            pass.setToPlace3(drop3);
                            pass.setToPlace4(drop4);
                        }
                        if(dl == 6) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace(drop);
                            pass.setToPlace1(drop1);

                            pass.setToPlace2(drop2);
                            pass.setToPlace3(drop3);
                            pass.setToPlace4(drop4);
                            pass.setToPlace5(drop5);
                        }
                        if(dl == 7) {
                            pass.setFromPlace(pickup);
                            pass.setToPlace(drop);

                            pass.setToPlace1(drop1);

                            pass.setToPlace2(drop2);
                            pass.setToPlace3(drop3);
                            pass.setToPlace4(drop4);
                            pass.setToPlace5(drop5);
                            pass.setToPlace6(drop6);
                        }

                        // pass.settype2(vin);
                        bundle.putSerializable("data", pass);
                        RequestFragment fragobj = new RequestFragment();
                        fragobj.setArguments(bundle);
                        ((HomeActivity) getActivity()).changeFragment(fragobj, getString(R.string.request_ride));

                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.network), Toast.LENGTH_LONG).show();

                }
            }
        }

        catch (JSONException e) {
            Log.e("JSON", "There was an error parsing the JSON", e);
        }
    }


    private String getAdd(double latitude, double longitude) {
        String finalAddress = null;
        try {

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            finalAddress = address + ", " + city + "," + state + "," + country;


        } catch (Exception e) {

        }
        return finalAddress;
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {


        if (marker.equals(marker1)) {
            Log.d("22position", "" + marker1.getPosition());

            currentLongitude = marker1.getPosition().longitude;
            currentLatitude = marker1.getPosition().latitude;

            pass.setpickLat(currentLatitude);  // Added by shameem 26/12/2019
            pass.setpickLong(currentLongitude);// Added by shameem 26/12/2019


            List<Address> address3;
            Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address3 = geocoder1.getFromLocation(currentLatitude,currentLongitude,1);
                Log.d("Address3",""+address3);
                pickup_location.setText(address3.get(0).getAddressLine(0).toString());


                String a = pickup_location.getText().toString();



            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (marker.equals(marker2)) {
            Log.d("22position", "" + marker2.getPosition());

            droplongitude = marker2.getPosition().longitude;
            droplatitude = marker2.getPosition().latitude;

            List<Address> address5;
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address5 = geocoder.getFromLocation(droplatitude,droplongitude,1);
                drop_location.setText(address5.get(0).getAddressLine(0).toString());



            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        else if (marker.equals(marker3)) {
            //        Log.d("22position", "" + marker2.getPosition());

            droplongitude1 = marker3.getPosition().longitude;
            droplatitude1 = marker3.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude1,droplongitude1,1);
                drop_location1.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (marker.equals(marker4)) {
            //        Log.d("22position", "" + marker4.getPosition());

            droplongitude2 = marker4.getPosition().longitude;
            droplatitude2 = marker4.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude2,droplongitude2,1);
                drop_location2.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (marker.equals(marker5)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude3 = marker3.getPosition().longitude;
            droplatitude3 = marker3.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude3,droplongitude3,1);
                drop_location3.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        if (marker.equals(marker6)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude4 = marker6.getPosition().longitude;
            droplatitude4 = marker6.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude4,droplongitude4,1);
                drop_location4.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (marker.equals(marker7)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude5 = marker7.getPosition().longitude;
            droplatitude5 = marker7.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude5,droplongitude5,1);
                drop_location5.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (marker.equals(marker8)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude6 = marker6.getPosition().longitude;
            droplatitude6 = marker6.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude6,droplongitude6,1);
                drop_location6.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }




    @Override
    public void onMarkerDrag(Marker marker) {
        if (marker.equals(marker1))
        {
            marker1.isDraggable();
            Log.d("22position" , "" +    marker1.isDraggable());


//                marker1 = myMap.addMarker(new MarkerOptions()
//                        .position(new LatLng(currentLatitude, currentLongitude))
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_dark)));
//                marker1.setTag(current_location);
//                marker1.setDraggable(true);



            Log.d("22position", "" + marker1.getPosition());

            currentLongitude = marker1.getPosition().longitude;
            currentLatitude = marker1.getPosition().latitude;

            List<Address> address3;
            Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address3 = geocoder1.getFromLocation(currentLatitude,currentLongitude,1);
                Log.d("Address3",""+address3);
                pickup_location.setText(address3.get(0).getAddressLine(0).toString());


                String a = pickup_location.getText().toString();



            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (marker.equals(marker2)) {
            Log.d("22position", "" + marker2.getPosition());

            droplongitude = marker2.getPosition().longitude;
            droplatitude = marker2.getPosition().latitude;

            List<Address> address5;
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address5 = geocoder.getFromLocation(droplatitude,droplongitude,1);
                drop_location.setText(address5.get(0).getAddressLine(0).toString());



            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }
    public void onMarkerDragEnd(Marker marker) {


        if (marker.equals(marker1)) {
            Log.d("22position", "" + marker1.getPosition());

            currentLongitude = marker1.getPosition().longitude;
            currentLatitude = marker1.getPosition().latitude;

            pass.setpickLat(currentLatitude);  // Added by shameem 26/12/2019
            pass.setpickLong(currentLongitude);// Added by shameem 26/12/2019

            List<Address> address3;
            Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address3 = geocoder1.getFromLocation(currentLatitude,currentLongitude,1);
                Log.d("Address3",""+address3);
                pickup_location.setText(address3.get(0).getAddressLine(0).toString());


                String a = pickup_location.getText().toString();



            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (marker.equals(marker2)) {
            Log.d("22position", "" + marker2.getPosition());

            droplongitude = marker2.getPosition().longitude;
            droplatitude = marker2.getPosition().latitude;

            List<Address> address5;
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address5 = geocoder.getFromLocation(droplatitude,droplongitude,1);
                drop_location.setText(address5.get(0).getAddressLine(0).toString());



            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        else if (marker.equals(marker3)) {
            //        Log.d("22position", "" + marker2.getPosition());

            droplongitude1 = marker3.getPosition().longitude;
            droplatitude1 = marker3.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude1,droplongitude1,1);
                drop_location1.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        else if (marker.equals(marker4)) {
            //        Log.d("22position", "" + marker4.getPosition());

            droplongitude2 = marker4.getPosition().longitude;
            droplatitude2 = marker4.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude2,droplongitude2,1);
                drop_location2.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        else if (marker.equals(marker5)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude3 = marker3.getPosition().longitude;
            droplatitude3 = marker3.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude3,droplongitude3,1);
                drop_location3.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        else if (marker.equals(marker6)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude4 = marker6.getPosition().longitude;
            droplatitude4 = marker6.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude4,droplongitude4,1);
                drop_location4.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (marker.equals(marker7)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude5 = marker7.getPosition().longitude;
            droplatitude5 = marker7.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude5,droplongitude5,1);
                drop_location5.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (marker.equals(marker8)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude6 = marker8.getPosition().longitude;
            droplatitude6 = marker8.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude6,droplongitude6,1);
                drop_location6.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        else if (marker.equals(marker9)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude7 = marker9.getPosition().longitude;
            droplatitude7 = marker9.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude7,droplongitude7,1);
                drop_location7.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else if (marker.equals(marker10)) {
            //     Log.d("22position", "" + marker2.getPosition());

            droplongitude8 = marker10.getPosition().longitude;
            droplatitude8 = marker10.getPosition().latitude;


            List<Address> address4;
            Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
            try {
                address4 = geocoder2.getFromLocation(droplatitude8,droplongitude8,1);
                drop_location8.setText(address4.get(0).getAddressLine(0).toString());




            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onMarkerDragStart(Marker marker) {


        if (marker.equals(marker1)) {
            Log.d("22position", "" + marker1.getPosition());




        }
    }



}


