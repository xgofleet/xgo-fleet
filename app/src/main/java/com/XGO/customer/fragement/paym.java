package com.XGO.customer.fragement;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.XGO.customer.R;
import com.XGO.customer.Server.Server;
import com.XGO.customer.acitivities.HomeActivity;
import com.XGO.customer.pojo.NearbyData;
import com.XGO.customer.pojo.PendingRequestPojo;
import com.XGO.customer.session.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;


public class paym extends FragmentManagePermission {
    Button button;
    View view;
    String ride_id = "";
    String vinfo="";
    String key = "";
    String runtime;
    Double fare;
    String email,mobile;
    SessionManager sessionManager;
    TextView payment,rideid;
    PendingRequestPojo pojo;
    String price;
    Double b1,b2,b3,ap1,ap2,ap3,ac1,ac2,ac3,d11,d2,d3,t1,t23,t33;
    Double driverate;
    Double fare123;
    String runtime1 = "0";
    String v_info ="7";
    String helper;
    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //  MapsInitializer.initialize(this.getActivity());

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = inflater.inflate(R.layout.payment, container, false);
        // view= inflater inflate (R.layout.payment);


        bindView();

        // Payment button created by you in XML layout
        Button button = (Button) view.findViewById(R.id.btn_pay);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPayment();
            }
        });

        return view;

    }


    public void bindView() {

        sessionManager = new SessionManager(getActivity());
        Bundle bundle = getArguments();
        HashMap<String, String> user = sessionManager.getUserDetails();
        String url = user.get(SessionManager.AVATAR);
        String name = user.get(SessionManager.KEY_NAME);
        email = user.get(SessionManager.KEY_EMAIL);
        mobile = user.get(SessionManager.KEY_MOBILE);

        pojo = (PendingRequestPojo) bundle.getSerializable("data");
        NeaBy1();
        Log.d("run",""+pojo.getruntime()+"");

        runtime=pojo.getruntime();

        String k = sessionManager.getKEY();

        if (k != null) {
            key = k;


            Log.d("k", key);
        }



        payment = (TextView) view.findViewById(R.id.payment565);
        rideid = (TextView) view.findViewById(R.id.rideid);
        rideid.setText("Ride Id: "+pojo.getRide_id()+"");




    }


    public void NeaBy1() {
        RequestParams params = new RequestParams();

        //  Log.d("sessionkey",""+sessionManager.getKEY());
        SessionManager sessionManager = new SessionManager(getActivity());
        params.put("ride_id", pojo.getRide_id());

        Server.setHeader(sessionManager.getKEY());
        Server.setContetntType();
        Server.get("api/user/fare23/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> address;

                try {

                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new GsonBuilder().create();
                        List<NearbyData> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<NearbyData>>() {

                        }.getType());


                        if (response.has("data") ) {

                            // If you have array
                            // hold your JSON response in String
//


                            JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array

                            //         Iterate the loop
                            for (int i = 0; i < 15; i++) {

                                if(i==0) {
                                    // get value with the NODE key
                                    JSONObject obj = resultArray.getJSONObject(0);
                                    String name = obj.getString("name");

                                    String bike =  obj.getString("value");
                                    b1 = Double.parseDouble(bike);
                                }
                                if(i==1){
                                    JSONObject obj = resultArray.getJSONObject(7);
                                    String bikekm =  obj.getString("value");
                                    b2 = Double.parseDouble(bikekm);
                                }




                                if(i==2){
                                    JSONObject obj = resultArray.getJSONObject(8);
                                    String bikemin =  obj.getString("value");
                                    b3 = Double.parseDouble(bikemin);

                                }

                                if(i==3){
                                    JSONObject obj = resultArray.getJSONObject(9);
                                    String ape =  obj.getString("value");
                                    ap1 = Double.parseDouble(ape);

                                }
                                if(i==4){
                                    JSONObject obj = resultArray.getJSONObject(10);
                                    String apekm =  obj.getString("value");
                                    ap2 = Double.parseDouble(apekm);

                                }
                                if(i==5){
                                    JSONObject obj = resultArray.getJSONObject(11);
                                    String apemin =  obj.getString("value");
                                    ap3 = Double.parseDouble(apemin);

                                }

                                if(i==6){
                                    JSONObject obj = resultArray.getJSONObject(12);
                                    String ace =  obj.getString("value");
                                    ac1 = Double.parseDouble(ace);

                                }
                                if(i==7){
                                    JSONObject obj = resultArray.getJSONObject(13);
                                    String acekm =  obj.getString("value");
                                    ac2 = Double.parseDouble(acekm);

                                }
                                if(i==8){
                                    JSONObject obj = resultArray.getJSONObject(14);
                                    String acemin =  obj.getString("value");
                                    ac3 = Double.parseDouble(acemin);

                                }

                                if(i==9){
                                    JSONObject obj = resultArray.getJSONObject(15);
                                    String DOST =  obj.getString("value");
                                    d11 = Double.parseDouble(DOST);
                                    Log.d("dost45",""+d11);

                                }
                                if(i==10){
                                    JSONObject obj = resultArray.getJSONObject(16);
                                    String DOSTkm =  obj.getString("value");
                                    d2 = Double.parseDouble(DOSTkm);

                                }
                                if(i==11){
                                    JSONObject obj = resultArray.getJSONObject(17);
                                    String DOSTmin =  obj.getString("value");
                                    d3 = Double.parseDouble(DOSTmin);

                                }

                                if(i==12){
                                    JSONObject obj = resultArray.getJSONObject(18);
                                    String T407 =  obj.getString("value");
                                    t1 = Double.parseDouble(T407);

                                }
                                if(i==13){

                                    JSONObject obj = resultArray.getJSONObject(19);
                                    String T407km =  obj.getString("value");
                                    t23 = Double.parseDouble(T407km);

                                }
                                if(i==14){
                                    JSONObject obj = resultArray.getJSONObject(20);
                                    String T407min =  obj.getString("value");
                                    t33 = Double.parseDouble(T407min);


                                }

                            }

                            JSONObject obj = resultArray.getJSONObject(21);
                            String driverrt =  obj.getString("value");
                            driverate = Double.parseDouble(driverrt);

                            String error = response.getString("data");

                            Log.d("fare1", ""+b1);
                            Log.d("razorpay", ""+b2);
                            Log.d("fare3", ""+b3);
                            Log.d("fare4", ""+ap1);
                            Log.d("fare5", ""+ap2);
                            Log.d("fare6", ""+ap3);
                            Log.d("fare7", ""+ac1);
                            Log.d("fare8", ""+ac2);
                            Log.d("fare9", ""+ac3);



                            JSONArray resultArray1 = response.getJSONArray("data1"); // Here you will get the Array


                            // get value with the NODE key
                            JSONObject obj1 = resultArray1.getJSONObject(0);
                            String name1 =  obj1.getString("helper");

                            helper = name1;


                            Log.d("distancew",name1);



                            JSONArray resultArray2 = response.getJSONArray("data2"); // Here you will get the Array

                            //         Iterate the loop

                            JSONObject obj2 = resultArray2.getJSONObject(0);
                            String name3 =  obj2.getString("packages");

                            v_info = name3;

                            Log.d("package",name3);




                            calculatefar();


                        }



                    }
                }

                catch (JSONException e) {


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                }
            }
        });
    }

    private void calculatefar() {
        runtime1 = pojo.getruntime();

        String distance = pojo.getDistance();
        Double dist = Double.parseDouble(distance);
        Double rt = Double.parseDouble(runtime1);
//        helper = pojo.gethelper();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        Date currentTime = Calendar.getInstance().getTime();
        String currentDateandTime = sdf.format(currentTime);
        int currenttime1 = Integer.parseInt(currentDateandTime);
        int t21;

        try {
            t21 = Integer.parseInt(v_info);
            //  helper = pojo.gethelper();


        }
        catch (Error error)
        {
            t21 = Integer.parseInt("7");
            helper = "yes";

        }

        Log.d("rrazorpay2",""+t21);

        if(t21 == 0 )
        {

            DecimalFormat dtime = new DecimalFormat("##.##");

            Double help;
            Double ff8 = (rt+30)*b3;


            if(helper == "yes") {
                help = 170.0;
            }
            else
            {
                help = 0.0;
            }
            Double ff = (dist * b2)+help+15.0+(rt*b3)+b1;
            if(currenttime1 >=18 && currenttime1 <= 6  ) {
                ff =ff*1.5;
            }
            fare123 = Double.valueOf(dtime.format(ff));
        }

        if(t21 == 7)
        {

            DecimalFormat dtime = new DecimalFormat("##.##");

            Double ff23;
            Double help;

            if(helper == "yes") {
                help = 170.0;
            }
            else
            {
                help = 0.0;
            }

            Double ff = (dist * ac2)+help+143+(rt*ac3)+ac1;
            if(currenttime1 >=18 && currenttime1 <= 6  ) {
                ff =ff*1.5;
            }

            ;

            fare123 = Double.valueOf(dtime.format(ff));


        }
        if(t21 == 8)
        {

            DecimalFormat dtime = new DecimalFormat("##.##");


            Double help;

            if(helper == "yes") {
                help = 170.0;
            }
            else
            {
                help = 0.0;
            }
            Double ff = (dist * ap2)+help+143+(rt*ap3)+ap1;
            if(currenttime1 >=18 && currenttime1 <= 6  ) {
                ff =ff*1.5;
            }


            fare123 = Double.valueOf(dtime.format(ff));

        }
        if(t21 == 9)
        {
            DecimalFormat dtime = new DecimalFormat("##.##");


            Double help;

            if(helper == "yes") {
                help = 170.0;
            }
            else
            {
                help = 0.0;
            }

            Double ff = (dist * t23)+help+182+(rt*t33)+t1;

            if(currenttime1 >=18 && currenttime1 <= 6  ) {
                ff =ff*2.5;

            }


            fare123 = Double.valueOf(dtime.format(ff));


        }

        if(t21 == 10)
        {

            DecimalFormat dtime = new DecimalFormat("##.##");

            Double help;

            if(helper == "yes") {
                help = 170.0;
            }
            else
            {
                help = 0.0;
            }

            Double ff = (dist * d2)+help+233+(rt*d3)+d11;

            if(currenttime1 >=18 && currenttime1 <= 6  ) {
                ff =ff*2;

            }

            fare123 = Double.valueOf(dtime.format(ff));

        }






        if(t21 == 1) {
            if (helper == "yes") {

                fare123 =920.00;
            } else {

                fare123 = 750.00;
            }

        }
        if(t21 == 2)
        {
            if(helper == "yes") {

                fare123 =1470.00;

            }
            else
            {

                fare123 = 1300.00;
            }

        }
        if(t21 == 3) {
            if (helper == "yes") {

                fare123 = 1820.00;

            }
            else
            {

                fare123 = 1650.00;
            }

        }
        if(t21 == 4) {
            if (helper == "yes") {


                fare123 = 3020.00;
            } else {

                fare123 = 2850.00;


            }

        }
        if(t21 == 5)
        {
            if(helper == "yes")
            {

                fare123 = 1020.00;
                payment.setText("INR"+" "+fare123);

            }
            else {

                fare123 = 850.00;
                payment.setText("INR"+" "+fare123);


            }

        }
        if(t21 == 6) {
            if (helper == "yes") {

                fare123 = 1670.00;
                payment.setText("INR"+" "+fare123);


            } else {

                fare123 = 1500.00;
                payment.setText("INR"+" "+fare123);

            }

        }

        pojo.setprice(fare123);
        int fare = (int) Math.round(fare123);


        payment.setText("INR"+" "+fare);



    }
    // Payment button created by you in XML layout



    public void startPayment() {

        RequestParams params = new RequestParams();
        params.put("ride_id", pojo.getRide_id());
        params.put("payment_mode", "OFFLINE"); //Added by shameem
        Log.d("amount455",""+fare123);
        int fare = (int) Math.round(fare123);
        double drate = driverate/100;
        double dfare = fare*drate;
        //double dfare = fare*0.82;
        int dfare1 = (int) Math.round(dfare);
        double cfare = 1 - drate;
        double xfare = fare*cfare;
        //double xfare = fare*0.18;
        int xfare1 = (int) Math.round(xfare);
        params.put("amount",fare);
        params.put("damount",dfare1);
        params.put("xamount",xfare1);
        Server.setHeader(sessionManager.getKEY());
        Server.setContetntType();
        Server.post("api/user/rides", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                HomeFragment fragobj = new HomeFragment();
                //  bundle.putString("runtime",runtime1);
                ((HomeActivity) getActivity()).changeFragment(fragobj, "Home");




            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }

    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that getActivity() method runs correctly
     */

}
