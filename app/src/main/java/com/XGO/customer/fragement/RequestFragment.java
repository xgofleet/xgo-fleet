package com.XGO.customer.fragement;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.XGO.customer.pojo.PendingRequestPojo;
import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.XGO.customer.R;
import com.XGO.customer.Server.Server;
import com.XGO.customer.acitivities.HomeActivity;
import com.XGO.customer.custom.CheckConnection;;
import com.XGO.customer.pojo.NearbyData;
import com.XGO.customer.pojo.Pass;
import com.XGO.customer.session.SessionManager;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.paypal.android.sdk.payments.PayPalService;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static com.loopj.android.http.AsyncHttpClient.log;

/**
 * Created by android on 14/3/17.
 */

public class RequestFragment  extends FragmentManagePermission implements OnMapReadyCallback, DirectionCallback {
    private static final String ORDER = "";
    View view;
    TextView txt_vehicleinfo, rate, txt_info, txt_cost, txt_color, txt_address, request_ride, open1, close1, any, textView8, helper_text5;
    private TextView mResult;
    private String mContactName = "";
    private String mAllCols = "";
    AppCompatButton confirm, cancel;
    TextView pickup_location, drop_location,textView12;
    Double finalfare= 0.0,finalfare1 ;
    MapView mapView;
    private Double fare =10.0;
    GoogleMap myMap;
    double discount =0.0, discount1 = 0.0;
    int s1 = 10;
    EditText couponcode;
    Spinner goods_spinner, ins_spinner, payment_spinner, helper_spinner1, pacakege1;
    Button pick_later, pick_now, opened, closed, change,button5;
    LinearLayout linear_request,addpointlay, micro, mini, sedan, xuv, bike, goodslay, helperlay, v2, llTrack, taxi;
    String goodstype, vectype, insurance,  payment, date1, time1;
    String helper="def";
    String s="20";
    String t = "3";
    int pi;
    private int mYear, mMonth, mDay, mHour, mMinute, date_flag;
    String help = "No";
    Boolean flag = false;
    boolean flag1 = true;
    String vin = "0";
    String pickup_loc, drop_loc;
    ImageView bike_img, micro_img, mini_img, sedan_img, xuv_img,imageButton8,close2 ;
    private int btn = 1;
    JSONObject response2;
    String[] val = new String[]{"Furniture", "Food and Beverages", "House Shifting", "Machines", "Wood", "Courier", "Vehicles", "Chemicals", "Tiles", "Glassware"};
    String[] val1 = new String[]{"Food from Hotel", "Food from Home", "Documents", "Couriers", "Auto Spares(2.5Kg Max)", "Small Electronics", "Others Items (2.5Kg Max)"};
    AlertDialog alert;
    ImageView imageView5,imageView12;
    EditText editText3,editText4;
    static final int PICK_CONTACT = 1;
    Double bikebase,bikekms,bikemins,apebase,apekms,apemins,acebase,acekms,acemins,dostbase,dostkms,dostmins,t407base,t407kms,t407mins;
    String taxi1 = "def";
    String mycoupon = "def"; //Added by shameem 26/12/2019
    ScrollView scroll2;
   // Context context;
    Button applycoupon,button7;
    final int RQS_PICKCONTACT = 1;
    String phoneNumber="";
    LinearLayout help1,pack1;
    private LatLng origin;
    Double clat,clong,dlat,dlong,dlat1,dlong1,dlat2,dlong2,dlat3,dlong3,dlat4,dlong4,dlat5,dlong5,dlat6,dlong6,dlat7,dlong7,dlat8,dlong8;
    private LatLng destination, destination1,destination2,destination3,destination4,destination5,destination6,destination7,destination8;
    private String networkAvailable;
    private String tryAgain;
    int ef =1;
    int ft = 0;
    private String directionRequest;
    TextView textView1, textView2, textView3,textView6, textView4, textView5, txt_name, txt_number, txt_fare, title, txt_vehiclename,txt_goodstype,txt_insurance,txt_payment;
    SessionManager sessionManager;
    ImageButton phone,imageButton7,imageButton2;
    ;
    ;
    String driver_id,dl;

    String in ="99" ;
    String newin="99";
    String in1 ;
    private String user_id;
    private String pickup_address;
    private String drop_address  = " ";
    private String drop_address1 = "def",drop_address2  = "def",drop_address3  = "def",drop_address4  = "def",drop_address5  = "def" ,drop_address6  = "def" ,drop_address7  = "def" ,drop_address8  = "def";
    String distance,ride1,goodstype1=" ";
    private String drivername = "";
    SwipeRefreshLayout swipeRefreshLayout;
    Pass pass;
    TextView calculateFare;
    Double ff1= 0.0;
    Double ff;
    Double sf;
    Double ff2= 0.0;
    Double ff8= 0.0;
    Double basefare=0.0;
    String Faretxt ; //Added Newly estimated value stored 21/12/2019
    int dl1;
   TextView goodstext;

//    private ContentResolver contentResolver = getActivity().getContentResolver() ;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        networkAvailable = getResources().getString(R.string.network);
        tryAgain = getResources().getString(R.string.try_again);
        directionRequest = getResources().getString(R.string.direction_request);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.request_ride, container, false);

        if (!CheckConnection.haveNetworkConnection(getActivity())) {
            Toast.makeText(getActivity(), networkAvailable, Toast.LENGTH_LONG).show();
        }
       // context = container.getContext();
        bindView(savedInstanceState);
        taxi.setVisibility(View.GONE);
        bike.performClick();
        any.performClick();
        NeaBy1();
        pick_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.dialog2);
                dialog.setTitle("SELECT PICKUP DETAILS");

            //    dialog.setTitle("SELECT PICKUP DETAILS");
                Button select_date = dialog.findViewById(R.id.select_date);
                Button select_time = dialog.findViewById(R.id.select_time);
                final TextView pickup_date = dialog.findViewById(R.id.date23);
                final TextView pickup_time = dialog.findViewById(R.id.pickup_time1);
                Button next = dialog.findViewById(R.id.btn_next);



                select_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        // Create a new OnDateSetListener instance. This listener will be invoked when user click ok button in DatePickerDialog.
                        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                StringBuffer strBuf = new StringBuffer();
                                strBuf.append("You select date is ");
                                strBuf.append(year);
                                strBuf.append("-");
                                strBuf.append(month+1);
                                strBuf.append("-");
                                strBuf.append(dayOfMonth);

                                Calendar cal = Calendar.getInstance();
                                cal.setTimeInMillis(0);
                                cal.set(year, month, dayOfMonth, 0, 0, 0);
                                Date chosenDate = cal.getTime();

                                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                                SimpleDateFormat fmt2 = new SimpleDateFormat("dd-MM-yyyy");

                                Log.d("dd", fmt2.format(chosenDate));
                                pickup_date.setText(fmt2.format(chosenDate));

                               // pickup_date.setText(dayOfMonth + "-" + "0"+(month + 1) + "-" + year);
                                date1 = pickup_date.getText().toString();
                            }
                        };

                        // Get current year, month and day.
                        Calendar now = Calendar.getInstance();
                        int year = now.get(java.util.Calendar.YEAR);
                        int month = now.get(java.util.Calendar.MONTH);
                        int day = now.get(java.util.Calendar.DAY_OF_MONTH);

                        // Create the new DatePickerDialog instance.
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onDateSetListener, year, month, day);

                        // Set dialog icon and title.

                        datePickerDialog.setTitle("Please select date.");

                        // Popup the dialog.
                        datePickerDialog.show();
                    }
                });

                pickup_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        // Create a new OnDateSetListener instance. This listener will be invoked when user click ok button in DatePickerDialog.
                        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                StringBuffer strBuf = new StringBuffer();
                                strBuf.append("You select date is ");
                                strBuf.append(year);
                                strBuf.append("-");
                                strBuf.append(month+1);
                                strBuf.append("-");
                                strBuf.append(dayOfMonth);


                                pickup_date.setText(dayOfMonth + "-" + "0"+(month + 1) + "-" + year);
                                      date1 = pickup_date.getText().toString();
                            }
                        };

                        // Get current year, month and day.
                        Calendar now = Calendar.getInstance();
                        int year = now.get(java.util.Calendar.YEAR);
                        int month = now.get(java.util.Calendar.MONTH);
                        int day = now.get(java.util.Calendar.DAY_OF_MONTH);

                        // Create the new DatePickerDialog instance.
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onDateSetListener, year, month, day);

                        // Set dialog icon and title.

                        datePickerDialog.setTitle("Please select date.");

                        // Popup the dialog.
                        datePickerDialog.show();
                    }
                });

                select_time.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                StringBuffer strBuf = new StringBuffer();
                                strBuf.append("You select time is ");
                                strBuf.append(hour);
                                strBuf.append(":");
                                strBuf.append(minute);

                                pickup_time.setText(String.format("%02d:%02d",hour,minute));
                                time1 = pickup_time.getText().toString();

                            }
                        };

                        Calendar now = Calendar.getInstance();
                        int hour = now.get(java.util.Calendar.HOUR_OF_DAY);
                        int minute = now.get(java.util.Calendar.MINUTE);

                        // Whether show time in 24 hour format or not.
                        boolean is24Hour = true;

                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hour, minute, is24Hour);

                        timePickerDialog.setTitle("Please select time.");

                        timePickerDialog.show();

                    }
                });

                pickup_time.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                StringBuffer strBuf = new StringBuffer();
                                strBuf.append("You select time is ");
                                strBuf.append(hour);
                                strBuf.append(":");
                                strBuf.append(minute);
                                pickup_time.setText(String.format("%02d:%02d",hour,minute));
                                time1 = pickup_time.getText().toString();

                            }
                        };

                        Calendar now = Calendar.getInstance();
                        int hour = now.get(java.util.Calendar.HOUR_OF_DAY);
                        int minute = now.get(java.util.Calendar.MINUTE);

                        // Whether show time in 24 hour format or not.
                        boolean is24Hour = true;

                        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hour, minute, is24Hour);

                        timePickerDialog.setTitle("Please select time.");

                        timePickerDialog.show();

                    }
                });

                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        calculateDistance();

                        //making date and time row visible in request_ride.xml
                        date_flag = 1;
                        ride1 = "1";
                        if (destination.toString().matches("")) {
                            Toast.makeText(getActivity(), "Enter Drop Location", Toast.LENGTH_LONG).show(); // not null not empty
                            Log.d("dp", "true");

                        } else if (goodstext.getText().toString().matches("Select Goods Type")) {
                            Toast.makeText(getActivity(), "Select Goods Type", Toast.LENGTH_LONG).show(); // not null not empty
                            Log.d("dp", "true");

                        } else {

                            Log.d("dp", "false");
                            request_ride_method1();

                        }
                        dialog.dismiss();


                    }
                });

                dialog.show();


            }
        });

        applycoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                applycoupon();

            }
        });

        pick_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!CheckConnection.haveNetworkConnection(getActivity())) {
                    Toast.makeText(getActivity(), networkAvailable, Toast.LENGTH_LONG).show();
                } else {
//                    int r = Integer.parseInt(ride1);
                    if (distance == null) {
                        Toast.makeText(getActivity(), getString(R.string.invalid_distance), Toast.LENGTH_LONG).show();
                    } else if (sessionManager.getKEY() == null || sessionManager.getKEY().equals("")) {
                        Toast.makeText(getActivity(), getString(R.string.relogin), Toast.LENGTH_SHORT).show();
                    } else if (fare == null) {
                        Toast.makeText(getActivity(), getString(R.string.invalid_fare), Toast.LENGTH_SHORT).show();
                    }

                    else {
                        String d =  " ";
                        String d1 = "def";
                        String d2 = "def";
                        String d3 = "def";
                        String d4 = "def";
                        String d5 = "def";
                        String d6 = "def";
                        String d7 = "def";
                        String d8 = "def";

                        dl = pass.getdl();
                        int dl1 = Integer.parseInt(dl);
                        String o = clat + "," + clong;

                       if(dl1 == 1) {
                            d = dlat+ "," + dlong;
                        }
                        if(dl1 == 2) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                        }
                        if(dl1 == 3) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;

                        }
                        if(dl1 == 4) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;
                            d3 = dlat3+ "," + dlong3;



                        }
                        if(dl1 == 5) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;
                            d3 = dlat3+ "," + dlong3;
                            d4 = dlat4+ "," + dlong4;


                        }
                        if(dl1 == 6) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;
                            d3 = dlat3+ "," + dlong3;
                            d4 = dlat4+ "," + dlong4;
                            d5 = dlat5+ "," + dlong5;



                        }
                        if(dl1 == 7) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;
                            d3 = dlat3+ "," + dlong3;
                            d4 = dlat4+ "," + dlong4;
                            d5 = dlat5+ "," + dlong5;
                            d6 = dlat6+ "," + dlong6;



                        }
                        if(dl1 == 8) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;
                            d3 = dlat3+ "," + dlong3;
                            d4 = dlat4+ "," + dlong4;
                            d5 = dlat5+ "," + dlong5;
                            d6 = dlat6+ "," + dlong6;
                            d7 = dlat7+ "," + dlong7;


                        }
                        if(dl1 == 9) {
                            d = dlat+ "," + dlong;
                            d1 = dlat1+ "," + dlong1;
                            d2 = dlat2+ "," + dlong2;
                            d3 = dlat3+ "," + dlong3;
                            d4 = dlat4+ "," + dlong4;
                            d5 = dlat5+ "," + dlong5;
                            d6 = dlat6+ "," + dlong6;
                            d7 = dlat7+ "," + dlong7;
                            d8 = dlat8+ "," + dlong8;


                        }
                        if(dl1 == 10) {
                            d = destination.latitude + "," + destination.longitude;
                            d1 = destination1.latitude + "," + destination1.longitude;
                            d2 = destination2.latitude + "," + destination2.longitude;
                            d3 = destination3.latitude + "," + destination3.longitude;
                            d4 = destination4.latitude + "," + destination4.longitude;
                            d5 = destination5.latitude + "," + destination5.longitude;
                            d6 = destination6.latitude + "," + destination6.longitude;
                            d7 = destination7.latitude + "," + destination7.longitude;
                            d8 = destination8.latitude + "," + destination8.longitude;


                        }

                        AddRide(sessionManager.getKEY(), pickup_address, drop_address, o, d,d1,d2,d3,d4,d5,d6,d7,d8, String.valueOf(finalfare), distance, goodstype1,t,s,drop_address1,drop_address2,drop_address3,drop_address4,drop_address5,drop_address6,drop_address7,drop_address8);
                    }
                }


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HomeActivity.class));

            }
        });

        open1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t = "1";
                bike.setVisibility(View.VISIBLE);

                open1.setBackgroundColor(getResources().getColor(R.color.red1));
                close1.setBackgroundResource(R.drawable.bg3);
                any.setBackgroundResource(R.drawable.bg3);
                open1.setTextColor(getResources().getColor(R.color.white));
                close1.setTextColor(getResources().getColor(R.color.red1));
                any.setTextColor(getResources().getColor(R.color.red1));
                bike_img.setImageResource(R.drawable.bike);
                micro_img.setImageResource(R.drawable.ace);
                mini_img.setImageResource(R.drawable.ape);
                sedan_img.setImageResource(R.drawable.t407);
                xuv_img.setImageResource(R.drawable.dost);

                taxi.setVisibility(View.GONE);


            }
        });



        linear_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                custom_dialog();

                //  goods_dialog(vectype);
                Log.d("vectype", "vectype: " + vectype);


            }
        });

        bike.setOnClickListener(new View.OnClickListener() {                          //bike
            @Override
            public void onClick(View v) {
                s = "1";
                in = "0";
                newin="0";
                vin = "0";
                ft = 0;
                discount = 0.0;
                discount1 = 0.0;
                vectype = "bike";
                llTrack.setVisibility(View.GONE);
                scroll2.scrollTo(0, 120);
                goodslay.setVisibility(View.VISIBLE);
                imageView12.setImageResource(R.drawable.delivery);
                helper = "def";
                help = "no";
                helperlay.setVisibility(View.GONE);
                taxi.setVisibility(View.VISIBLE);
                goodstext.setText("Delivery");
                bike.setBackgroundColor(getResources().getColor(R.color.divider1));
                micro.setBackgroundColor(getResources().getColor(R.color.white));
                mini.setBackgroundColor(getResources().getColor(R.color.white));
                sedan.setBackgroundColor(getResources().getColor(R.color.white));
                xuv.setBackgroundColor(getResources().getColor(R.color.white));
                txt_fare.setText("Estimate Fare");
            }
        });

        taxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goodslay.setVisibility(View.GONE);
                taxi1 = "yes";
                goodstype = "taxi";
                scroll2.scrollBy(0,120);
                dl = pass.getdl();
                int dl1 = Integer.parseInt(dl);
                if(dl1 == 1)
                {
                   calculateDistance1();
                }

                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setIcon(R.drawable.xgo_icon);
                    builder.setTitle(Html.fromHtml("<font color='#4678F6'>Drop Confirmation</font>"));
                    builder.setMessage(" Taxi option permits one Drop point. Are you sure you want to continue ?? ");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            calculateDistance1();
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }

            }
        });

        micro.setOnClickListener(new View.OnClickListener() {                  //ACE
            @Override
            public void onClick(View v) {
                ft = 0;
                s = "3";
                in = "7";
                newin = "7";
                goodslay.setVisibility(View.VISIBLE);
//                scroll2.scrollTo(0, 280);
                scroll2.scrollBy(0,120);
                vectype = "micro";
                int t1 = Integer.parseInt(t);
                textView8.setText("Select packages");
                close2.setVisibility(View.GONE);
                imageButton2.setVisibility(View.VISIBLE);
                llTrack.setVisibility(View.VISIBLE);
                taxi.setVisibility(View.GONE);
                helperlay.setVisibility(View.VISIBLE);
                imageView12.setImageResource(R.drawable.goods);
                discount = 0.0;
                discount1 = 0.0;
                goodstext.setText("Select Goods Type");
                txt_fare.setText("Estimate Fare");

                bike.setBackgroundColor(getResources().getColor(R.color.white));
                micro.setBackgroundColor(getResources().getColor(R.color.divider1));
                mini.setBackgroundColor(getResources().getColor(R.color.white));
                sedan.setBackgroundColor(getResources().getColor(R.color.white));
                xuv.setBackgroundColor(getResources().getColor(R.color.white));
            }
        });

        mini.setOnClickListener(new View.OnClickListener() {                //APE
            @Override
            public void onClick(View v) {
                ft = 0;
                s = "2";
                in = "8";
                newin="8";
                vin = "0";
                discount = 0.0;
                discount1 = 0.0;
                vectype = "mini";
                goodslay.setVisibility(View.VISIBLE);
                helperlay.setVisibility(View.VISIBLE);
                scroll2.scrollTo(0, 120);
                int t1 = Integer.parseInt(t);
                llTrack.setVisibility(View.GONE);
                taxi.setVisibility(View.GONE);
                goodstext.setText("Select Goods Type");
                imageView12.setImageResource(R.drawable.goods);
                bike.setBackgroundColor(getResources().getColor(R.color.white));
                micro.setBackgroundColor(getResources().getColor(R.color.white));
                mini.setBackgroundColor(getResources().getColor(R.color.divider1));
                sedan.setBackgroundColor(getResources().getColor(R.color.white));
                xuv.setBackgroundColor(getResources().getColor(R.color.white));
                txt_fare.setText("Estimate Fare");

            }
        });

        sedan.setOnClickListener(new View.OnClickListener() {                  //407
            @Override
            public void onClick(View v) {
                ft = 0;
                in = "9";
                newin = "9";
                vin = "0";
                s = "5";
                discount = 0.0;
                discount1 = 0.0;
                vectype = "sedan";
                int t1 = Integer.parseInt(t);
                goodslay.setVisibility(View.VISIBLE);
                helperlay.setVisibility(View.VISIBLE);
                scroll2.scrollTo(0, 120);
                textView8.setText("Select packages");
                close2.setVisibility(View.GONE);
                imageButton2.setVisibility(View.VISIBLE);
                llTrack.setVisibility(View.VISIBLE);
                taxi.setVisibility(View.GONE);
                goodstext.setText("Select Goods Type");
                imageView12.setImageResource(R.drawable.goods);
                bike.setBackgroundColor(getResources().getColor(R.color.white));
                micro.setBackgroundColor(getResources().getColor(R.color.white));
                mini.setBackgroundColor(getResources().getColor(R.color.white));
                sedan.setBackgroundColor(getResources().getColor(R.color.divider1));
                xuv.setBackgroundColor(getResources().getColor(R.color.white));
                txt_fare.setText("Estimate Fare");
            }
        });

        xuv.setOnClickListener(new View.OnClickListener() {           //DOSHT
            @Override
            public void onClick(View v) {
                ft = 0;
                textView8.setText("Select packages");
                close2.setVisibility(View.GONE);
                imageButton2.setVisibility(View.VISIBLE);
                llTrack.setVisibility(View.VISIBLE);
                taxi.setVisibility(View.GONE);
                helperlay.setVisibility(View.VISIBLE);
                goodstext.setText("Select Goods Type");
                scroll2.scrollTo(0, 120);
                in = "10";
                newin = "10";
                discount = 0.0;
                discount1 = 0.0;
                vin = "0";
                s = "4";
                vectype = "xuv";
                int t1 = Integer.parseInt(t);
                imageView12.setImageResource(R.drawable.goods);
                bike.setBackgroundColor(getResources().getColor(R.color.white));
                micro.setBackgroundColor(getResources().getColor(R.color.white));
                mini.setBackgroundColor(getResources().getColor(R.color.white));
                sedan.setBackgroundColor(getResources().getColor(R.color.white));
                xuv.setBackgroundColor(getResources().getColor(R.color.divider1));
                txt_fare.setText("Estimate Fare");
            }
        });

        goodslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discount = 0.0;
                discount1 = 0.0;
                calculateDistance();

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                /* builder.setTitle(Html.fromHtml("<font color='#4678F6'>Select GoodsType</font>")); */
                TextView title = new TextView(getActivity());
                /* You Can Customise your Title here */
                title.setText("Select GoodsType");
                title.setBackgroundColor(getResources().getColor(R.color.white));
                title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                title.setPadding(10, 20, 10, 10);
                title.setGravity(Gravity.CENTER);
                title.setTextColor(getResources().getColor(R.color.blue));
                title.setTextSize(20);

                builder.setCustomTitle(title);
                if (s == "1") {
                    builder.setItems(val1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            String i = String.valueOf(val1[item]);
                            Log.d("list", i);
                            goodstext.setText(i);
                            goodstype = i;

                            taxi.setVisibility(View.GONE);
                            scroll2.scrollBy(0,180);
                            Toast.makeText(getActivity(), i, Toast.LENGTH_LONG);
                        }
                    });
                } else {
                    builder.setItems(val, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            String i = String.valueOf(val[item]);
                            Log.d("list", i);
                            goodstext.setText(i);
                            taxi.setVisibility(View.GONE);
                            scroll2.scrollBy(0,280);
                            Toast.makeText(getActivity(), i, Toast.LENGTH_LONG);
                        }
                    });
                }
                android.app.AlertDialog alert = builder.create();
                alert.show();
            }
        });
        helperlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateDistance();

                if (flag1 == true) {
                    imageButton8.setImageResource(R.drawable.helper1);
                    help = "yes";
                    helper_text5.setText("₹ 170");
                    flag1 = false;
                    btn = 2;
                    in = "70";
                  Log.d("flag1", "false");
                    txt_fare.setText("Estimate Fare");

                } else {
                    imageButton8.setImageResource(R.drawable.helper);
                    help = "No";
                    helper_text5.setText("Helper");
                    flag1 = true;
                    btn = 1;
                    in = "71";
                    Log.d("flag1", "true");
                    txt_fare.setText("Estimate Fare");

                }

            }
        });
//
//        pick_now.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                date_flag = 0;
//
//
//                if (pi==1) {
//                    Toast.makeText(getActivity(), "Enter Pickup Location", Toast.LENGTH_LONG).show(); // not null not empty
//                    Log.d("dp", "true");
//
//                }
//                    else if (drop_location.getText().toString().matches("")) {
//                    Toast.makeText(getActivity(), "Enter Drop Location", Toast.LENGTH_LONG).show(); // not null not empty
//                    Log.d("dp", "true");
//
//                } else if (goodstext.getText().toString().matches("Select Goods Type")) {
//                    Toast.makeText(getActivity(), "Select Goods Type", Toast.LENGTH_LONG).show(); // not null not empty
//                    Log.d("dp", "true");
//
//                } else {
//
//                    Log.d("dp", "false");
//                    request_ride_method();
//
//                }
//
//
//            }
//        });


        imageButton2.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                //txt_fare.setText("Estimate Fare"); commented on 21/12/2019

                //   close2.setVisibility(View.VISIBLE);
                s1 = Integer.parseInt(s);
                Log.d("testdata", s);
                if (s1 == 1) {
                    Toast.makeText(getActivity(), "No package avaiable For Bikes", Toast.LENGTH_LONG).show();
                }
                if (s1 == 3) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.packages);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();
                    title.setText("3 hours Package");
                    contenta.setText("₹ 13/km after 40 Kms");
                    contentb.setText("₹ 3.5/min after 3.5hrs");
                    button4.setText(" ₹ 700");

                    title1.setText("7 hours Package");
                    content1.setText("₹ 10/km after 70 Kms");
                    content2.setText("₹ 1.75/min after 7hrs");
                    priceb.setText(" ₹ 1300");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "1";
                            textView8.setText("3 hours Package - Tata Ace");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "2";
                            textView8.setText("7 hours Package - Tata Ace");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });
                }

                if (s1 == 2) {
                    Toast.makeText(getActivity(), "No package avaiable For Tata Ape", Toast.LENGTH_LONG).show();
                }
                if (s1 == 5) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.packages);
                    //    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();

                    title.setText("5.5 hours Package");
                    contenta.setText("₹ 21/km after 50 Kms");
                    contentb.setText("₹ 2.25/min after 5.5hrs");
                    button4.setText(" ₹ 1650");

                    title1.setText("9 hours Package");
                    content1.setText("₹ 21/km after 70 Kms");
                    content2.setText("₹ 2.25/min after 9hrs");
                    priceb.setText(" ₹ 2850");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "3";
                            textView8.setText("5.5 hours Package - Tata 407");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "4";
                            textView8.setText("9 hours Package - Tata 407");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });
                }

                if (s1 == 4) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.packages);

                    //    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();

                    title.setText("3.5 hours Package");
                    contenta.setText("₹ 14/km after 40 Kms");
                    contentb.setText("₹ 4/min after 3.5hrs");
                    button4.setText(" ₹ 850");

                    title1.setText("7 hours Package");
                    content1.setText("₹ 11/km after 70 Kms");
                    content2.setText("₹ 2.25/min after 7hrs");
                    priceb.setText(" ₹ 1500");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "5";
                            //newin = "5";
                            textView8.setText("3.5 hours Package - Tata Dost");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "6";
                            //newin = "6";
                            textView8.setText("7 hours Package - Tata Dost");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });
                }

                else {
                    Toast.makeText(getActivity(), getString(R.string.SELECT_VEHICLE), Toast.LENGTH_LONG).show();

                }

            }
        });

        textView8.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                txt_fare.setText("Estimate Fare");

                //   close2.setVisibility(View.VISIBLE);
                s1 = Integer.parseInt(s);
                Log.d("testdata", s);
                if (s1 == 1) {
                    Toast.makeText(getActivity(), "No package avaiable For Bikes", Toast.LENGTH_LONG).show();
                }
                if (s1 == 3) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.packages);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();
                    title.setText("3 hours Package");
                    contenta.setText("₹ 13/km after 40 Kms");
                    contentb.setText("₹ 3.5/min after 3.5hrs");
                    button4.setText(" ₹ 700");

                    title1.setText("7 hours Package");
                    content1.setText("₹ 10/km after 70 Kms");
                    content2.setText("₹ 1.75/min after 7hrs");
                    priceb.setText(" ₹ 1300");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "1";
                            textView8.setText("3 hours Package - Tata Ace");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "2";
                            textView8.setText("7 hours Package - Tata Ace");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });
                }
                if (s1 == 2) {
                    Toast.makeText(getActivity(), "No package avaiable For Tata Ape", Toast.LENGTH_LONG).show();
                }
                if (s1 == 5) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.packages);
                    //    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();

                    title.setText("5.5 hours Package");
                    contenta.setText("₹ 21/km after 50 Kms");
                    contentb.setText("₹ 2.25/min after 5.5hrs");
                    button4.setText(" ₹ 1650");

                    title1.setText("9 hours Package");
                    content1.setText("₹ 21/km after 70 Kms");
                    content2.setText("₹ 2.25/min after 9hrs");
                    priceb.setText(" ₹ 2850");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "3";
                            textView8.setText("5.5 hours Package - Tata 407");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "4";
                            textView8.setText("9 hours Package - Tata 407");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });
                }

                if (s1 == 4) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.packages);

                    //    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    final TextView title = dialog.findViewById(R.id.title);
                    final TextView title1 = dialog.findViewById(R.id.title1);
                    final TextView contenta = dialog.findViewById(R.id.contenta);
                    final TextView contentb = dialog.findViewById(R.id.contentb);
                    final TextView content1 = dialog.findViewById(R.id.content1);
                    final TextView content2 = dialog.findViewById(R.id.content2);
                    Button priceb = dialog.findViewById(R.id.priceb);
                    Button button4 = dialog.findViewById(R.id.button4);
                    dialog.show();

                    title.setText("3.5 hours Package");
                    contenta.setText("₹ 14/km after 40 Kms");
                    contentb.setText("₹ 4/min after 3.5hrs");
                    button4.setText(" ₹ 850");

                    title1.setText("7 hours Package");
                    content1.setText("₹ 11/km after 70 Kms");
                    content2.setText("₹ 2.25/min after 7hrs");
                    priceb.setText(" ₹ 1500");

                    button4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "5";
                            newin = "5";
                            textView8.setText("3.5 hours Package - Tata Dost");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });

                    priceb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            in = "6";
                            newin = "6";
                            textView8.setText("7 hours Package - Tata Dost");
                            dialog.dismiss();
                            imageButton2.setVisibility(View.GONE);
                            close2.setVisibility(View.VISIBLE);
                            calculateDistance();
                        }
                    });
                }

                else {
                    Toast.makeText(getActivity(), getString(R.string.SELECT_VEHICLE), Toast.LENGTH_LONG).show();

                }

            }
        });

        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t = "2";
                open1.setBackgroundResource(R.drawable.bg3);
                close1.setBackgroundColor(getResources().getColor(R.color.red1));
                any.setBackgroundResource(R.drawable.bg3);
                bike.setVisibility(View.GONE);
                open1.setTextColor(getResources().getColor(R.color.red1));
                close1.setTextColor(getResources().getColor(R.color.white));
                any.setTextColor(getResources().getColor(R.color.red1));
                bike_img.setImageResource(R.drawable.bike);
                micro_img.setImageResource(R.drawable.ace2);
                mini_img.setImageResource(R.drawable.ape1);
                sedan_img.setImageResource(R.drawable.t4072);
                xuv_img.setImageResource(R.drawable.dost1);
                taxi.setVisibility(View.GONE);
            }
        });

        any.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t = "3";
                bike.setVisibility(View.VISIBLE);

                open1.setBackgroundResource(R.drawable.bg3);
                close1.setBackgroundResource(R.drawable.bg3);
                any.setBackgroundColor(getResources().getColor(R.color.red1));
                open1.setTextColor(getResources().getColor(R.color.red1));
                close1.setTextColor(getResources().getColor(R.color.red1));
                any.setTextColor(getResources().getColor(R.color.white));
                bike_img.setImageResource(R.drawable.bike);
                micro_img.setImageResource(R.drawable.ace);
                mini_img.setImageResource(R.drawable.ape);
                sedan_img.setImageResource(R.drawable.t407);
                xuv_img.setImageResource(R.drawable.dost);


            }
        });


        goodstext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateDistance();

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                //builder.setTitle(Html.fromHtml("<font color='#4678F6'>Select GoodsType</font>"));
                TextView title = new TextView(getActivity());
// You Can Customise your Title here
                title.setText("Select GoodsType");
                title.setBackgroundColor(getResources().getColor(R.color.white));
                title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                title.setPadding(10, 20, 10, 10);
                title.setGravity(Gravity.CENTER);
                title.setTextColor(getResources().getColor(R.color.blue));
                title.setTextSize(20);

                builder.setCustomTitle(title);
                if (s == "1") {
                    builder.setItems(val1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            String i = String.valueOf(val1[item]);
                            Log.d("list", i);
                            goodstext.setText(i);
                            goodstype = i;

                            taxi.setVisibility(View.GONE);

                            Toast.makeText(getActivity(), i, Toast.LENGTH_LONG);
                        }
                    });
                } else {
                    builder.setItems(val, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            String i = String.valueOf(val[item]);
                            Log.d("list", i);
                            goodstext.setText(i);
                            taxi.setVisibility(View.GONE);

                            Toast.makeText(getActivity(), i, Toast.LENGTH_LONG);
                        }
                    });
                }
                android.app.AlertDialog alert = builder.create();
                alert.show();
            }
        });




        imageButton8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag1 == true) {
                    imageButton8.setImageResource(R.drawable.helper1);
                    help = "yes";
                    helper_text5.setText("₹ 170");
                    flag1 = false;
                    btn = 2;
                    in1 = "70";
                    Log.d("flag1", "false");
                } else {
                    imageButton8.setImageResource(R.drawable.helper);
                    help = "No";
                    helper_text5.setText("Helper");
                    flag1 = true;
                    btn = 1;
                    in1 = "71";
                   Log.d("flag1", "true");
                }

            }
        });

        imageView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // textView8.setText("Select packages");
               // close2.setVisibility(View.GONE);
                //imageButton2.setVisibility(View.VISIBLE);


                calculateDistance();

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                // builder.setTitle(Html.fromHtml("<font color='#4678F6'>Select GoodsType</font>"));
                TextView title = new TextView(getActivity());
// You Can Customise your Title here
                title.setText("Select GoodsType");
                title.setBackgroundColor(getResources().getColor(R.color.white));
                title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                title.setPadding(10, 20, 10, 10);
                title.setGravity(Gravity.CENTER);
                title.setTextColor(getResources().getColor(R.color.blue));
                title.setTextSize(20);

                builder.setCustomTitle(title);
                if (s == "1") {
                    builder.setItems(val1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            String i = String.valueOf(val1[item]);
                            Log.d("list", i);
                            goodstext.setText(i);

                            goodstype = i;
                            taxi.setVisibility(View.GONE);

                            Toast.makeText(getActivity(), i, Toast.LENGTH_LONG);
                        }
                    });
                } else {
                    builder.setItems(val, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {

                            String i = String.valueOf(val[item]);
                            Log.d("list", i);
                            goodstext.setText(i);
                            taxi.setVisibility(View.GONE);

                            Toast.makeText(getActivity(), i, Toast.LENGTH_LONG);
                        }
                    });
                }
                android.app.AlertDialog alert = builder.create();
                alert.show();
            }
        });

        close2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///s1 = 10;
                close2.setVisibility(View.GONE);
                imageButton2.setVisibility(View.VISIBLE);
                textView8.setText("Select Packages");
                txt_fare.setText("Estimate Fare");

                Log.d("Goodstype",goodstext.getText().toString());
                String goodsval = goodstext.getText().toString();
                if (goodsval == "Select Goods Type") {
                    txt_fare.setText("Estimate Fare");
               }else{
                   txt_fare.setText(Faretxt);
                }
                calculateDistance();

            }
        });

        return view;
    }

    private void request_ride_method1() {

        if (!CheckConnection.haveNetworkConnection(getActivity())) {
            Toast.makeText(getActivity(), networkAvailable, Toast.LENGTH_LONG).show();
        } else {
           String time = pass.getTime();
            String date = pass.getDate();
            ride1 = pass.getRide1();
//                    int r = Integer.parseInt(ride1);
            if (distance == null) {
                Toast.makeText(getActivity(), getString(R.string.invalid_distance), Toast.LENGTH_LONG).show();
            } else if (sessionManager.getKEY() == null || sessionManager.getKEY().equals("")) {
                Toast.makeText(getActivity(), getString(R.string.relogin), Toast.LENGTH_SHORT).show();
            } else if (fare == null) {
                Toast.makeText(getActivity(), getString(R.string.invalid_fare), Toast.LENGTH_SHORT).show();
            }
            else {

                String d =  " ";
                String d1 = "def";
                String d2 = "def";
                String d3 = "def";
                String d4 = "def";
                String d5 = "def";
                String d6 = "def";
                String d7 = "def";
                String d8 = "def";

                dl = pass.getdl();
                int dl1 = Integer.parseInt(dl);

                String o = clat + "," + clong;
                if(dl1 == 1) {
                    d = dlat+ "," + dlong;
                }
                if(dl1 == 2) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                }
                if(dl1 == 3) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;

                }
                if(dl1 == 4) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;
                    d3 = dlat3+ "," + dlong3;



                }
                if(dl1 == 5) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;
                    d3 = dlat3+ "," + dlong3;
                    d4 = dlat4+ "," + dlong4;


                }
                if(dl1 == 6) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;
                    d3 = dlat3+ "," + dlong3;
                    d4 = dlat4+ "," + dlong4;
                    d5 = dlat5+ "," + dlong5;



                }
                if(dl1 == 7) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;
                    d3 = dlat3+ "," + dlong3;
                    d4 = dlat4+ "," + dlong4;
                    d5 = dlat5+ "," + dlong5;
                    d6 = dlat6+ "," + dlong6;



                }
                if(dl1 == 8) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;
                    d3 = dlat3+ "," + dlong3;
                    d4 = dlat4+ "," + dlong4;
                    d5 = dlat5+ "," + dlong5;
                    d6 = dlat6+ "," + dlong6;
                    d7 = dlat7+ "," + dlong7;


                }
                if(dl1 == 9) {
                    d = dlat+ "," + dlong;
                    d1 = dlat1+ "," + dlong1;
                    d2 = dlat2+ "," + dlong2;
                    d3 = dlat3+ "," + dlong3;
                    d4 = dlat4+ "," + dlong4;
                    d5 = dlat5+ "," + dlong5;
                    d6 = dlat6+ "," + dlong6;
                    d7 = dlat7+ "," + dlong7;
                    d8 = dlat8+ "," + dlong8;


                }
                if(dl1 == 10) {
                    d = destination.latitude + "," + destination.longitude;
                    d1 = destination1.latitude + "," + destination1.longitude;
                    d2 = destination2.latitude + "," + destination2.longitude;
                    d3 = destination3.latitude + "," + destination3.longitude;
                    d4 = destination4.latitude + "," + destination4.longitude;
                    d5 = destination5.latitude + "," + destination5.longitude;
                    d6 = destination6.latitude + "," + destination6.longitude;
                    d7 = destination7.latitude + "," + destination7.longitude;
                    d8 = destination8.latitude + "," + destination8.longitude;


                }



                AddRide1(sessionManager.getKEY(), pickup_address, drop_address, o, d, d1, d2, d3, d4, d5, d6, d7, d8, String.valueOf(finalfare), distance, goodstype1,t,s,drop_address1,drop_address2,drop_address3,drop_address4,drop_address5,drop_address6,drop_address7,drop_address8);
            }



        }
    }
    public void AddRide1(String key, String pickup_adress, String drop_address, String pickup_location, String drop_locatoin, String drop_locatoin1, String drop_locatoin2, String drop_locatoin3, String drop_locatoin4, String drop_locatoin5, String drop_locatoin6, String drop_locatoin7, String drop_locatoin8, String amount, String distance, String goodstype1, String t, String s, String drop_address1, String drop_address2, String drop_address3, String drop_address4, String drop_address5, String drop_address6, String drop_address7, String drop_address8) {
        calculateDistance();

        String gtype = goodstext.getText().toString();

        final RequestParams params = new RequestParams();
        params.put("user_id", user_id);
        params.put("pickup_adress", pickup_adress);
        params.put("drop_address", drop_address);
        params.put("pikup_location", pickup_location);
        params.put("drop_locatoin", drop_locatoin);
        params.put("drop_locatoin1", drop_locatoin1);
        params.put("drop_locatoin2", drop_locatoin2);
        params.put("drop_locatoin3", drop_locatoin3);
        params.put("drop_locatoin4", drop_locatoin4);
        params.put("drop_locatoin5", drop_locatoin5);
        params.put("drop_locatoin6", drop_locatoin6);
        params.put("drop_locatoin7", drop_locatoin7);
        params.put("drop_locatoin8", drop_locatoin8);
        params.put("drop_address1" , drop_address1);
        params.put("drop_address2" , drop_address2);
        params.put("drop_address3" , drop_address3);
        params.put("drop_address4" , drop_address4);
        params.put("drop_address5" , drop_address5);
        params.put("drop_address6" , drop_address6);
        params.put("drop_address7" , this.drop_address7);
        params.put("drop_address8" , this.drop_address8);
        params.put("helper",helper);
        params.put("amount", finalfare);
        params.put("distance", distance);
        params.put("goodstype", gtype);
        params.put("vehicle_info", s);
        params.put("vtype", t);
        params.put("add_date",date1);
        params.put("add_time",time1);
        params.put("packages",in);
        Server.setHeader(key);
        Log.d("flag1", ""+params);
        Server.get("api/user/addRide1/format/json", params, new JsonHttpResponseHandler() {


             @Override
            public void onStart() {
                super.onStart();
                // swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Toast.makeText(getActivity(), getString(R.string.ride_has_been_requested), Toast.LENGTH_LONG).show();
                        ScheduledRequestfragment acceptedRequestFragment = new ScheduledRequestfragment();
                        Bundle bundle;

                        bundle = new Bundle();
                        bundle.putString("status", "PENDING");
                        acceptedRequestFragment.setArguments(bundle);
                        ((HomeActivity) getActivity()).changeFragment(acceptedRequestFragment, "Requests");
                    } else {
                        Toast.makeText(getActivity(), "RIDE NOT AVAILABLE", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "RIDE NOT AVAILABLE", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getActivity(), "Ride Not Available", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getActivity(), "Ride Not Available", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                    // swipeRefreshLayout.setRefreshing(false);

                }

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public void bindView(Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).fontToTitleBar(getString(R.string.ride_request));
        ((HomeActivity) getActivity()).toolbar.setTitle(getString(R.string.request_ride));
        mapView = (MapView) view.findViewById(R.id.mapview);
        calculateFare = (TextView) view.findViewById(R.id.txt_calfare);
        confirm = (AppCompatButton) view.findViewById(R.id.btn_confirm);
        cancel = (AppCompatButton) view.findViewById(R.id.btn_cancel);
        pickup_location = (TextView) view.findViewById(R.id.txt_pickup);
        drop_location = (TextView) view.findViewById(R.id.txt_drop);
        textView1 = (TextView) view.findViewById(R.id.textView1);
        open1 = view.findViewById(R.id.open1);
        textView8 = view.findViewById(R.id.textView8);
        close1 = view.findViewById(R.id.close1);
        any = view.findViewById(R.id.any);
        textView2 = (TextView) view.findViewById(R.id.textView12);
        editText3 = (EditText) view.findViewById(R.id.editText3);
        editText4 = (EditText) view.findViewById(R.id.editText4);
        couponcode = (EditText) view.findViewById(R.id.couponcode);
        imageView5 =(ImageView) view.findViewById(R.id.imageView5);
        imageView12 =(ImageView) view.findViewById(R.id.imageView12);
        textView3 = (TextView) view.findViewById(R.id.textView3);
        textView4 = (TextView) view.findViewById(R.id.textView4);
        textView5 = (TextView) view.findViewById(R.id.textView5);
        textView6 = (TextView) view.findViewById(R.id.textView6);
        txt_name = (TextView) view.findViewById(R.id.txt_name);
        txt_number = (TextView) view.findViewById(R.id.txt_number);
        txt_fare = (TextView) view.findViewById(R.id.fare_textview);
        txt_vehiclename = (TextView) view.findViewById(R.id.txt_vehiclename1);
        txt_goodstype = (TextView) view.findViewById(R.id.txt_goodstype1);
        txt_insurance = (TextView) view.findViewById(R.id.txt_insurance1);
        txt_payment = (TextView) view.findViewById(R.id.txt_payment1);
        phone = (ImageButton) view.findViewById(R.id.phone);
        help1 =(LinearLayout) view.findViewById(R.id.help1);
        pack1 =(LinearLayout) view.findViewById(R.id.pack1);
        pick_later = view.findViewById(R.id.pick_later1);
        pick_now = view.findViewById(R.id.pick_now1);
        goodstext = view.findViewById(R.id.goodstext);
        imageButton8 = view.findViewById(R.id.imageButton8);
        imageButton7 = view.findViewById(R.id.imageButton7);
        imageButton2 = view.findViewById(R.id.imageButton2);
        applycoupon = view.findViewById(R.id.button6);

        bike_img = view.findViewById(R.id.bike_img);
        scroll2 = view.findViewById(R.id.scroll2);
        micro_img = view.findViewById(R.id.micro_img);
        mini_img = view.findViewById(R.id.mini_img);
        sedan_img = view.findViewById(R.id.sedan_img);
        xuv_img = view.findViewById(R.id.xuv_img);
        close2 = view.findViewById(R.id.close2);
        goods_spinner = view.findViewById(R.id.goods_spinner1);
        ins_spinner = view.findViewById(R.id.ins_spinner1);
        payment_spinner = view.findViewById(R.id.payment_spinner1);
        pacakege1 = view.findViewById(R.id.pacakege1);
        linear_request = view.findViewById(R.id.linear_request);
        v2 = view.findViewById(R.id.v2);
        helper_spinner1 = view.findViewById(R.id.helper_spinner1);
        bike = view.findViewById(R.id.bike);
        llTrack = view.findViewById(R.id.llTrack);
        goodslay = view.findViewById(R.id.goodslay);
        helperlay = view.findViewById(R.id.helperlay);
        taxi = view.findViewById(R.id.taxi);
        micro = view.findViewById(R.id.micro);
        mini = view.findViewById(R.id.mini);
        sedan = view.findViewById(R.id.sedan);
        xuv = view.findViewById(R.id.xuv);
        helper_text5 = view.findViewById(R.id.textView2);

        //title = (TextView) view.findViewById(R.id.text2w);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        sessionManager = new SessionManager(getActivity());
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        Bundle bundle = getArguments();
        pass = new Pass();
        if (bundle != null) {
            pass = (Pass) bundle.getSerializable("data");
            if (pass != null) {

                clat = pass.getpickLat();
                clong=pass.getpickLong();
                origin = new LatLng(clat, clong);
              //  driver_id = pass.getDriverId();
              //  fare = Double.valueOf(pass.getFare());
         //       drivername = pass.getDriverName();
//                s = pass.gettype();
//                t = pass.gettype1();
//                vin =pass.gettype2();
//                in = pass.getInsurance();
//                helper=pass.gethelper();
                List<Address> address0,address23;
                Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    address0 = geocoder1.getFromLocation(clat,clong,1);
                    pickup_address =address0.get(0).getAddressLine(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }


//                if (drivername != null) {
//                    //   txt_name.setText(drivername);
//                }
////                pickup_location.setText(pickup_address);
////                drop_location.setText(drop_address);
                dl =pass.getdl();
                int s1 = Integer.parseInt(s);
                int t1 =1;
                t1 = Integer.parseInt(t);

                 dl1 = Integer.parseInt(dl);
                if(dl1 == 1)
                {
                   dlat=pass.getdropLat();
                   dlong=pass.getdropLong();
                    List<Address> address1;
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);

                }
                if(dl1 == 2)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);

                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);
                }
                if(dl1 == 3)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);



                }
                if(dl1 == 4)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);


                    dlat3=pass.getdropLat3();
                    dlong3=pass.getdropLong3();
                    List<Address> address4;
                    try {
                        address4 = geocoder1.getFromLocation(dlat3,dlong3,1);
                        drop_address3 =address4.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination3 = new LatLng(dlat3, dlong3);

                }
                if(dl1 == 5)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);


                    dlat3=pass.getdropLat3();
                    dlong3=pass.getdropLong3();
                    List<Address> address4;
                    try {
                        address4 = geocoder1.getFromLocation(dlat3,dlong3,1);
                        drop_address3 =address4.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination3 = new LatLng(dlat3, dlong3);


                    dlat4=pass.getdropLat4();
                    dlong4=pass.getdropLong4();
                    List<Address> address5;
                    try {
                        address5 = geocoder1.getFromLocation(dlat4,dlong4,1);
                        drop_address4 =address5.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination4 = new LatLng(dlat4, dlong4);

                }
                if(dl1 == 6)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);


                    dlat3=pass.getdropLat3();
                    dlong3=pass.getdropLong3();
                    List<Address> address4;
                    try {
                        address4 = geocoder1.getFromLocation(dlat3,dlong3,1);
                        drop_address3 =address4.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination3 = new LatLng(dlat3, dlong3);


                    dlat4=pass.getdropLat4();
                    dlong4=pass.getdropLong4();
                    List<Address> address5;
                    try {
                        address5 = geocoder1.getFromLocation(dlat4,dlong4,1);
                        drop_address4 =address5.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination4 = new LatLng(dlat4, dlong4);

                    dlat5=pass.getdropLat5();
                    dlong5=pass.getdropLong5();
                    List<Address> address6;
                    try {
                        address6 = geocoder1.getFromLocation(dlat5,dlong5,1);
                        drop_address5 =address6.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination5 = new LatLng(dlat5, dlong5);


                }
                if(dl1 == 7)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);


                    dlat3=pass.getdropLat3();
                    dlong3=pass.getdropLong3();
                    List<Address> address4;
                    try {
                        address4 = geocoder1.getFromLocation(dlat3,dlong3,1);
                        drop_address3 =address4.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination3 = new LatLng(dlat3, dlong3);


                    dlat4=pass.getdropLat4();
                    dlong4=pass.getdropLong4();
                    List<Address> address5;
                    try {
                        address5 = geocoder1.getFromLocation(dlat4,dlong4,1);
                        drop_address4 =address5.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination4 = new LatLng(dlat4, dlong4);

                    dlat5=pass.getdropLat5();
                    dlong5=pass.getdropLong5();
                    List<Address> address6;
                    try {
                        address6 = geocoder1.getFromLocation(dlat5,dlong5,1);
                        drop_address5 =address6.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination5 = new LatLng(dlat5, dlong5);

                    dlat6=pass.getdropLat6();
                    dlong6=pass.getdropLong6();
                    List<Address> address7;
                    try {
                        address7 = geocoder1.getFromLocation(dlat6,dlong6,1);
                        drop_address6 =address7.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination6 = new LatLng(dlat6, dlong6);


                }
                if(dl1 == 8)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);


                    dlat3=pass.getdropLat3();
                    dlong3=pass.getdropLong3();
                    List<Address> address4;
                    try {
                        address4 = geocoder1.getFromLocation(dlat3,dlong3,1);
                        drop_address3 =address4.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination3 = new LatLng(dlat3, dlong3);


                    dlat4=pass.getdropLat4();
                    dlong4=pass.getdropLong4();
                    List<Address> address5;
                    try {
                        address5 = geocoder1.getFromLocation(dlat4,dlong4,1);
                        drop_address4 =address5.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination4 = new LatLng(dlat4, dlong4);

                    dlat5=pass.getdropLat5();
                    dlong5=pass.getdropLong5();
                    List<Address> address6;
                    try {
                        address6 = geocoder1.getFromLocation(dlat5,dlong5,1);
                        drop_address5 =address6.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination5 = new LatLng(dlat5, dlong5);

                    dlat6=pass.getdropLat6();
                    dlong6=pass.getdropLong6();
                    List<Address> address7;
                    try {
                        address7 = geocoder1.getFromLocation(dlat6,dlong6,1);
                        drop_address6 =address7.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination6 = new LatLng(dlat6, dlong6);


                    dlat7=pass.getdropLat7();
                    dlong7=pass.getdropLong7();
                    List<Address> address8;
                    try {
                        address8 = geocoder1.getFromLocation(dlat7,dlong7,1);
                        drop_address7 =address8.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination7 = new LatLng(dlat7, dlong7);


                }
                if(dl1 == 9)
                {
                    dlat=pass.getdropLat();
                    dlong=pass.getdropLong();
                    List<Address> address1;
                    Geocoder geocoder2 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address1 = geocoder1.getFromLocation(dlat,dlong,1);
                        drop_address =address1.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination = new LatLng(dlat, dlong);


                    dlat1=pass.getdropLat1();
                    dlong1=pass.getdropLong1();
                    List<Address> address2;
                    try {
                        address2 = geocoder2.getFromLocation(dlat1,dlong1,1);
                        drop_address1 =address2.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination1 = new LatLng(dlat1, dlong1);

                    dlat2=pass.getdropLat2();
                    dlong2=pass.getdropLong2();
                    List<Address> address3;
                    Geocoder geocoder3 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        address3 = geocoder3.getFromLocation(dlat2,dlong2,1);
                        drop_address2 =address3.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination2 = new LatLng(dlat2, dlong2);


                    dlat3=pass.getdropLat3();
                    dlong3=pass.getdropLong3();
                    List<Address> address4;
                    try {
                        address4 = geocoder1.getFromLocation(dlat3,dlong3,1);
                        drop_address3 =address4.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination3 = new LatLng(dlat3, dlong3);


                    dlat4=pass.getdropLat4();
                    dlong4=pass.getdropLong4();
                    List<Address> address5;
                    try {
                        address5 = geocoder1.getFromLocation(dlat4,dlong4,1);
                        drop_address4 =address5.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination4 = new LatLng(dlat4, dlong4);

                    dlat5=pass.getdropLat5();
                    dlong5=pass.getdropLong5();
                    List<Address> address6;
                    try {
                        address6 = geocoder1.getFromLocation(dlat5,dlong5,1);
                        drop_address5 =address6.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination5 = new LatLng(dlat5, dlong5);

                    dlat6=pass.getdropLat6();
                    dlong6=pass.getdropLong6();
                    List<Address> address7;
                    try {
                        address7 = geocoder1.getFromLocation(dlat6,dlong6,1);
                        drop_address6 =address7.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination6 = new LatLng(dlat6, dlong6);


                    dlat7=pass.getdropLat7();
                    dlong7=pass.getdropLong7();
                    List<Address> address8;
                    try {
                        address8 = geocoder1.getFromLocation(dlat7,dlong7,1);
                        drop_address7 =address8.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination7 = new LatLng(dlat7, dlong7);


                    dlat8=pass.getdropLat8();
                    dlong8=pass.getdropLong8();
                    List<Address> address9;
                    try {
                        address9 = geocoder1.getFromLocation(dlat8,dlong8,1);
                        drop_address8 =address9.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    destination8 = new LatLng(dlat8, dlong8);

                }


            }
        }
        overrideFonts(getActivity(), view);
        if (sessionManager != null) {
            HashMap<String, String> getDetail = sessionManager.getUserDetails();
            String id = getDetail.get(SessionManager.USER_ID);
            if (id != null && !id.equals("")) {
                user_id = id;
            }

        }

//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });

        txt_fare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ef == 2) {

                    if(ft == 1) {
                        final Dialog dialog = new Dialog(getActivity());
                        dialog.setTitle("Fare Breakup");
                        dialog.setContentView(R.layout.dialog);
                        dialog.show();
                        final TextView basefare1 = (TextView) dialog.findViewById(R.id.bf);
                        final TextView km1 = (TextView) dialog.findViewById(R.id.km);
                        final TextView mins1 = (TextView) dialog.findViewById(R.id.mins);
                        final TextView tot1 = (TextView) dialog.findViewById(R.id.tot);
                        final TextView dis = (TextView) dialog.findViewById(R.id.discount2);
                        final TextView sf1 = (TextView) dialog.findViewById(R.id.sf2);

                        String bf = String.format("₹" + "%.2f", basefare);
                        String ff11 = String.format("₹" + "%.2f", ff1);
                        String ff111 = String.format("₹" + "%.2f", ff2);
                        String ff = String.format("₹" + "%.2f", finalfare);
                        String dis1 = String.format("₹" + "%.2f", discount1);
                        String sf2 = String.format("₹" + "%.2f", sf);

                        basefare1.setText(bf);
                        km1.setText(ff11);
                        mins1.setText(ff111);
                        tot1.setText(ff);
                        dis.setText(dis1);
                        sf1.setText(sf2);
                    }

                    if(ft == 0) {
                        final Dialog dialog = new Dialog(getActivity());
                        dialog.setTitle("Fare Breakup");
                        dialog.setContentView(R.layout.dialog);
                        dialog.show();
                        final TextView basefare1 = (TextView) dialog.findViewById(R.id.bf);
                        final TextView km1 = (TextView) dialog.findViewById(R.id.km);
                        final TextView mins1 = (TextView) dialog.findViewById(R.id.mins);
                        final TextView tot1 = (TextView) dialog.findViewById(R.id.tot);
                        final TextView dis = (TextView) dialog.findViewById(R.id.discount2);
                        final TextView sf1 = (TextView) dialog.findViewById(R.id.sf2);



                        basefare1.setText("₹" + "0.0" );
                        km1.setText("₹" + "0.0" );
                        mins1.setText("₹" + "0.0" );
                        tot1.setText("₹" + "0.0" );
                        dis.setText("₹" + "0.0" );
                        sf1.setText("₹" + "0.0" );
                    }
                }

                else
                {
                        Toast.makeText(getActivity(), "Select Goods First", Toast.LENGTH_LONG).show();

                }

            }
        });

        phone.setOnClickListener(new View.OnClickListener() {
            private Object Preferences;

            @Override
            public void onClick(View v) {
//                final Uri uriContact = ContactsContract.Contacts.CONTENT_URI;
//                Intent intentPickContact = new Intent(Intent.ACTION_PICK, uriContact);
//                startActivityForResult(intentPickContact, RQS_PICKCONTACT);
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
                // final Context context = container.getContext();

//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                startActivityForResult(intent, 1);
            }
        });

    }





    public static final String QUERY_KEY = "query";

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Log.i(TAG, "onActivityResult()");
        int IDresultHolder;
        if (resultCode == Activity.RESULT_OK) {
            Log.d("number", " hi");
            switch (requestCode) {
                case (PICK_CONTACT):
//                    Uri result = data.getData();
//                    String id = result.getLastPathSegment();
//
//                    getContactName(id);

                    Uri contactData = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst())
                    {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1"))
                        {
                            Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                            phones.moveToFirst();
                            String cNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            // Toast.makeText(getApplicationContext(), cNumber, Toast.LENGTH_SHORT).show();
                          Log.d("number",""+cNumber);
                            editText4.setText(cNumber);




                        }
                    }

                    break;

            }

        }
    }
    private void getContactName(String id) {
        Cursor cursor = null;
        String result = "";

        try {


            cursor = getActivity().getContentResolver().query(ContactsContract
                            .Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=?",
                    new String[] { id },
                    null);

            if (cursor.moveToFirst()) {
                result = cursor.getString(cursor.getColumnIndex
                        (ContactsContract.Data.DATA1));

                // Get all columns
                mAllCols = "";
                String columns[] = cursor.getColumnNames();
                for (String column : columns) {
                    int index = cursor.getColumnIndex(column);
                    mAllCols += ("(" + index + ") [" + column + "] = ["
                            + cursor.getString(index) + "]\n");

                    Log.d("mcols",mAllCols);
                }

                cursor.close();
            }
        } catch (Exception e) {

        }

        mContactName = result;

    }



//    private ArrayList<String> getOwnerPhone() {
//        ArrayList<String> numbers = new ArrayList<>();
//     //   getPhoneFromContacts(numbers);
//       getPhoneFromProfile(numbers);
//        return numbers;
//    }
//    private void getPhoneFromContacts(ArrayList<String> numbers) {
//        final AccountManager manager = AccountManager.get(getActivity());
//        final Account[] accounts = manager.getAccountsByType("com.google");
//        String accountName, id, number;
//
//        if (accounts[0].name != null) {
//            accountName = accounts[0].name;
//
//            ContentResolver cr = getActivity().getContentResolver();
//            Cursor emailCur = cr.query(
//                    ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
//                    ContactsContract.CommonDataKinds.Email.DATA + " = ?",
//                    new String[]{accountName}, null);
//            if (emailCur != null) {
//                if (emailCur.moveToFirst()) {
//                    do {
//                        id = emailCur
//                                .getString(emailCur
//                                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
//
//                        if (id != null) {
//
//                            Cursor pCur = cr.query(
//                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                                    null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
//                                            + " = ?", new String[]{id}, null);
//                            if (pCur != null) {
//                                while (pCur.moveToNext()) {
//                                    number = pCur
//                                            .getString(pCur
//                                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                                    // this will check and format phone number
//                                    Log.d("num",""+number);
//                                }
//                                pCur.close();
//                            }
//                        }
//                    } while (emailCur.moveToNext());
//                }
//
//                emailCur.close();
//            }
//        }
//    }
//    private void getPhoneFromProfile(ArrayList<String> numbers) {
//        final ContentResolver content = getActivity().getContentResolver();
//        final Cursor cursor = content.query(
//                Uri.withAppendedPath(
//                        ContactsContract.Profile.CONTENT_URI,
//                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
//                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
//                ContactsContract.Contacts.Data.MIMETYPE + "=?",
//                new String[]{ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
//                null
//        );
//
//        String number;
//        if (cursor != null) {
//            String[] columns = cursor.getColumnNames();
//            while (cursor.moveToNext()) {
//                for (String column : columns) {
//                    number = cursor.getString(cursor.getColumnIndex(column));
//                    //check and format phone number
//                    Log.d("num",""+number);
//                }
//            }
//            cursor.close();
//
//        }







    /*
     * Invoked when the parent Activity is instantiated
     * and the Fragment's UI is ready. Put final initialization
     * steps here.
     */


        // Initializes the loader framework




        @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
            if (getActivity() != null) {
                if (direction.isOK()) {
                    ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
                    myMap.addPolyline(DirectionConverter.createPolyline(getActivity(), directionPositionList, 3, Color.RED));
                } else {
                    //  distanceAlert(direction.getErrorMessage());
                }

                myMap.addMarker(new MarkerOptions().position(new LatLng(clat, clong)).title("Pickup Location").snippet(pickup_address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                myMap.addMarker(new MarkerOptions().position(new LatLng(dlat, dlong)).title("Drop Location").snippet(drop_address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                if(destination1!=null) {
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat1, dlong1)).title("Drop Location 1").snippet(drop_address1).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                }
                if(destination2!=null) {
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat2, dlong2)).title("Drop Location 2").snippet(drop_address2).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                }
                if(destination3!=null) {
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat3, dlong3)).title("Drop Location 3").snippet(drop_address3).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
                }
                if(destination4!=null) {
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat4, dlong4)).title("Drop Location 4").snippet(drop_address4).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
                }
                if(destination5!=null){
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat5, dlong5)).title("Drop Location 5").snippet(drop_address5).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                }
                if(destination6!=null) {
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat6, dlong6)).title("Drop Location 6").snippet(drop_address6).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                }
                if(destination7!=null) {
                    myMap.addMarker(new MarkerOptions().position(new LatLng(dlat7, dlong7)).title("Drop Location 7").snippet(drop_address7).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                }
//            if(destination7!=null) {
//                myMap.addMarker(new MarkerOptions().position(new LatLng(dlat8, dlong8)).title("Drop Location 8").snippet(drop_address8).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HU)));
//            }

                myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(clat,80.1444 ),10));

            }

        }

    @Override
    public void onDirectionFailure(Throwable t) {
        distanceAlert(t.getMessage() + "\n" + t.getLocalizedMessage() + "\n");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myMap = googleMap;
        requestDirection();
    }

    public void requestDirection() {
        calculateFare.setVisibility(View.VISIBLE);
        // Snackbar.make(view, "calculating fare", Snackbar.LENGTH_INDEFINITE).show();
        GoogleDirection.withServerKey(getString(R.string.google_api_key))
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(this);

        if(destination1!=null) {
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination).to(destination1).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination2!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination1).to(destination2).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination3!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination2).to(destination3).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination4!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination3).to(destination4).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination5!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination4).to(destination5).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination6!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination5).to(destination6).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination7!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination6).to(destination7).transportMode(TransportMode.DRIVING).execute(this);
        }
        if(destination8!=null){
            GoogleDirection.withServerKey(getString(R.string.google_api_key)).from(destination7).to(destination8).transportMode(TransportMode.DRIVING).execute(this);
        }
    }

    private void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "font/AvenirLTStd_Medium.otf"));
            }
        } catch (Exception e) {
        }
    }
    public void applycoupon() {


        int t2 = Integer.parseInt(in);
        if (t2==10 || t2==9 || t2==3 || t2==4){
            Toast.makeText(getActivity(),"Not Accepted for  HIGHER VEHCILES OR PACKAGES", Toast.LENGTH_LONG).show();
         return;
        }
       String coupon = couponcode.getText().toString();
            final RequestParams params = new RequestParams();
            params.put("coupon",coupon);
            params.put("userid",user_id); //Added by shameem 26/12/2019
            Server.setHeader(sessionManager.getKEY());

            Server.get("api/user/applycoupon1/format/json", params, new JsonHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();

                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    super.onSuccess(statusCode, headers, response);
                    String jsonStr = null;
                    try {
                        Gson gson = new GsonBuilder().create();

                        if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                            List<PendingRequestPojo> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<PendingRequestPojo>>() {
                            }.getType());
                            if (response.has("data") ) {


                                // If you have array
                                // hold your JSON response in String

                                JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array

                                //         Iterate the loop
                                for (int i = 0; i < resultArray.length(); i++) {
                                    // get value with the NODE key
                                    JSONObject obj = resultArray.getJSONObject(i);
                                    String name =  obj.getString("discount");
                                    discount = Double.parseDouble(name);

                                    String coupon_name = obj.getString("couponcode");
                                    mycoupon = coupon_name;

                                    Log.d("discount","" +discount);
                                    Log.d("coupon_name",mycoupon);

                                    Toast.makeText(getActivity(),"COUPON APPLIED", Toast.LENGTH_LONG).show();

                                }


                            } else {
                                Toast.makeText(getActivity(),"Enter Valid Code", Toast.LENGTH_LONG).show();

                            }


                        } else {

                            Toast.makeText(getActivity(),"Enter Valid Code", Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {

                        Toast.makeText(getActivity(), getString(R.string.contact_admin), Toast.LENGTH_LONG).show();


                    }

                    calculateDistance();


                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    if (getActivity() != null) {
                    }
                }
            });





//            Intent intent = new Intent(getActivity(), payment.class);
//            startActivity(intent);

    }

    public void AddRide(String key, String pickup_adress, String drop_address, String pickup_location, String drop_locatoin, String drop_locatoin1, String drop_locatoin2, String drop_locatoin3, String drop_locatoin4, String drop_locatoin5, String drop_locatoin6, String drop_locatoin7, String drop_locatoin8, String amount, String distance, String goodstype1, String t, String s, String drop_address1, String drop_address2, String drop_address3, String drop_address4, String drop_address5, String drop_address6, String drop_address7, String drop_address8) {

        calculateDistance();
        String gtype = goodstext.getText().toString();

        final RequestParams params = new RequestParams();
        params.put("user_id", user_id);
        params.put("pickup_adress", pickup_adress);
        params.put("drop_address", drop_address);
        params.put("pikup_location", pickup_location);
        params.put("drop_locatoin", drop_locatoin);
        params.put("drop_locatoin1", drop_locatoin1);
        params.put("drop_locatoin2", drop_locatoin2);
        params.put("drop_locatoin3", drop_locatoin3);
        params.put("drop_locatoin4", drop_locatoin4);
        params.put("drop_locatoin5", drop_locatoin5);
        params.put("drop_locatoin6", drop_locatoin6);
        params.put("drop_locatoin7", drop_locatoin7);
        params.put("drop_locatoin8", drop_locatoin8);
        params.put("drop_address1" , drop_address1);
        params.put("drop_address2" , drop_address2);
        params.put("drop_address3" , drop_address3);
        params.put("drop_address4" , drop_address4);
        params.put("drop_address5" , drop_address5);
        params.put("drop_address6" , drop_address6);
        params.put("drop_address7" , drop_address7);
        params.put("drop_address8" , drop_address8);
        params.put("amount", finalfare);
        params.put("distance", distance);
        params.put("goodstype", gtype);
        Log.d("vehicleinfocheck",s);
        params.put("vehicle_info", s);
        params.put("helper",helper);
        params.put("packages",in);
        params.put("vtype", t);
        Server.setHeader(sessionManager.getKEY());

        Log.d("server",""+params);
        Server.get("api/user/addRide/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                // swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Toast.makeText(getActivity(), getString(R.string.ride_has_been_requested), Toast.LENGTH_LONG).show();
                        AcceptedRequestFragment acceptedRequestFragment = new AcceptedRequestFragment();
                        Bundle bundle;

                        bundle = new Bundle();
                        bundle.putString("status", "PENDING");
                        acceptedRequestFragment.setArguments(bundle);
                        ((HomeActivity) getActivity()).changeFragment(acceptedRequestFragment, "Requests");
                    } else {
                        Toast.makeText(getActivity(), "Ride Not Available", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Ride Not Available", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Toast.makeText(getActivity(), " Ride Not Available ", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                    // swipeRefreshLayout.setRefreshing(false);

                }

            }
        });
    }

    private  double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit.equals("K")) {
                dist = dist * 1.609344;
            } else if (unit.equals("N")) {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }

    public double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    public double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public void calculateDistance() {
           ft = 1;
            ef =2;
        android.location.Location location1 = new android.location.Location("location1");
        android.location.Location location2 = new android.location.Location("location2");
        android.location.Location location3 = new android.location.Location("location3");
        android.location.Location location4 = new android.location.Location("location4");
        android.location.Location location5 = new android.location.Location("location5");
        android.location.Location location6 = new android.location.Location("location6");
        android.location.Location location7 = new android.location.Location("location7");
        android.location.Location location8 = new android.location.Location("location8");
        android.location.Location location9 = new android.location.Location("location9");

        double d,dd1,dd2,dd3,dd4,dd5,dd6,dd7;
        double distancemtrs = 0.0;
        if(dl1 == 1 ) {
            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
             d = location1.distanceTo(location2) ;
             log.d("distanceinmeteres",Double.toString(d));
             double mydist;
             mydist = distance(clat, clong, destination.latitude, destination.longitude, "K");
             log.d("Shameems distance calc",Double.toString(mydist));
            distancemtrs =0.0+ d;
        }
        if(dl1 == 2 ) {
            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);

            d = location1.distanceTo(location2) ;
            dd1 = location2.distanceTo(location3) ;
            distancemtrs =0.0+ d+dd1;
        }
        if(dl1 == 3 ) {
            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);
            location4.setLatitude(destination2.latitude);
            location4.setLongitude(destination2.longitude);

            d = location1.distanceTo(location2) ;
            dd1 = location2.distanceTo(location3);

            dd2 = location3.distanceTo(location4) ;
            distancemtrs =0.0+ d+dd1+dd2;

        }

        if(dl1 == 4 ) {

            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);
            location4.setLatitude(destination2.latitude);
            location4.setLongitude(destination2.longitude);
            location5.setLatitude(destination3.latitude);
            location5.setLongitude(destination3.longitude);

            d = location1.distanceTo(location2) ;
            dd1 = location2.distanceTo(location3);

            dd2 = location3.distanceTo(location4) ;
            dd3 = location4.distanceTo(location5) ;
            distancemtrs =0.0+ d+dd1+dd2+dd3;

        }
        if(dl1 == 5 ) {

            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);
            location4.setLatitude(destination2.latitude);
            location4.setLongitude(destination2.longitude);
            location5.setLatitude(destination3.latitude);
            location5.setLongitude(destination3.longitude);
            location6.setLatitude(destination4.latitude);
            location6.setLongitude(destination4.longitude);

            d = location1.distanceTo(location2);
            dd1 = location2.distanceTo(location3) ;

            dd2 = location3.distanceTo(location4) ;
            dd3 = location4.distanceTo(location5) ;
            dd4 = location5.distanceTo(location6);
            distancemtrs =0.0+ d+dd1+dd2+dd3+dd4;


        }
        if(dl1 == 6 ) {
            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);
            location4.setLatitude(destination2.latitude);
            location4.setLongitude(destination2.longitude);
            location5.setLatitude(destination3.latitude);
            location5.setLongitude(destination3.longitude);
            location6.setLatitude(destination4.latitude);
            location6.setLongitude(destination4.longitude);
            location7.setLatitude(destination5.latitude);
            location7.setLongitude(destination5.longitude);

            d = location1.distanceTo(location2) ;
            dd1 = location2.distanceTo(location3);

            dd2 = location3.distanceTo(location4);
            dd3 = location4.distanceTo(location5);
            dd4 = location5.distanceTo(location6);
            dd5 = location6.distanceTo(location7);

            distancemtrs =0.0+ d+dd1+dd2+dd3+dd4+dd5;

        }
        if(dl1 == 7 ) {

            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);
            location4.setLatitude(destination2.latitude);
            location4.setLongitude(destination2.longitude);
            location5.setLatitude(destination3.latitude);
            location5.setLongitude(destination3.longitude);
            location6.setLatitude(destination4.latitude);
            location6.setLongitude(destination4.longitude);
            location7.setLatitude(destination5.latitude);
            location7.setLongitude(destination5.longitude);
            location8.setLatitude(destination6.latitude);
            location8.setLongitude(destination6.longitude);

            d = location1.distanceTo(location2) ;
            dd1 = location2.distanceTo(location3);

            dd2 = location3.distanceTo(location4);
            dd3 = location4.distanceTo(location5);
            dd4 = location5.distanceTo(location6);
            dd5 = location6.distanceTo(location7);
            dd6 = location7.distanceTo(location8);
            distancemtrs =0.0+ d+dd1+dd2+dd3+dd4+dd5+dd6;



        }
        if(dl1 == 8 ) {
            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            location3.setLatitude(destination1.latitude);
            location3.setLongitude(destination1.longitude);
            location4.setLatitude(destination2.latitude);
            location4.setLongitude(destination2.longitude);
            location5.setLatitude(destination3.latitude);
            location5.setLongitude(destination3.longitude);
            location6.setLatitude(destination4.latitude);
            location6.setLongitude(destination4.longitude);
            location7.setLatitude(destination5.latitude);
            location7.setLongitude(destination5.longitude);
            location8.setLatitude(destination6.latitude);
            location8.setLongitude(destination6.longitude);
            location9.setLatitude(destination7.latitude);
            location9.setLongitude(destination7.longitude);

            d = location1.distanceTo(location2) ;
            dd1 = location2.distanceTo(location3) ;

            dd2 = location3.distanceTo(location4);
            dd3 = location4.distanceTo(location5) ;
            dd4 = location5.distanceTo(location6);
            dd5 = location6.distanceTo(location7);
            dd6 = location7.distanceTo(location8);
            dd7 = location8.distanceTo(location9);
            distancemtrs =0.0+ d+dd1+dd2+dd3+dd4+dd5+dd6+dd7;



        }
        //-------------------------

        SimpleDateFormat dateformat = new SimpleDateFormat("HH");
        Date currentTime = Calendar.getInstance().getTime();
        String currentDateandTime = dateformat.format(currentTime);
        int currenttime1 = Integer.parseInt(currentDateandTime);
        double distancekm = distancemtrs/1000;
        String dd = Double.toString(distancekm);
        double timetaken = distancekm / 0.15;
        Log.d("distance33","" +distancemtrs);
        Log.d("timetaken","" +timetaken);

        distance = String.valueOf(distancekm);

        Double caldistancekm = distancekm;
        Double esttimetaken = timetaken;


        if (caldistancekm != null) {

           int t2 = Integer.parseInt(in);

           Log.d("t2value",Integer.toString(t2));
            //   int t3 = Integer.parseInt(vin);
            int t3= 0;
            if(t2 == 0)
            {
                if (fare != null && fare != 0.0) {
                    DecimalFormat dtime = new DecimalFormat("##.##");
                    Double helperfare;
                    ff8 = (esttimetaken+20)*bikemins;
                    if(helper == "yes") {
                        helperfare = 0.0;
                    }
                    else
                    {
                        helperfare = 0.0;
                    }
                    ff1 = caldistancekm * bikekms;   //kmsfare for ratecard
                    ff2 = esttimetaken * bikemins;  //timefare for ratecard
                    sf = 15.0;
                    basefare = bikebase;
                    Double estfinalfare1 = (caldistancekm * bikekms)+helperfare+15.0+(esttimetaken*bikemins)+bikebase;

                    //if(couponcode.getText().toString().equals("test1") || couponcode.getText().toString().equals("test2"))
                    if (mycoupon.equals("percent"))
                    {

                        double d1 = estfinalfare1/100;
                        double d2 = d1*discount;
                        discount1 = d2;
                        //discount1 = estfinalfare1-d2;
                        estfinalfare1 = estfinalfare1-d2;
                    }

                    else
                    {
                        discount1 = discount;
                        estfinalfare1 = estfinalfare1-discount;

                    }
                    ff8 = (esttimetaken+20)*bikemins;
                    Double estfinalfare2 = estfinalfare1+ff8;
                    if(currenttime1 >=18 && currenttime1 <= 6  ) {
                        estfinalfare1 =estfinalfare1*1.5;
                        estfinalfare2 =estfinalfare2*1.5;
                    }
                    finalfare = Double.valueOf(dtime.format(estfinalfare1));
                    finalfare1 = Double.valueOf(dtime.format(estfinalfare2));
                    pack1.setVisibility(View.GONE);
                    textView6.setText("None");
                    txt_fare.setText("₹" + " " + finalfare+" "+"-"+" "+finalfare1);
                    Faretxt = "₹" + " " + finalfare+" "+"-"+" "+finalfare1;
                } else {
                    txt_fare.setText("₹");

                }
            }

            if(t2 == 7)
            {
                if (fare != null && fare != 0.0) {
                    DecimalFormat dtime = new DecimalFormat("##.##");
                    ff1 = caldistancekm * acekms;        //kmsfare for ratecard
                    ff2 = esttimetaken * acemins;   //timefare for ratecard
                    ff8 = (esttimetaken+20)*acemins;
                    Double estfinalfare1 = ff1+ff2;
                    Double estfinalfare2;
                    Double helperfare;
                    sf= 143.0;
                    if(helper == "yes") {
                        helperfare = 0.0;
                    }
                    else
                    {
                        helperfare = 0.0;
                    }
                    estfinalfare1 = estfinalfare1+helperfare+acebase+143;
                    ff8 = (esttimetaken+20)*acemins;


                    //if(couponcode.getText().toString().equals("test1") || couponcode.getText().toString().equals("test2"))
                    if (mycoupon.equals("percent"))
                    {

                        double d1 = estfinalfare1/100;
                        double d2 = d1*discount;
                        discount1 = d2;
                        //discount1 = estfinalfare1-d2;
                        estfinalfare1 = estfinalfare1-d2;
                    }

                    else
                    {
                        discount1 = discount;
                        estfinalfare1 = estfinalfare1-discount;

                    }

                    estfinalfare2 = estfinalfare1+ff8;

                    if(currenttime1 >=18 && currenttime1 <= 6  ) {
                        estfinalfare1 =estfinalfare1*1.5;
                        estfinalfare2=estfinalfare2*1.5;
                    }

                    basefare = acebase;
                    finalfare = Double.valueOf(dtime.format(estfinalfare1));
                    finalfare1=Double.valueOf(dtime.format(estfinalfare2));
                    pack1.setVisibility(View.GONE);
                    textView6.setText("None");
                    txt_fare.setText("₹" + " " + finalfare+" "+"-"+" "+finalfare1);
                    Faretxt = "₹" + " " + finalfare+" "+"-"+" "+finalfare1;
                } else {
                    txt_fare.setText("₹");

                }
            }
            if(t2 == 8)
            {
                if (fare != null && fare != 0.0) {
                    DecimalFormat dtime = new DecimalFormat("##.##");
                     ff1 = caldistancekm * apekms;      //kmsfare for ratecard
                     ff2 = esttimetaken * apemins;      //timefare for ratecard
                    Double estfinalfare1 = ff1+ff2;
                    sf = 143.0;
                    Double help;

                    if(helper == "yes") {
                        help = 0.0;
                    }
                    else
                    {
                        help = 0.0;
                    }
                    estfinalfare1 = estfinalfare1+help+apebase+143;
                    ff8 = (esttimetaken+20)*apemins;
                    //if(couponcode.getText().toString().equals("test1") || couponcode.getText().toString().equals("test2"))
                    if (mycoupon.equals("percent"))
                    {

                        double d1 = estfinalfare1/100;
                        double d2 = d1*discount;
                        discount1 = d2;
                        //discount1 = estfinalfare1-d2;
                        estfinalfare1 = estfinalfare1-d2;
                    }

                    else
                    {
                        discount1 = discount;
                        estfinalfare1 = estfinalfare1-discount;

                    }
                    Double estfinalfare2 = estfinalfare1+ff8;
                    if(currenttime1 >=18 && currenttime1 <= 6  ) {
                        estfinalfare1 =estfinalfare1*1.5;
                        estfinalfare2 =estfinalfare2*1.5;
                    }

                    basefare = apebase;

                    finalfare = Double.valueOf(dtime.format(estfinalfare1));
                    finalfare1=Double.valueOf(dtime.format(estfinalfare2));
                    pack1.setVisibility(View.GONE);
                    textView6.setText("None");
                    txt_fare.setText("₹" + " " + finalfare+" "+"-"+" "+finalfare1);
                    Faretxt = "₹" + " " + finalfare+" "+"-"+" "+finalfare1;
                } else {
                    txt_fare.setText("₹");
                }
            }
            if(t2 == 9)
            {
                if (fare != null && fare != 0.0) {
                    DecimalFormat dtime = new DecimalFormat("##.##");
                     ff1 = caldistancekm * t407kms;          //kmsfare for ratecard
                     ff2 = esttimetaken * t407mins;           //timefare for ratecard
                    Double estfinalfare1 = ff1+ff2+t407base;
                    sf = 182.0;
                    Double help;

                    if(helper == "yes") {
                        help = 0.0;
                    }
                    else
                    {
                        help = 0.0;
                    }
                    estfinalfare1 = estfinalfare1+help+182.00;
                    ff8 = (esttimetaken+20)*t407mins;
                    //if(couponcode.getText().toString().equals("test1") || couponcode.getText().toString().equals("test2"))
                    if (mycoupon.equals("percent"))
                    {

                        double d1 = estfinalfare1/100;
                        double d2 = d1*discount;
                        discount1 = d2;
                        //discount1 = estfinalfare1-d2;
                        estfinalfare1 = estfinalfare1-d2;
                    }

                    else
                    {
                        discount1 = discount;
                        estfinalfare1 = estfinalfare1-discount;

                    }
                    Double estfinalfare2 = estfinalfare1+ff8;

                    if(currenttime1 >=18 && currenttime1 <= 6  ) {
                        estfinalfare1 =estfinalfare1*2.5;
                        estfinalfare2 =estfinalfare2*2.5;
                    }


                    basefare = t407base;
                    finalfare = Double.valueOf(dtime.format(estfinalfare1));
                    finalfare1=Double.valueOf(dtime.format(estfinalfare2));
                    pack1.setVisibility(View.GONE);
                    textView6.setText("None");
                    txt_fare.setText("₹" + " " + finalfare+" "+"-"+" "+finalfare1);
                    Faretxt = "₹" + " " + finalfare+" "+"-"+" "+finalfare1;
                } else {
                    txt_fare.setText("₹");
                }
            }

            if(t2 == 10)
            {
                if (fare != null && fare != 0.0) {
                    DecimalFormat dtime = new DecimalFormat("##.##");
                     ff1 = caldistancekm * dostkms;          //kmsfare for ratecard
                     ff2 = esttimetaken * dostmins;           //timefare for ratecard
                    Double estfinalfare1 = ff1+ff2;
                    Double helperfare;
                    sf = 233.0;
                    if(helper == "yes") {
                        helperfare = 0.0;
                    }
                    else
                    {
                        helperfare = 0.0;
                    }
                    estfinalfare1 = ff1+ff2+helperfare+dostbase+233.0;
                    ff8 = (esttimetaken+20)*dostmins;
                   // if(couponcode.getText().toString().equals("test1") || couponcode.getText().toString().equals("test2"))
                    if (mycoupon.equals("percent"))
                    {

                        double d1 = estfinalfare1/100;
                        double d2 = d1*discount;
                        discount1 = d2;
                        //discount1 = estfinalfare1-d2;
                        estfinalfare1 = estfinalfare1-d2;
                    }

                    else
                    {
                        discount1 = discount;
                        estfinalfare1 = estfinalfare1-discount;

                    }
                    Double estfinalfare2 = estfinalfare1+ff8;

                    if(currenttime1 >=18 && currenttime1 <= 6  ) {
                        estfinalfare1 =estfinalfare1*2;
                        estfinalfare2 =estfinalfare2*2;
                    }

                    basefare = dostbase;
                    finalfare = Double.valueOf(dtime.format(estfinalfare1));
                    finalfare1=Double.valueOf(dtime.format(estfinalfare2));
                    pack1.setVisibility(View.GONE);
                    textView6.setText("None");
                    txt_fare.setText("₹" + " " + finalfare+" "+"-"+" "+finalfare1);
                    Faretxt = "₹" + " " + finalfare+" "+"-"+" "+finalfare1;
                } else {
                    txt_fare.setText("₹");
                }
            }





            if(t2 == 1) {
                if (helper == "yes") {
                    txt_fare.setText("₹" + " " + "750");
                    textView6.setText("3 hours Package"+ "₹" + " " + "750");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 750.0;
                } else {
                    txt_fare.setText("₹" + " " + "750");
                    textView6.setText("3 hours Package"+ "₹" + " " + "750");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 750.0;
                }

            }
            if(t2 == 2)
            {
                if(helper == "yes") {
                    txt_fare.setText("₹" + " " + "1300");
                    textView6.setText("7 hours Package"+"₹" + " " + "1300");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 1300.0;
                }
                else
                {
                    txt_fare.setText("₹" + " " + "1300");
                    textView6.setText("7 hours Package"+"₹" + " " + "1300");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 1300.0;
                }
            }
            if(t2 == 3) {
                if (helper == "yes") {
                    txt_fare.setText("₹" + " " + "1650");
                    textView6.setText("5.5 hours Package"+"₹" + " " + "1650");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 1650.0;

                }
                else
                {
                    txt_fare.setText("₹"+" "+"1650");
                    textView6.setText("5.5 hours Package"+"₹" + " " + "1650");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 1650.0;
                }
            }
            if(t2 == 4) {
                if (helper == "yes") {
                    txt_fare.setText("₹" + " " + "2850");
                    textView6.setText("9 hours Package"+"₹" + " " + "2850");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 2850.0;
                } else {
                    txt_fare.setText("₹" + " " + "2850");
                    textView6.setText("9 hours Package"+"₹" + " " + "2850");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 2850.0;

                }
            }
            if(t2 == 5)
            {
                if(helper == "yes")
                {
                    txt_fare.setText("₹"+" "+"850");
                    textView6.setText("3.5 hours Package"+"₹" + " " + "850");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 850.0;
                }
                else {
                    txt_fare.setText("₹" + " " + "850");
                    textView6.setText("3.5 hours Package"+"₹" + " " + "850");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 850.0;
                }
            }
            if(t2 == 6) {
                if (helper == "yes") {
                    txt_fare.setText("₹" + " " + "1500");
                    textView6.setText("7 hours Package"+"₹" + " " + "850");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 1500.0;
                } else {
                    txt_fare.setText("₹" + " " + "1500");
                    textView6.setText("7 hours Package"+"₹" + " " + "850");
                    ff1 = 0.0;
                    ff2 = 0.0;
                    basefare = 0.0;
                    finalfare = 1500.0;

                }
            }


            if(t2 == 99){

                Toast.makeText(getActivity(), getString(R.string.SELECT_VEHICLE), Toast.LENGTH_LONG).show();

            }

        }
        Log.d("newin",newin);
        if (newin=="7"){
            in = "7";
        }else if (newin=="9"){
            in = "9";
        }
        if (Integer.parseInt(newin)==Integer.parseInt("10")){
            in = "10";
        }
        calculateFare.setVisibility(View.GONE);


    }

    public void calculateDistance1() {
        ft = 1;
        ef =2;
        android.location.Location location1 = new android.location.Location("location1");
        android.location.Location location2 = new android.location.Location("location2");


        double d,dd1,dd2,dd3,dd4,dd5,dd6,dd7;
        double distancemtrs = 0.0;

            location1.setLatitude(clat);
            location1.setLongitude(clong);
            location2.setLatitude(destination.latitude);
            location2.setLongitude(destination.longitude);
            d = location1.distanceTo(location2);
        distancemtrs =0.0+ d;


        SimpleDateFormat dateformat = new SimpleDateFormat("HH");
        Date currentTime = Calendar.getInstance().getTime();
        String currentDateandTime = dateformat.format(currentTime);
        int currenttime1 = Integer.parseInt(currentDateandTime);
        double distancekm = distancemtrs/1000;
        String dd = Double.toString(distancekm);
        double timetaken = distancekm / 0.15;
        Log.d("distance33","" +distancemtrs);
        Log.d("distance33","" +distancemtrs);


        distance = String.valueOf(distancekm);

        Double caldistancekm = distancekm;
        Double esttimetaken = timetaken;


        if (caldistancekm != null) {




                if (fare != null && fare != 0.0) {
                    DecimalFormat dtime = new DecimalFormat("##.##");

                    Double helperfare;
                    ff8 = (esttimetaken+10)*bikemins;


                    if(helper == "yes") {
                        helperfare = 0.0;
                    }
                    else
                    {
                        helperfare = 0.0;
                    }
                    ff1 = caldistancekm * bikekms;
                    ff2 = esttimetaken * bikemins;
                    sf = 15.0;
                    basefare = bikebase;
                    Double estfinalfare1 = (caldistancekm * bikekms)+helperfare+15.0+(esttimetaken*bikemins)+bikebase-discount;
                    ff8 = (esttimetaken+10)*bikemins;
                    Double estfinalfare2 = estfinalfare1+ff8;
                    if(currenttime1 >=18 && currenttime1 <= 6  ) {
                        estfinalfare1 =estfinalfare1*1.5;
                        estfinalfare2 =estfinalfare2*1.5;
                    }
                    finalfare = Double.valueOf(dtime.format(estfinalfare1));
                    finalfare1 = Double.valueOf(dtime.format(estfinalfare1));
                    pack1.setVisibility(View.GONE);
                    textView6.setText("None");
                    txt_fare.setText("₹" + " " + finalfare+" "+"-"+" "+finalfare1);
                } else {
                    txt_fare.setText("₹");
                }


        }
        calculateFare.setVisibility(View.GONE);


    }

    public void distanceAlert(String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(getString(R.string.INVALID_DISTANCE));
        alertDialog.setMessage(message);
        alertDialog.setCancelable(true);
        Drawable drawable = ContextCompat.getDrawable(getActivity(), R.mipmap.ic_warning_white_24dp);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.RED);
        alertDialog.setIcon(drawable);


        alertDialog.setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alert.cancel();
            }
        });
        alert = alertDialog.create();
        alert.show();
    }

    public void NeaBy1() {
        RequestParams params = new RequestParams();

      //  Log.d("sessionkey",""+sessionManager.getKEY());
        Server.setHeader(sessionManager.getKEY());
        Server.get("api/user/fare23/format/json", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> address;

                try {

                    if (response.has("status") && response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new GsonBuilder().create();
                        List<NearbyData> list = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<NearbyData>>() {

                        }.getType());


                        if (response.has("data") ) {

                            // If you have array
                            // hold your JSON response in String
//
                            JSONArray resultArray = response.getJSONArray("data"); // Here you will get the Array

                            //         Iterate the loop
                            for (int i = 0; i < 15; i++) {

                                if(i==0) {
                                    // get value with the NODE key
                                    JSONObject obj = resultArray.getJSONObject(0);
                                    String name = obj.getString("name");

                                    String bike =  obj.getString("value");
                                    bikebase = Double.parseDouble(bike);
                                }
                                if(i==1){
                                    JSONObject obj = resultArray.getJSONObject(7);
                                    String bikekm =  obj.getString("value");
                                    bikekms = Double.parseDouble(bikekm);
                                }




                                if(i==2){
                                    JSONObject obj = resultArray.getJSONObject(8);
                                        String bikemin =  obj.getString("value");
                                    bikemins = Double.parseDouble(bikemin);

                                    }

                                if(i==3){
                                    JSONObject obj = resultArray.getJSONObject(9);
                                        String ape =  obj.getString("value");
                                        apebase = Double.parseDouble(ape);

                                    }
                                if(i==4){
                                    JSONObject obj = resultArray.getJSONObject(10);
                                        String apekm =  obj.getString("value");
                                        apekms = Double.parseDouble(apekm);

                                    }
                                if(i==5){
                                    JSONObject obj = resultArray.getJSONObject(11);
                                        String apemin =  obj.getString("value");
                                        apemins = Double.parseDouble(apemin);

                                    }

                                if(i==6){
                                    JSONObject obj = resultArray.getJSONObject(12);
                                        String ace =  obj.getString("value");
                                        acebase = Double.parseDouble(ace);

                                    }
                                if(i==7){
                                    JSONObject obj = resultArray.getJSONObject(13);
                                        String acekm =  obj.getString("value");
                                        acekms = Double.parseDouble(acekm);

                                    }
                                if(i==8){
                                    JSONObject obj = resultArray.getJSONObject(14);
                                        String acemin =  obj.getString("value");
                                        acemins = Double.parseDouble(acemin);

                                    }

                                if(i==9){
                                    JSONObject obj = resultArray.getJSONObject(15);
                                        String DOST =  obj.getString("value");
                                    dostbase = Double.parseDouble(DOST);
                                        Log.d("dost45",""+dostbase);

                                    }
                                if(i==10){
                                    JSONObject obj = resultArray.getJSONObject(16);
                                        String DOSTkm =  obj.getString("value");
                                        dostkms = Double.parseDouble(DOSTkm);

                                    }
                                if(i==11){
                                    JSONObject obj = resultArray.getJSONObject(17);
                                        String DOSTmin =  obj.getString("value");
                                        dostmins = Double.parseDouble(DOSTmin);

                                    }

                                if(i==12){
                                    JSONObject obj = resultArray.getJSONObject(18);
                                        String T407 =  obj.getString("value");
                                        t407base = Double.parseDouble(T407);

                                    }
                                if(i==13){

                                    JSONObject obj = resultArray.getJSONObject(19);
                                    String T407km =  obj.getString("value");
                                        t407kms = Double.parseDouble(T407km);

                                    }
                                if(i==14){
                                    JSONObject obj = resultArray.getJSONObject(20);
                                        String T407min =  obj.getString("value");
                                        t407mins = Double.parseDouble(T407min);


                                }

                            }
                            String error = response.getString("data");
                        }

                    }
                } catch (JSONException e) {


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                Toast.makeText(getActivity(), getString(R.string.try_again), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (getActivity() != null) {
                }
            }
        });
    }
    public void custom_dialog()
    {

    }



}
