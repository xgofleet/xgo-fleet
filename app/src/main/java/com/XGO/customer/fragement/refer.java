package com.XGO.customer.fragement;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.XGO.customer.R;
import com.XGO.customer.acitivities.HomeActivity;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link refer.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class refer extends FragmentManagePermission {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
View view;
TextView messenger1,sms,wa1,email1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //  MapsInitializer.initialize(this.getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.refer, container, false);
        ((HomeActivity) getActivity()).fontToTitleBar("Refer A Friend");
        bindView();

        return view;

    }

    public void bindView() {

        messenger1= view.findViewById(R.id.messenger1);
        sms= view.findViewById(R.id.sms);
        wa1=view.findViewById(R.id.wa1);
        email1= view.findViewById(R.id.email1);


        email1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String subject = "Hey Try XGO";
                String message = "Try the New Commerical vehicle Booking app For Easy and hassel free delivery Download:   ";
                String linkUrl = "play.google.com/store/apps/details?id=com.XGO.taxicustomer";
                String body = "<a href=\"" + linkUrl + "\">" + linkUrl+ "</a>";

                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_CC, "admin@xgofleets.com");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, message+ "  " + Html.fromHtml(body));
                startActivity(Intent.createChooser(intent, "Choose an Email client :"));

            }
        });
        wa1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String linkUrl = "play.google.com/store/apps/details?id=com.XGO.taxicustomer";
                String body = "<a href=\"" + linkUrl + "\">" + linkUrl+ "</a>";


                PackageManager pm= getActivity().getPackageManager();
                try {

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    String message = "Try the New Commerical vehicle Booking app For Easy and hassel free delivery Download:   ";

                    PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");

                    waIntent.putExtra(Intent.EXTRA_TEXT, message + "  " + Html.fromHtml(body));
                    startActivity(Intent.createChooser(waIntent, "Share with"));

                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(getActivity(), "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });


        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = "Try the New Commerical vehicle Booking app For Easy and hassel free delivery Download:   ";
                String linkUrl = "play.google.com/store/apps/details?id=com.XGO.taxicustomer";
                String body = "<a href=\"" + linkUrl + "\">" + linkUrl+ "</a>";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
                {
                    String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getActivity()); // Need to change the build to API 19

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, message+ "" +  Html.fromHtml(body));

                    if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                    // any app that support this intent.
                    {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    startActivity(sendIntent);

                }
                else // For early versions, do what worked for you before.
                {
                    Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address","phoneNumber");
                    smsIntent.putExtra(Intent.EXTRA_TEXT, message+ "" +  Html.fromHtml(body));
                    startActivity(smsIntent);
                }
            }
        });

        messenger1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String message = "Try the New Commerical vehicle Booking app For Easy and hassel free delivery Download:   ";
                String linkUrl = "play.google.com/store/apps/details?id=com.XGO.taxicustomer";
                String body = "<a href=\"" + linkUrl + "\">" + linkUrl+ "</a>";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,message+ "" +  Html.fromHtml(body));
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.facebook.orca");
                try {
                    startActivity(sendIntent);
                }
                catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(),"Please Install Facebook Messenger", Toast.LENGTH_LONG).show();
                }
            }
        });





    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
